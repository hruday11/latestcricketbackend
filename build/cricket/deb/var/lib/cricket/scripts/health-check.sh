#!/usr/bin/env bash

SVC_PORT=22222

rotation_status=$(curl -w %{http_code} -s -o /dev/null "http://localhost:$SVC_PORT/elbhealthcheck/status")
if [ $rotation_status -eq 200 ]; then
    echo "INFO : Cricket Backend is IN rotation"
    exit 0
elif [ $rotation_status -eq 503 ]; then
    echo "WARNING : Cricket Backend is OUT of rotation"
    exit 2
else
    echo "CRITICAL : Cricket Backend is down"
    exit 2
fi