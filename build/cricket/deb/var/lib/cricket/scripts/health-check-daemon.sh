#!/usr/bin/env bash

host=`hostname`;

exec /usr/lib/nagios/plugins/fk-nsca-wrapper/nsca_wrapper -H $host -S 'Cricket Backend _ENV_ : Health Check' -b /usr/sbin/send_nsca -c /etc/send_nsca.cfg -C "sh /var/lib/cricket/scripts/health-check.sh " 2>&1 &