package com.game.db;


import com.mongodb.MongoClient;
import io.dropwizard.lifecycle.Managed;

import javax.inject.Inject;

/**
 * This is used for manage the connection to MongoDB.
 * @version 1.0.0
 * @since 1.0.0
 * @author Hruday
 */

public class MongoDBManaged implements Managed {

    /**
     * The mongoDb Client
     */
    private MongoClient mongoClient;

    @Inject
    public MongoDBManaged(final MongoClient mongoClient){
        this.mongoClient = mongoClient;
    }

    @Override
    public void start() throws Exception {
    }

    @Override
    public void stop() throws Exception{
        mongoClient.close();
    }
}
