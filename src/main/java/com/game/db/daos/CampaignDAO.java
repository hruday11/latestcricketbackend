package com.game.db.daos;

import com.game.api.CampaignDetails;
import com.game.api.Team;
import com.game.util.CampaignMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CampaignDAO {
    /**
     * The collection of Teams
     */
    final MongoCollection<Document> campaignCollection;

    private String campaignId = "campaignId";

    /**
     * Constructor.
     *
     * @param campaignCollection the collection of teams.
     */
    public CampaignDAO(final MongoCollection<Document> campaignCollection) {
        this.campaignCollection = campaignCollection;
    }

    public CampaignDetails getOneByCampaignName(final String id) {
        final Optional<Document> campaignFind = Optional.ofNullable(campaignCollection.find(new Document(campaignId, id)).first());
        return campaignFind.map(CampaignMapper::map).orElse(null);
    }

    /**
     * Find all teams.
     *
     * @return the teams.
     */

    public List<CampaignDetails> getAll() {
        final MongoCursor<Document> campaigns = campaignCollection.find().iterator();
        final List<CampaignDetails> campaignsList = new ArrayList<>();
        try {
            while (campaigns.hasNext()) {
                final Document campaign = campaigns.next();
                campaignsList.add(CampaignMapper.map(campaign));
            }
        } finally {
            campaigns.close();
        }
        return campaignsList;
    }

    public Object saveCampaignToDb(CampaignDetails campaignDetails){
        final Document saveCampaign;

        saveCampaign = new Document(
                "campaignName", campaignDetails.getCampaignName())
                .append("tips", campaignDetails.getTips())
                .append("advertisement", campaignDetails.getAdvertisement())
                .append("campaignActions", campaignDetails.getCampaignActions())
                .append(campaignId, campaignDetails.getCampaignId())
                .append("lifeManagement", campaignDetails.getLifeManagement())
                .append("stadium", campaignDetails.getStadium());
        return saveCampaign;
    }

    public CampaignDetails save(final CampaignDetails campaignDetails){
        final Optional<Document> campaignFind = Optional.ofNullable(campaignCollection.find(new Document(campaignId, campaignDetails.getCampaignId())).first());
        if (campaignFind.isPresent())
        {
            return null;
        }else{
            Object saveCampaign = saveCampaignToDb(campaignDetails);
            campaignCollection.insertOne((Document) saveCampaign);
            return getOneByCampaignName(campaignDetails.getCampaignId());
        }
    }

}
