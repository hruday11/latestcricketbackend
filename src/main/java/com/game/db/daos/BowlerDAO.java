package com.game.db.daos;

import com.game.api.Bowler;
import com.game.util.BowlerMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.validation.constraints.NotNull;
import java.util.*;

public class BowlerDAO {

    /**
     * The collection of bowler
     */
    final MongoCollection<Document> bowlerCollection;

    private String campaignId = "campaignId";

    private String teamId = "teamId";

    /**
     * Constructor.
     *
     * @param bowlerCollection the collection of bowlers.
     */
    public BowlerDAO(final MongoCollection<Document> bowlerCollection) {
        this.bowlerCollection = bowlerCollection;
    }

    /**
     * Find all bowlers.
     *
     * @return the bowlers.
     */

    public List<Bowler> getAll(String id, ObjectId tId) {
        final MongoCursor<Document> bowlers = bowlerCollection.find(new Document(campaignId, id).append(teamId, tId)).iterator();
        final List<Bowler> bowlerFind = new ArrayList<>();
        try {
            while (bowlers.hasNext()) {
                final Document bowler = bowlers.next();
                bowlerFind.add(BowlerMapper.map(bowler));
            }
        } finally {
            bowlers.close();
        }
        return bowlerFind;
    }

    /**
     * Get one document find in other case return null.
     *
     * @param id the identifier for find.
     * @return the Bowler find.
     */
    public Bowler getOne(final ObjectId id) {
        final Optional<Document> bowlerFind = Optional.ofNullable(bowlerCollection.find(new Document("_id", id)).first());
        return bowlerFind.map(BowlerMapper::map).orElse(null);
    }

    public Object saveBowlerToDb(Bowler bowler){
        final Document saveBowler;
        saveBowler = new Document(
                "name", bowler.getName())
                .append("type", bowler.getType())
                .append(campaignId, bowler.getCampaignId())
                .append("character", bowler.getCharacter())
                .append("image", bowler.getImage())
                .append("description", bowler.getDescription())
                .append(teamId, bowler.getTeamId())
                .append("speed", bowler.getSpeed());
        return saveBowler;
    }

    public Object save(final Bowler bowler){
        final Optional<Document> bowlerFind = Optional.ofNullable(bowlerCollection.find(new Document("name", bowler.getName()).append(campaignId, bowler.getCampaignId()).append(teamId,bowler.getTeamId())).first());
        if (bowlerFind.isPresent())
        {
            return "Bowler with same name exists";
        }else{
            Object saveBowler = saveBowlerToDb(bowler);
            bowlerCollection.insertOne((Document) saveBowler);
            return saveBowler;
        }
    }

    public Bowler update(final @NotNull ObjectId id, final Bowler bowler) {
        Object saveBowler = saveBowlerToDb(bowler);
        bowlerCollection.updateOne(new Document("_id", id),
                new Document("$set", saveBowler)
        );

        return getOne(id);
    }

    public void delete(final ObjectId id){
        bowlerCollection.deleteOne(new Document("_id", id));
    }


}
