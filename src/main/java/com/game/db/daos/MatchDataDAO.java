package com.game.db.daos;

import com.game.api.gameplaypojos.MatchData;
import com.game.api.Player;
import com.game.util.MatchDataMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MatchDataDAO {

    final MongoCollection<Document> matchCollection;

    final PlayerDAO playerDAO;

    private static final Logger LOGGER = LoggerFactory.getLogger(MatchDataDAO.class);


    public MatchDataDAO(MongoCollection<Document> matchCollection, PlayerDAO playerDAO) {
        this.matchCollection = matchCollection;
        this.playerDAO = playerDAO;
    }

    public MatchData getOne(ObjectId id){
        final Optional<Document> matchFind = Optional.ofNullable(matchCollection.find(new Document("_id", id)).first());
        return matchFind.map(MatchDataMapper::map).orElse(null);
    }

    public MatchData getOneByPlayerId(String id){
        final Optional<Document> matchFind = Optional.ofNullable(matchCollection.find(new Document("playerId", id)).first());
        return matchFind.map(MatchDataMapper::map).orElse(null);
    }

    /**
     * Find all teams.
     *
     * @return the teams.
     */

    public List<MatchData> getAll() {
        final MongoCursor<Document> matches = matchCollection.find().iterator();
        final List<MatchData> matchFind = new ArrayList<>();
        try {
            while (matches.hasNext()) {
                final Document match = matches.next();
                    matchFind.add(MatchDataMapper.map(match));
            }
        } finally {
            matches.close();
        }
        return matchFind;
    }



    public List<MatchData> getNewMatches(){
        final MongoCursor<Document> matches = matchCollection.find().iterator();
        final List<MatchData> matchFind = new ArrayList<>();
        try {
            while (matches.hasNext()) {
                final Document match = matches.next();
                MatchData matchData =  MatchDataMapper.map(match);
                if(matchData.getMatchStatus().equals("New")){
                    matchFind.add(MatchDataMapper.map(match));
                }
            }
        } finally {
            matches.close();
        }
        return matchFind;
    }

    public Object saveMatchToDb(MatchData matchDataCopy){
        final Document saveMatch;
        saveMatch = new Document(
                "playerId", matchDataCopy.getPlayerId())
                .append("opponentId", matchDataCopy.getOpponentId())
                .append("playerData", matchDataCopy.getPlayerData())
                .append("opponentData", matchDataCopy.getOpponentData())
                .append("matchStatus", matchDataCopy.getMatchStatus())
                .append("count", matchDataCopy.getCount())
                .append("won", matchDataCopy.getWon())
                .append("isBot", matchDataCopy.isBot())
                .append("botId", matchDataCopy.getBotId());
        return saveMatch;
    }

    public Object updateCount(MatchData matchData){
        final Document saveMatch;
        saveMatch = new Document(
                "count",matchData.getCount());
        return saveMatch;
    }


    public MatchData update(final @NotNull ObjectId id, final MatchData matchDataCopy) {
        Object saveMatch = saveMatchToDb(matchDataCopy);
        LOGGER.info("update : {}", saveMatch);
        matchCollection.updateOne(new Document("_id", id),
                new Document("$set", saveMatch)
        );
        return getOne(id);
    }

    public MatchData findAndModify(final @NotNull ObjectId id, final String tokenId, final String campaignId){
        Bson condition = Filters.and (Filters.eq("opponentId", null), Filters.eq("_id", id));

        final Optional<Document> matchFind = Optional.ofNullable(matchCollection.find(condition).first());
        MatchData matchData = matchFind.map(MatchDataMapper::map).orElse(null);

        if(matchData.getOpponentId() == null){
            matchData.setOpponentId(tokenId);
            matchData.setMatchStatus("PLAYING");
            matchCollection.findOneAndUpdate(
                    new Document("_id", id),
                    new Document("$set", matchData)
            );
        }else{
            Player player = playerDAO.getOne(tokenId);
            player.setStatus("PLAYING");
            playerDAO.updateStatusDAO(tokenId, player);
            return (MatchData) startGame(tokenId, null, campaignId);
        }
        return getOne(id);
    }

    public MatchData setCount(final @NotNull ObjectId id, final MatchData matchDataCopy) {
        Object saveMatch = updateCount(matchDataCopy);
        matchCollection.updateOne(new Document("_id", id),
                new Document("$set", saveMatch)
        );
        return getOne(id);
    }

    /**
     * Get one document find in other case return null
     * @param id the identifier for find.
     * @return the Match find.
     */
    public boolean isTeamAvail(final ObjectId id) {
        final Optional<Document> matchFind = Optional.ofNullable(matchCollection.find(new Document("_id", id)).first());
        return matchFind.isPresent();
    }

    public void updatePlayerStatus(final String playerId){
        Player player = playerDAO.getOne(playerId);
        player.setStatus("PLAYING");
        playerDAO.updateStatusDAO(playerId, player);
    }

    public Object startGame(final String reqPlayerId, final String oppPlayerId, final String campaignId){
        final Document saveMatch =new Document(
                "playerId", reqPlayerId)
                .append("opponentId", oppPlayerId)
                .append("playerData", null)
                .append("opponentData", null)
                .append("matchStatus", "New")
                .append("count", 1)
                .append("isBot", false)
                .append("botId", null)
                .append("won", null)
                .append("campaignId", campaignId);
        matchCollection.insertOne(saveMatch);
        return getOne((ObjectId) saveMatch.get("_id"));
    }

    public MatchData updateMatchDataToDB(MatchData matchDataCopy, ObjectId id){
        Object saveMatch = saveMatchToDb(matchDataCopy);
        matchCollection.updateOne(
                new Document("_id", id),
                new Document("$set", saveMatch)
        );
        return getOne(id);
    }

}
