package com.game.db.daos;

import com.game.api.gameplaypojos.PlayersGameData;
import com.game.util.PlayersMatchDataMapper;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.*;

public class PlayersGameDataDAO {
    /**
     * The collection of Players
     */
    final MongoCollection<Document> playersGameDataCollection;

    final MatchDataDAO match;

    /**
     * Constructor.
     *
     * @param playersGameDataCollection the collection of PlayerGameData.
     */
    public PlayersGameDataDAO(final MongoCollection<Document> playersGameDataCollection,final MatchDataDAO match) {
        this.playersGameDataCollection = playersGameDataCollection;
        this.match = match;
    }

    public PlayersGameData getOne(String playerId, ObjectId matchId){
        final Optional<Document> playersGameData = Optional.ofNullable(playersGameDataCollection.find(new Document("playerId", playerId).append("gameId", matchId)).first());
        return playersGameData.map(PlayersMatchDataMapper::map).orElse(null);
    }

    public PlayersGameData getOneByOpponentId(String playerId){
        final Optional<Document> playersGameData = Optional.ofNullable(playersGameDataCollection.find(new Document("opponentId", playerId)).first());
        return playersGameData.map(PlayersMatchDataMapper::map).orElse(null);
    }

    public PlayersGameData getOneByPlayerId(String playerId){
        final Optional<Document> playersGameData = Optional.ofNullable(playersGameDataCollection.find(new Document("playerId", playerId)).first());
        return playersGameData.map(PlayersMatchDataMapper::map).orElse(null);
    }


    public PlayersGameData getOneByMatchId(ObjectId id){
        final Optional<Document> playersGameData = Optional.ofNullable(playersGameDataCollection.find(new Document("gameId", id)).first());
        return playersGameData.map(PlayersMatchDataMapper::map).orElse(null);
    }

    /**
     * Find all players.
     *
     * @return the players.
     */

    public Object savePlayersData(final String playerId, final ObjectId gameId,final PlayersGameData data){
        final Document savedPlayerData = getPlayerData(playerId, gameId, data);
        if(getOne(playerId,gameId ) == null){
            playersGameDataCollection.insertOne((Document) savedPlayerData);
        }
        return getOne(playerId, gameId);
    }

    public Document getPlayerData(final String playerId, final ObjectId gameId,final PlayersGameData data){
        final Document playerData;
        playerData = new Document(
                "playerId", playerId)
                .append("gameId", gameId)
                .append("playerData", data.getPlayerData())
                .append("opponentId", data.getOpponentId())
                .append("currentBall", data.getCurrentBall())
                .append("matchStatus", "New")
                .append("won", null);
        return playerData;
    }

    public  Object updateplayersGameDatToDB(ObjectId id, final String playerId, final ObjectId gameId,final PlayersGameData data) {
        final Document savedPlayerData = getPlayerData(playerId, gameId, data);
        playersGameDataCollection.updateOne(
                new Document("_id", id),
                new Document("$set", savedPlayerData)
        );
        return getOne(playerId, gameId);
    }
}
