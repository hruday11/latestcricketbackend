package com.game.db.daos;

import com.game.api.Team;
import com.game.util.TeamMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class TeamDAO {

    /**
     * The collection of Teams
     */
    final MongoCollection<Document> teamCollection;

    private static final Logger LOGGER = LoggerFactory.getLogger(TeamDAO.class);

    /**
     * Constructor.
     *
     * @param teamCollection the collection of teams.
     */
    public TeamDAO(final MongoCollection<Document> teamCollection) {
        this.teamCollection = teamCollection;
    }

    /**
     * Find all teams.
     *
     * @return the teams.
     */

    public List<Team> getAll(String campaignId) {
        final MongoCursor<Document> teams = teamCollection.find(new BasicDBObject("campaignId", campaignId)).iterator();
        final List<Team> teamFind = new ArrayList<>();
        try {
            while (teams.hasNext()) {
                final Document team = teams.next();
                teamFind.add(TeamMapper.map(team));
            }
        } finally {
            teams.close();
        }
        return teamFind;
    }


    /**
     * Find all teams.
     *
     * @return the teams.
     */

    public List<Team> getScores(String campaignId) {
        final MongoCursor<Document> teams = teamCollection.find(new BasicDBObject("campaignId", campaignId)).sort(new BasicDBObject("playerCount", -1)).iterator();
        final List<Team> teamFind = new ArrayList<>();
        try {
            while (teams.hasNext()) {
                final Document team = teams.next();
                teamFind.add(TeamMapper.map(team));
            }
        } finally {
            teams.close();
        }
        return teamFind;
    }
    /**
     * Get one document find in other case return null.
     *
     * @param id the identifier for find.
     * @return the Team find.
     */
    public Team getOne(final ObjectId id) {
        final Optional<Document> teamFind = Optional.ofNullable(teamCollection.find(new Document("_id", id)).first());
        return teamFind.map(TeamMapper::map).orElse(null);
    }

    public Team getOneByTeamName(final String teamName) {
        final Optional<Document> teamFind = Optional.ofNullable(teamCollection.find(new Document("teamName", teamName)).first());
        return teamFind.map(TeamMapper::map).orElse(null);
    }

    public Object saveTeamToDb(Team team){
        final Document saveTeam;
        saveTeam = new Document(
                "name", team.getName())
                .append("playerCount", team.getPlayerCount())
                .append("score", team.getScore())
                .append("campaignId",team.getCampaignId())
                .append("teamLogo",team.getTeamLogo())
                .append("sponsorLogo",team.getSponsorLogo())
                .append("bowlerList",team.getBowlerList())
                .append("jerseyColor", team.getJerseyColor());
        return saveTeam;
    }

    public Team save(final Team team){
        final Optional<Document> teamFind = Optional.ofNullable(teamCollection.find(new Document("teamName", team.getName())).first());
        LOGGER.info("teamFind:{}", teamFind.isPresent());
        if (teamFind.isPresent())
        {
            return null;
        }else{
            Object saveTeam = saveTeamToDb(team);
            teamCollection.insertOne((Document) saveTeam);
            ObjectId id = ((Document) saveTeam).getObjectId("_id");
            LOGGER.info("id:{}", id);
            return getOne(id);
        }
    }

    public void update(final Team team, ObjectId id){
            Object saveTeam = saveTeamToDb(team);
            teamCollection.updateOne(new Document("_id", id),
                    new Document("$set", saveTeam)
            );
    }


    public void delete(final ObjectId id){
        teamCollection.deleteOne(new Document("_id", id));
    }

}
