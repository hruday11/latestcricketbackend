package com.game.db.daos;

import com.game.api.Player;
import com.game.api.Team;
import com.game.util.PlayerMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.util.*;

public class PlayerDAO {

    /**
     * The collection of Players
     */
    final MongoCollection<Document> playerCollection;

    final TeamDAO teamDAO;


    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerDAO.class);

    /**
     * Constructor.
     *
     * @param playerCollection the collection of players.
     */
    public PlayerDAO(final MongoCollection<Document> playerCollection,final TeamDAO teamDAO) {
        this.playerCollection = playerCollection;
        this.teamDAO = teamDAO;
    }

    /**
     * Find all players.
     *
     * @return the players.
     */

    public List<Player> getAll() {
        final MongoCursor<Document> players = playerCollection.find().iterator();
        final List<Player> playerFind = new ArrayList<>();
        try {
            while (players.hasNext()) {
                final Document player = players.next();
                playerFind.add(PlayerMapper.map(player));
            }
        } finally {
            players.close();
        }
        return playerFind;
    }

    /**
     * Get one document find in other case return null
     * @param id the identifier for find.
     * @return the Player find.
     */
    public Player getOne(final String id) {
        final Optional<Document> playerFind = Optional.ofNullable(playerCollection.find(new Document("tokenId", id)).first());
        return playerFind.map(PlayerMapper::map).orElse(null);
    }

    public Object savePlayerToDb(Player player){

//        Team team = teamDAO.getOne(player.getTeamId());
//        team.setPlayerCount(team.getPlayerCount() + 1);
//        teamDAO.update(team, team.getId());
        final Document savePlayer;
        savePlayer = new Document(
                "playerName", player.getPlayerName())
                .append("tokenId", player.getTokenId())
                .append("lives", player.getLives())
                .append("eloRating", player.getEloRating())
                .append("status", player.getStatus())
                .append("tutorialCompleted", player.isTutorialCompleted())
                .append("highScore", player.gethighScore())
                .append("stageNumber", player.getStageNumber())
                .append("campaignId", player.getCampaignId())
                .append("teamId", player.getId());
        return savePlayer;
    }

    /**
     * Find all players.
     *
     * @return the players.
     */

    public List<Player> getAllWithSameEloRating(final int eloRating, String campaignId) {
        final MongoCursor<Document> players = playerCollection.find(new Document("eloRating", eloRating).append("campaignId", campaignId)).iterator();
        final List<Player> PlayerFind = new ArrayList<>();
        try {
            while (players.hasNext()) {
                final Document player = players.next();
                if(player.getString("teamId") != null){
                    PlayerFind.add(PlayerMapper.map(player));
                }
            }
        } finally {
            players.close();
        }
        return PlayerFind;
    }

    public Object updatePlayerToDB(Player player){
        return new Document("playerName", player.getPlayerName())
                .append("teamId", player.getTeamId());
    }

    public Object updateStatus(Player player){
        return new Document("status",player.getStatus());
    }

    public Object save(final Player player){
        final Optional<Document> playerFind = Optional.ofNullable(playerCollection.find(new Document("tokenId", player.getTokenId())).first());
        if (playerFind.isPresent())
        {
            return "Player already registered";
        }else{
            Object savePlayer = savePlayerToDb(player);
            playerCollection.insertOne((Document) savePlayer);
            LOGGER.info("savePlayer : {}", player.getTokenId());
            return getOne((String) player.getTokenId());
        }
    }

    public Player update(final @NotNull String id, final Player updatePlayer) {
        Player savePlayer = getOne(id);
        savePlayer.setPlayerName(updatePlayer.getPlayerName());
        savePlayer.setTutorialCompleted(updatePlayer.isTutorialCompleted());
        if(updatePlayer.getTeamId() != savePlayer.getTeamId()){
            if(savePlayer.getTeamId() == null){
                Team newTeam = teamDAO.getOne(updatePlayer.getTeamId());
                newTeam.setPlayerCount(newTeam.getPlayerCount() + 1);
                savePlayer.setTeamId(newTeam.getId());
                teamDAO.update(newTeam, newTeam.getId());
            }else{
                Team oldTeam = teamDAO.getOne(savePlayer.getTeamId());
                oldTeam.setPlayerCount(oldTeam.getPlayerCount() - 1);
                teamDAO.update(oldTeam, oldTeam.getId());
                Team newTeam = teamDAO.getOne(updatePlayer.getTeamId());
                newTeam.setPlayerCount(newTeam.getPlayerCount() + 1);
                savePlayer.setTeamId(newTeam.getId());
                teamDAO.update(newTeam, newTeam.getId());
            }

        }
        playerCollection.updateOne(new Document("tokenId", id),
                new Document("$set", savePlayer)
        );

        return getOne(id);
    }

    public Player updateStatusDAO(final @NotNull String id, final Player player) {
        Object savePlayer = updateStatus(player);
        playerCollection.updateOne(new Document("tokenId", id),
                new Document("$set", savePlayer)
        );

        return getOne(id);
    }

}
