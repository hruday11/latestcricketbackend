package com.game.db;

import com.game.db.configuration.*;
import com.mongodb.*;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * This class has the porpuse of create a MongoDB client with their configuration.
 *
 * @author Hruday
 * @version 1.0.0
 * @since 1.0.0
 */
public class MongoDBFactoryConnection {

    /**
     * Logger class.
     */

    public static final Logger LOGGER = LoggerFactory.getLogger(MongoDBFactoryConnection.class);
    /**
     * The configuration for connect to MongoDB Server.
     */

    private MongoDBConnection mongoDBConnection;

    /**
     * Constructor.
     *
     * @param mongoDBConnection the mongoDB connection data.
     */
    public MongoDBFactoryConnection(final MongoDBConnection mongoDBConnection) {
        this.mongoDBConnection = mongoDBConnection;
    }

    /**
     * Gets the connection to MongoDB.
     *
     * @return the mongo Client.
     */
    public MongoClient getClient() {


        final String[] hosts = mongoDBConnection.getConnectionString().split(",");
        final List<ServerAddress> hostAddresses = Arrays.stream(hosts).map(h -> {
            String host[] = h.split(":");
                return new ServerAddress(host[0], Integer.parseInt(host[1]));
        }).collect(Collectors.toList());

        MongoClientOptions.Builder optionsBuilder = new MongoClientOptions.Builder();

        if (mongoDBConnection.getMaxWaitTimeInMilliSeconds() != 0) {
            optionsBuilder.maxWaitTime(mongoDBConnection.getMaxWaitTimeInMilliSeconds());
        }

        if (mongoDBConnection.getConnectTimeoutInMilliSeconds() != 0) {
            optionsBuilder.connectTimeout(mongoDBConnection.getConnectTimeoutInMilliSeconds());
        }

        if (mongoDBConnection.getSocketTimeoutInMilliSeconds() != 0) {
            optionsBuilder.socketTimeout(mongoDBConnection.getSocketTimeoutInMilliSeconds());
        }

        if (mongoDBConnection.getMaxConnectionsPerHost() != 0) {
            optionsBuilder.connectionsPerHost(mongoDBConnection.getMaxConnectionsPerHost());
        }

        CodecRegistry pojoCodecRegistry = org.bson.codecs.configuration.CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), org.bson.codecs.configuration.CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        optionsBuilder.codecRegistry(pojoCodecRegistry);

        MongoClientOptions options = optionsBuilder.build();

        MongoClient mongoClient;
        mongoClient = new MongoClient(hostAddresses, options);

        return mongoClient;
    }
    

}
