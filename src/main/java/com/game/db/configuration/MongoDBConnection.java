package com.game.db.configuration;


import java.util.Objects;

public class MongoDBConnection {

    private String connectionString;

    private String database;

    private int maxWaitTimeInMilliSeconds;

    private int connectTimeoutInMilliSeconds;

    private int socketTimeoutInMilliSeconds;

    private int maxConnectionsPerHost;




    public int getConnectTimeoutInMilliSeconds() {
        return connectTimeoutInMilliSeconds;
    }

    public int getMaxConnectionsPerHost() {
        return maxConnectionsPerHost;
    }


    public int getMaxWaitTimeInMilliSeconds() {
        return maxWaitTimeInMilliSeconds;
    }

    public int getSocketTimeoutInMilliSeconds() {
        return socketTimeoutInMilliSeconds;
    }

    public String getConnectionString() {
        return connectionString;
    }

    public String getDatabase() {
        return database;
    }


    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    public void setConnectTimeoutInMilliSeconds(int connectTimeoutInMilliSeconds) {
        this.connectTimeoutInMilliSeconds = connectTimeoutInMilliSeconds;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public void setMaxConnectionsPerHost(int maxConnectionsPerHost) {
        this.maxConnectionsPerHost = maxConnectionsPerHost;
    }

    public void setMaxWaitTimeInMilliSeconds(int maxWaitTimeInMilliSeconds) {
        this.maxWaitTimeInMilliSeconds = maxWaitTimeInMilliSeconds;
    }

    public void setSocketTimeoutInMilliSeconds(int socketTimeoutInMilliSeconds) {
        this.socketTimeoutInMilliSeconds = socketTimeoutInMilliSeconds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MongoDBConnection that = (MongoDBConnection) o;
        return maxWaitTimeInMilliSeconds == that.maxWaitTimeInMilliSeconds &&
                connectTimeoutInMilliSeconds == that.connectTimeoutInMilliSeconds &&
                socketTimeoutInMilliSeconds == that.socketTimeoutInMilliSeconds &&
                maxConnectionsPerHost == that.maxConnectionsPerHost &&
                Objects.equals(connectionString, that.connectionString) &&
                Objects.equals(database, that.database);
    }

    @Override
    public int hashCode() {
        return Objects.hash(connectionString, database, maxWaitTimeInMilliSeconds, connectTimeoutInMilliSeconds, socketTimeoutInMilliSeconds, maxConnectionsPerHost);
    }


    @Override
    public String toString() {
        return "MongoDBConnection{" +
                "connectionString='" + connectionString + '\'' +
                ", database='" + database + '\'' +
                ", maxWaitTimeInMilliSeconds=" + maxWaitTimeInMilliSeconds +
                ", connectTimeoutInMilliSeconds=" + connectTimeoutInMilliSeconds +
                ", socketTimeoutInMilliSeconds=" + socketTimeoutInMilliSeconds +
                ", maxConnectionsPerHost=" + maxConnectionsPerHost +
                '}';
    }
}
