package com.game.db.configuration;

import java.util.Objects;

public class Seed {

    public String host;

    public int port;

    public Seed(){
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        final Seed seed = (Seed) o;
        return port == seed.port &&
                Objects.equals(host, seed.host);
    }

    @Override
    public int hashCode(){
        return Objects.hash(host, port);
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "Seed{" +
                "host='" + host + '\'' +
                ", port=" + port +
                '}';
    }
}
