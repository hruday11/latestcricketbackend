package com.game.db.configuration;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.game.util.PasswordSerializer;

import java.util.Arrays;
import java.util.Objects;

public class Credentials {

    private String username;

    @JsonSerialize(using = PasswordSerializer.class)
    private char[] password;

    public Credentials(){
    }

    @Override
    public int hashCode(){
        int result = Objects.hash(username);
        result = 31 * result + Arrays.hashCode(password);
        return result;
    }

    public String getUsername(){
        return username;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(final char[] password){
        this.password = password;
    }

    @Override
    public String toString(){
        return "Credentials{"
                +"username='" + username + '\''
                + ".password="  + Arrays.toString(password)
                +'}';
    }
}
