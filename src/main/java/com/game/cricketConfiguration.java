package com.game;
import com.game.db.configuration.MongoDBConnection;
import io.dropwizard.Configuration;


/**
 * This class is a representation of YAML file configuration.
 * @version 1.0.0
 * @since 1.0.0
 */
public class CricketConfiguration extends Configuration {
    /**
     * The data configuration for MongoDB.
     */

    private MongoDBConnection mongoDBConnection;

    private String flipkartUrl;


    public String getFlipkartUrl(){
        return flipkartUrl;
    }

    public void setFlipkartUrl(String flipkartUrl) {
        this.flipkartUrl = flipkartUrl;
    }

    /**
     * Gets the mongoDBConnection.
     *
     * @return the value mongoDBConnection.
     */
    public MongoDBConnection getMongoDBConnection() {
        return mongoDBConnection;
    }

    /**
     * Sets the mongoDBConnection.
     *
     * @param mongoDBConnection value.
     */
    public void setMongoDBConnection(final MongoDBConnection mongoDBConnection) {
        this.mongoDBConnection = mongoDBConnection;
    }

}
