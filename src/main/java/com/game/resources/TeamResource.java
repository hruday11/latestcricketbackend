package com.game.resources;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.game.api.Team;
import com.game.db.daos.TeamDAO;
import com.mongodb.BasicDBObject;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import com.mongodb.operation.OrderBy;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class has receive all REST request and process it.
 *
 * @author Hruday
 * @version 1.0.0
 * @since 1.0.0
 */
@Api(value = "teams",
        description = "Teams REST API for handle player CRUD operations on teams collection.",
        tags = {"teams"})
@Path("/teams")
@Produces(MediaType.APPLICATION_JSON)
public class TeamResource {

    /**
     * Logger class.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(TeamResource.class);

    /**
     * DAO team.
     */
    private TeamDAO teamDAO;

    /**
     * Constructor.
     *
     * @param teamDAO the dao team.
     */
    public TeamResource(final TeamDAO teamDAO) {
        this.teamDAO = teamDAO;
    }

    /**
     * Get all {@link com.game.api.Team} objects.
     *
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "Teams not found")
    })
    @GET
    @Path("/{campaignId}")
    public Response all(@ApiParam(value = "campaignId") @PathParam("campaignId") @NotNull final String campaignId) {
        LOGGER.info("List all teams.");
        final List<Team> teamsFind = teamDAO.getAll(campaignId);
        if (teamsFind.isEmpty()) {
            return Response.accepted(new com.game.api.Response("Teams not found."))
                    .status(Response.Status.NOT_FOUND)
                    .build();
        }
        return Response.ok(teamsFind).build();
    }



//    /**
//     * Get a {@link Team} by identifier.
//     *
//     * @param id the identifier.
//     * @return A object {@link Response} with the information of result this method.
//     */
//    @ApiResponses(value = {
//            @ApiResponse(code = 200, message = "Operation success."),
//            @ApiResponse(code = 404, message = "Teams not found")
//    })
//    @GET
//    @Path("/{id}")
//    public Response getOne(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId id) {
//        LOGGER.info("Find the Player by identifier : " + id.toString());
//        final Team team = teamDAO.getOne(id);
//        if (team != null) {
//            return Response.ok(team).build();
//        }
//        return Response.accepted(new com.game.api.Response("Team not found.")).build();
//    }

    /**
     * Persis a {@link Team} object in a collection.
     *
     * @param team The objecto to persist.
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 201, message = "Team created successfully")
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(@ApiParam(value = "Team") @NotNull final Team team) {
        LOGGER.info("Persist a team in collection with the information: {}", team);
        Object teamData = teamDAO.save(team);
        if(teamData == null) {
            teamData = "Team already Exists, try new name";
        }
        return Response.ok(teamData).build();
    }

//    /**
//     * Update the information of a {@link Team}.
//     *
//     * @param id    The identifier.
//     * @param team the team information.
//     * @return A object {@link Response} with the information of result this method.
//     */
//    @ApiResponses(value = {
//            @ApiResponse(code = 200, message = "Operation success.")
//    })
//    @PUT
//    @Path("/{id}")
//    public Response update(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId id,
//                           @ApiParam(value = "Team") @NotNull final Team team) {
//        LOGGER.info("Update the information of a team : {} ", team);
//        Object playerData = teamDAO.update(id, team);
//        return Response.ok(playerData).build();
//    }

    /**
     * Delete a {@link Team} object.
     * @param id   the identifier.
     * @return  A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success.")
    })
    @DELETE
    @Path("/{id}")
    public Response delete(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId id) {
        LOGGER.info("Delete a team from collection with identifier: " + id.toString());
        teamDAO.delete(id);
        return Response.ok().build();
    }
}