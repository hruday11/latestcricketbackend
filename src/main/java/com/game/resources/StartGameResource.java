package com.game.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.api.*;
import com.game.api.fkpojos.*;
import com.game.api.gameplaypojos.*;
import com.game.api.gameplaypojos.PlayersGameData;
import com.game.api.gameplaypojos.responsepojos.BowlerPlayerData;
import com.game.api.gameplaypojos.responsepojos.ScoreBoardData;
import com.game.api.gameplaypojos.responsepojos.SelectedBowlerData;
import com.game.CricketConfiguration;
import com.game.db.daos.*;
import com.game.util.BallPath;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.net.www.protocol.http.HttpURLConnection;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

@Api(value = "start",
        tags = {"startGame"})
@Path("/stage")
@Produces(MediaType.APPLICATION_JSON)
public class StartGameResource {
    /**
     * Logger class.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(StartGameResource.class);

    /**
     * DAO matchData.
     */
    private MatchDataDAO matchDataDAO;
    private PlayerDAO playerDAO;
    private BowlerDAO bowlerDAO;
    private BowlerResource bowlerResource;
    private TeamDAO teamDAO;
    private CricketConfiguration configuration;





    private PlayersGameDataDAO playersGameDataDAO;

    /**
     * Constructor.
     *
     * @param matchDataDAO the dao matchData.
     * @param playerDAO the dao for playerData
     */
    public StartGameResource(final MatchDataDAO matchDataDAO,final PlayerDAO playerDAO, final BowlerDAO bowlerDAO, final  PlayersGameDataDAO playersGameDataDAO, final CricketConfiguration configuration,
                             final TeamDAO teamDAO) {
        this.matchDataDAO = matchDataDAO;
        this.playerDAO = playerDAO;
        this.bowlerDAO =  bowlerDAO;
        this.playersGameDataDAO = playersGameDataDAO;
        this.configuration = configuration;
        this.teamDAO = teamDAO;
    }


    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 201, message = "Bowler updated successfully")
    })
    @PUT
    @Path("updateBowler/{id}")
    public Response updateBowlerToGameDB(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId gameId,
                                         @ApiParam(value = "SelectBowler") final SelectBowler selectBowler) throws JsonProcessingException {

        if(matchDataDAO.isTeamAvail(gameId)) {
            MatchData matchData = matchDataDAO.getOne(gameId);
            String playerId = selectBowler.getPlayerId();
            String opponentId = selectBowler.getOpponentId();
            PlayersGameData playerGameData = playersGameDataDAO.getOne(selectBowler.getPlayerId(), matchData.getId());
            PlayersGameData oponentGameData = null;
            if(playerGameData != null) {
                oponentGameData = playersGameDataDAO.getOne(playerGameData.getOpponentId(), matchData.getId());
            }

            int playerOverSize = 0;
            int opponentOverSize = 0;

            if (playerGameData != null) {
                playerOverSize = playerGameData.getPlayerData().getOvers().size() - 1;
            }

            if(oponentGameData != null){
                opponentOverSize = oponentGameData.getPlayerData().getOvers().size() - 1;
            }

            SelectedBowlerData selectedBowlerData = new SelectedBowlerData();
            ArrayList<BowlerPlayerData> bowlerPlayerData = new ArrayList<BowlerPlayerData>();


            if(oponentGameData == null) {
                //create Player's data
                oponentGameData = new PlayersGameData();
                oponentGameData.setCurrentBall(0);
                oponentGameData.setGameId(matchData.getId());
                oponentGameData.setPlayerId(selectBowler.getOpponentId());
                oponentGameData.setOpponentId(selectBowler.getPlayerId());
                oponentGameData.setPlayerData(updatePlayerData(matchData, gameId, selectBowler));
                playersGameDataDAO.savePlayersData(selectBowler.getOpponentId(), matchData.getId(), oponentGameData);
                bowlerPlayerData.add(new BowlerPlayerData(opponentId,  bowlerDAO.getOne(new ObjectId(selectBowler.getBowlerId()))));
                if(playerGameData!=null && playerGameData.getPlayerData() != null){
                    bowlerPlayerData.add(new BowlerPlayerData(playerId,bowlerDAO.getOne(new ObjectId(playerGameData.getPlayerData().getOvers().get(playerOverSize).bowlerId))));
                }else{
                    bowlerPlayerData.add(new BowlerPlayerData(playerId, null));
                }
                selectedBowlerData.setBowlerData(bowlerPlayerData);
                return Response.ok(selectedBowlerData).build();
            } else if(oponentGameData.getPlayerData().getOvers().size() <= 3) {

                if(oponentGameData.getPlayerData().getOvers().get(opponentOverSize).getBallsData().size() == 0){
                    if(playerGameData == null || playerGameData.getPlayerData() == null ){
                        bowlerPlayerData.add(new BowlerPlayerData(playerId,null));
                        bowlerPlayerData.add(new BowlerPlayerData(opponentId, bowlerDAO.getOne(new ObjectId(oponentGameData.getPlayerData().getOvers().get(opponentOverSize).bowlerId))));
                        selectedBowlerData.setBowlerData(bowlerPlayerData);
                        return Response.ok(selectedBowlerData).build();
                    }else{
                        if(playerOverSize == opponentOverSize){
                            bowlerPlayerData.add(new BowlerPlayerData(playerId, bowlerDAO.getOne(new ObjectId(playerGameData.getPlayerData().getOvers().get(playerOverSize).bowlerId))));
                            bowlerPlayerData.add(new BowlerPlayerData(opponentId,bowlerDAO.getOne(new ObjectId(oponentGameData.getPlayerData().getOvers().get(opponentOverSize).bowlerId))));
                            selectedBowlerData.setBowlerData(bowlerPlayerData);
                            return Response.ok(selectedBowlerData).build();
                        }else{
                            bowlerPlayerData.add(new BowlerPlayerData(playerId,null));
                            bowlerPlayerData.add(new BowlerPlayerData(opponentId, bowlerDAO.getOne(new ObjectId(oponentGameData.getPlayerData().getOvers().get(opponentOverSize).bowlerId))));
                            selectedBowlerData.setBowlerData(bowlerPlayerData);
                            return Response.ok(selectedBowlerData).build();
                        }
                    }
                }else  if(oponentGameData.getPlayerData().getOvers().get(opponentOverSize).getBallsData().size() == 6){
                    if(oponentGameData.getPlayerData().getOvers().get(opponentOverSize).getBallsData().get(5).isScoreUpdated()){

                        Over over = updateOvers(selectBowler);
                        oponentGameData.getPlayerData().getOvers().add(over);
                        bowlerPlayerData.add(new BowlerPlayerData(opponentId,  bowlerDAO.getOne(new ObjectId(selectBowler.getBowlerId()))));
//                        playerOverSize =  playerOverSize - 1;
//                        opponentOverSize = oponentGameData.getPlayerData().getOvers().size();
                        if(playerGameData.getPlayerData() != null && playerGameData.getPlayerData().getOvers().size() == opponentOverSize){
                            bowlerPlayerData.add(new BowlerPlayerData(playerId,  bowlerDAO.getOne(new ObjectId(oponentGameData.getPlayerData().getOvers().get(playerOverSize).bowlerId))));
                        }else{
                            bowlerPlayerData.add( new BowlerPlayerData(playerId, null));
                        }

                        if(opponentOverSize >= 1){
                            int previousOverCheck = opponentOverSize;
                            if(oponentGameData.getPlayerData().getOvers().get(previousOverCheck).getBowlerId().equals(selectBowler.getBowlerId())){
                                return Response.status(Response.Status.BAD_REQUEST).entity("can't select previous over bowler again").header(HttpHeaders.CONTENT_TYPE,"text/plain" )
                                        .build();
                            }
                        }


                        playersGameDataDAO.updateplayersGameDatToDB(oponentGameData.getId(),oponentGameData.getPlayerId(), matchData.getId(), oponentGameData);
                        selectedBowlerData.setBowlerData(bowlerPlayerData);
                        return Response.ok(selectedBowlerData).build();
                    }else{
                        return Response.status(Response.Status.BAD_REQUEST).entity("previous over not completed").header(HttpHeaders.CONTENT_TYPE,"text/plain" ).build();
                    }
                }else{
                    return Response.status(Response.Status.BAD_REQUEST).entity("previous over not completed").header(HttpHeaders.CONTENT_TYPE,"text/plain" ).build();
                }
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).entity("match not found").header(HttpHeaders.CONTENT_TYPE,"text/plain" ).build();
    }


    /**
     * Update the information of a {@link MatchData}.
     * @return A object {@link} with the information of result  method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success.")
    })
    @PUT
    @Path("updateBotScore/{id}")
    public Response updateBotScore(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId gameId ,
                                   @ApiParam(value = "matchDataCopy") final Score score) {
        if(matchDataDAO.isTeamAvail(gameId)) {
            MatchData matchData = matchDataDAO.getOne(gameId);
            PlayersGameData playerGameData = playersGameDataDAO.getOne(score.getPlayerId(), matchData.getId());

            if(playerGameData == null) {
                return Response.ok("match not found").build();
            }
            PlayerData botGameData = playersGameDataDAO.getOne(playerGameData.getOpponentId(), matchData.getId()).getPlayerData();

            String playerId = playerGameData.getPlayerId();
            String opponentId = playerGameData.getOpponentId();
            PlayerData playerData = playerGameData.getPlayerData();
            int playerOverSize = 0;
            int opponentOverSize = 0;
            int playerBall = 0;

            if (playerData != null) {
                playerOverSize = playerData.getOvers().size() - 1;
                if(playerData.getOvers().get(playerOverSize).getBallsData().size() != 0){
                    playerBall = playerData.getOvers().get(playerOverSize).getBallsData().size() - 1;
                }
            }

            ScoreBoardData scoreBoardData = new ScoreBoardData();
            ArrayList<ScoreData> scoreDataList = new ArrayList<ScoreData>();
            BallData player1BallData = new BallData();
            BallData player2BallData = new BallData();
            if(playerData.getOvers().get(playerOverSize).getBallsData().size() > 0){
                if(playerId.equals(score.getPlayerId())){
                    if(playerData.getOvers().get(playerOverSize).getBallsData().size() > 0){
                        player1BallData = playerData.getOvers().get(playerOverSize).getBallsData().get(playerBall);
                    }

                    if(botGameData.getOvers().get(playerOverSize).getBallsData().size() > 0){
                        player2BallData = botGameData.getOvers().get(playerOverSize).getBallsData().get(playerBall);
                    }

                    CheckScore checkScore = checkShot(playerOverSize,score.getTime(), score.getHitDirection(), player1BallData.getPerfectPoint(), player1BallData.getDirection(), player1BallData.getExtreme(), player1BallData.getPerfectZone(), player1BallData.getSteps(), player1BallData.getDifficultModSteps(), player1BallData.isWicketInPathBool());
                    LOGGER.info("checkScore : {}", checkScore.getScore());
                    int total = playerData.getTotal();
                    double timingBonus = playerData.getTimingBonus();

                    if(!player1BallData.isScoreUpdated()){
                        total = playerData.getTotal() + checkScore.getScore();
                        timingBonus = (playerData.getTimingBonus() + checkScore.getTimeBonus());
                        playerData.setTimingBonus(timingBonus);
                        playerData.setTotal(total);
                        player1BallData.setHitTime(score.getTime());
                        player1BallData.setHitDirection(score.getHitDirection());
                        player1BallData.setScore(checkScore.getScore());
                        player1BallData.setBonusTime(checkScore.getTimeBonus());
                        player1BallData.setScoreUpdated(true);
                        if(checkScore.getScore() == 6){
                            playerData.setSixes(playerData.getSixes() + 1);
                        }else if(checkScore.getScore() == 6){
                            playerData.setFours(playerData.getFours() + 1);
                        }else if(checkScore.getScore() == -5){
                            playerData.setOut(playerData.getOut() + 1);
                        }
                        playersGameDataDAO.updateplayersGameDatToDB(playerGameData.getId(), playerGameData.getPlayerId(), playerGameData.getGameId(), playerGameData);
                    }
                    ScoreData playerBallData = new ScoreData(checkScore.getScore(),playerId,total, player1BallData.isScoreUpdated());
                    int totalBallsPlayedByPlayer = 0;
                    for(Over playerOver : playerGameData.getPlayerData().getOvers()) {
                        totalBallsPlayedByPlayer = totalBallsPlayedByPlayer + playerOver.getBallsData().size();
                    }

                    int currentBotBall = 1;
                    int botScore = 0;

                    for(Over botBall : botGameData.getOvers()) {
                        for(BallData ballData : botBall.getBallsData()) {
                            if(currentBotBall <= totalBallsPlayedByPlayer) {
                                botScore += ballData.getScore();
                                currentBotBall++;
                            }
                        }
                    }


                    ScoreData opponentBallData = new ScoreData(player2BallData.getScore(),opponentId,botScore, player2BallData.isScoreUpdated());
                    scoreDataList.add(playerBallData);
                    scoreDataList.add(opponentBallData);
                    scoreBoardData.setScoreBoardData(scoreDataList);
                    return Response.ok(scoreBoardData).build();

                }
            }else{
                return Response.ok("updateAgain").build();
            }


        }
        return Response.ok("previous ball updated").build();

    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 201, message = "Bowler updated successfully")
    })
    @PUT
    @Path("updateBotBowler/{id}")
    public Response updateBotBowlerToGameDB(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId gameId,
                                            @ApiParam(value = "SelectBowler") final SelectBowler selectBowler) throws JsonProcessingException {

        if(matchDataDAO.isTeamAvail(gameId)) {

            MatchData matchData = matchDataDAO.getOne(gameId);
            String playerId = selectBowler.getPlayerId();
            String opponentId = selectBowler.getOpponentId();
            PlayersGameData botGameData = playersGameDataDAO.getOne(selectBowler.getOpponentId(), matchData.getId());

            Bowler playerBowler =  null;

            PlayersGameData playerGameData = playersGameDataDAO.getOne(selectBowler.getPlayerId(), matchData.getId());
            final Team opponentTeam = teamDAO.getOne(playerDAO.getOne(opponentId).getTeamId());
            final List<Bowler> bowlersList = bowlerDAO.getAll(opponentTeam.getCampaignId(), opponentTeam.getId());

            if(playerGameData == null && botGameData != null) {
                playerBowler = bowlersList.get(0);
                playerGameData = new PlayersGameData();
                playerGameData.setCurrentBall(0);
                playerGameData.setGameId(matchData.getId());
                playerGameData.setPlayerId(selectBowler.getPlayerId());
                playerGameData.setOpponentId(selectBowler.getOpponentId());
                SelectBowler bowlerForPlayer = new SelectBowler();
                bowlerForPlayer.setOpponentId(playerBowler.getId().toString());
                bowlerForPlayer.setPlayerId(selectBowler.getOpponentId());
                bowlerForPlayer.setBowlerId(playerBowler.getId().toString());
                playerGameData.setPlayerData(updatePlayerData(matchData, gameId, bowlerForPlayer));
                botGameData.getPlayerData().getOvers().get(0).setBowlerId(selectBowler.getBowlerId());
                playersGameDataDAO.savePlayersData(playerGameData.getPlayerId(), matchData.getId(), playerGameData);
                playersGameDataDAO.savePlayersData(botGameData.getPlayerId(), matchData.getId(), botGameData);
                SelectedBowlerData selectedBowlerData = new SelectedBowlerData();
                ArrayList<BowlerPlayerData> bowlerPlayerData = new ArrayList<BowlerPlayerData>();

                bowlerPlayerData.add(new BowlerPlayerData(playerGameData.getPlayerId(),playerBowler));
                bowlerPlayerData.add(new BowlerPlayerData(playerGameData.getOpponentId(), bowlerDAO.getOne(new ObjectId(selectBowler.getBowlerId()))));
                selectedBowlerData.setBowlerData(bowlerPlayerData);
                return Response.ok(selectedBowlerData).build();
            }


            int playerOverSize = 0;

            if (playerGameData != null) {
                playerOverSize = playerGameData.getPlayerData().getOvers().size() - 1;
            }


            SelectedBowlerData selectedBowlerData = new SelectedBowlerData();
            ArrayList<BowlerPlayerData> bowlerPlayerData = new ArrayList<BowlerPlayerData>();

            if(playerGameData.getPlayerData().getOvers().size() <= 3) {

                if (playerGameData.getPlayerData().getOvers().get(playerOverSize).getBallsData().size() == 0) {
                    playerBowler = bowlersList.get(playerOverSize);
                    bowlerPlayerData.add(new BowlerPlayerData(playerId, playerBowler));
                    bowlerPlayerData.add(new BowlerPlayerData(opponentId, bowlerDAO.getOne(new ObjectId(botGameData.getPlayerData().getOvers().get(playerOverSize).bowlerId))));
                    selectedBowlerData.setBowlerData(bowlerPlayerData);
                    return Response.ok(selectedBowlerData).build();
                } else if (playerGameData.getPlayerData().getOvers().get(playerOverSize).getBallsData().size() == 6) {
                    playerBowler = bowlersList.get(playerOverSize);
                    Over playerOver = playerGameData.getPlayerData().getOvers().get(playerOverSize);
                    playerOver.setBowlerId(playerBowler.getId().toString());
                    SelectBowler bowlerForPlayer = new SelectBowler();
                    bowlerForPlayer.setBowlerId(playerBowler.getId().toString());
                    bowlerForPlayer.setPlayerId((String) playerGameData.getPlayerId());
                    bowlerForPlayer.setOpponentId((String) playerGameData.getOpponentId());
                    Over over = updateOvers(bowlerForPlayer);
                    playerGameData.getPlayerData().getOvers().add(over);

                    bowlerPlayerData.add(new BowlerPlayerData(playerGameData.getPlayerId(), playerBowler));
//                    playerOverSize = playerOverSize + 1;
//                    playerOverSize = playerGameData.getPlayerData().getOvers().size();

                    botGameData.getPlayerData().getOvers().get(playerOverSize).setBowlerId(selectBowler.getBowlerId());
                    bowlerPlayerData.add(new BowlerPlayerData(playerGameData.getOpponentId(), bowlerDAO.getOne(new ObjectId(selectBowler.getBowlerId()))));
                    LOGGER.info("ma");
                    playersGameDataDAO.updateplayersGameDatToDB(playerGameData.getId(), playerGameData.getPlayerId(), matchData.getId(), playerGameData);
                    playersGameDataDAO.updateplayersGameDatToDB(botGameData.getId(), botGameData.getPlayerId(), matchData.getId(), botGameData);
                    selectedBowlerData.setBowlerData(bowlerPlayerData);
                    return Response.ok(selectedBowlerData).build();

                } else {
                    return Response.ok("previous over not completed").build();
                }
            }
        }
        return Response.ok("match not found").build();
    }

    /**
     * Update the information of a {@link MatchData}.
     * @return A object {@link} with the information of result  method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success.")
    })
    @PUT
    @Path("updateScore/{id}")
    public Response updateScore(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId gameId ,
                                @ApiParam(value = "score") final Score score) {
        if(matchDataDAO.isTeamAvail(gameId)) {
            MatchData matchData = matchDataDAO.getOne(gameId);
            PlayersGameData playerGameData = playersGameDataDAO.getOne(score.getPlayerId(), matchData.getId());

            if(playerGameData == null) {
                return Response.ok("match not found").build();
            }
            PlayerData oponentGameData = playersGameDataDAO.getOne(playerGameData.getOpponentId(), matchData.getId()).getPlayerData();

            String playerId = playerGameData.getPlayerId();
            String opponentId = playerGameData.getOpponentId();
            PlayerData playerData = playerGameData.getPlayerData();
            PlayerData opponentData = matchData.getOpponentData();
            int playerOverSize = 0;
            int opponentOverSize = 0;
            int playerBall = 0;
            int opponentBall = 0;

            if (playerData != null) {
                playerOverSize = playerData.getOvers().size() - 1;
                if(playerData.getOvers().get(playerOverSize).getBallsData().size() != 0){
                    playerBall = playerData.getOvers().get(playerOverSize).getBallsData().size() - 1;
                }
            }
            if (oponentGameData != null) {
                opponentOverSize = oponentGameData.getOvers().size() - 1;
                if(oponentGameData.getOvers().get(opponentOverSize).getBallsData().size() != 0){
                    opponentBall = oponentGameData.getOvers().get(opponentOverSize).getBallsData().size() - 1;
                }
            }
            ScoreBoardData scoreBoardData = new ScoreBoardData();
            ArrayList<ScoreData> scoreDataList = new ArrayList<ScoreData>();
            BallData player1BallData = new BallData();
            BallData player2BallData = new BallData();
            if(opponentBall == playerBall){
                if(playerData.getOvers().get(playerOverSize).getBallsData().size() >0|| oponentGameData.getOvers().get(playerOverSize).getBallsData().size()>0){
                    if(playerId.equals(score.getPlayerId())){
                        if(playerData.getOvers().get(playerOverSize).getBallsData().size() > 0){
                            player1BallData = playerData.getOvers().get(playerOverSize).getBallsData().get(playerBall);
                        }

                        if(oponentGameData.getOvers().get(opponentOverSize).getBallsData().size() > 0){
                            player2BallData = oponentGameData.getOvers().get(opponentOverSize).getBallsData().get(opponentBall);
                        }

                        CheckScore checkScore = checkShot(playerOverSize, score.getTime(), score.getHitDirection(), player1BallData.getPerfectPoint(), player1BallData.getDirection(), player1BallData.getExtreme(), player1BallData.getPerfectZone(), player1BallData.getSteps(),player1BallData.getDifficultModSteps(), player1BallData.isWicketInPathBool());
                        int total = playerData.getTotal();
                        double timingScore = playerData.getTimingBonus();
                        if(!player1BallData.isScoreUpdated()){
                            total = playerData.getTotal() + checkScore.getScore();
                            timingScore = playerData.getTimingBonus() + checkScore.getTimeBonus();
                            playerData.setTimingBonus(timingScore);
                            playerData.setTotal(total);
                            player1BallData.setHitTime(score.getTime());
                            player1BallData.setHitDirection(score.getHitDirection());
                            player1BallData.setScore(checkScore.getScore());
                            player1BallData.setBonusTime(checkScore.getTimeBonus());
                            player1BallData.setScoreUpdated(true);
                            if(checkScore.getScore() == 6){
                                playerData.setSixes(playerData.getSixes() + 1);
                            }else if(checkScore.getScore() == 6){
                                playerData.setFours(playerData.getFours() + 1);
                            }else if(checkScore.getScore() == -5){
                                playerData.setOut(playerData.getOut() + 1);
                            }
                            playersGameDataDAO.updateplayersGameDatToDB(playerGameData.getId(), playerGameData.getPlayerId(), playerGameData.getGameId(), playerGameData);
                        }
                        if(playerBall == opponentBall){
                            ScoreData playerBallData = new ScoreData(checkScore.getScore(),playerId,total, player1BallData.isScoreUpdated());
                            ScoreData opponentBallData = new ScoreData(player2BallData.getScore(),opponentId,oponentGameData.getTotal(), player2BallData.isScoreUpdated());
                            scoreDataList.add(playerBallData);
                            scoreDataList.add(opponentBallData);
                            scoreBoardData.setScoreBoardData(scoreDataList);
                            return Response.ok(scoreBoardData).build();
                        }else{
                            ScoreData playerBallData = new ScoreData(checkScore.getScore(),playerId,total, player1BallData.isScoreUpdated());
                            ScoreData opponentBallData = new ScoreData(player2BallData.getScore(),opponentId,oponentGameData.getTotal(),player2BallData.isScoreUpdated());
                            scoreDataList.add(playerBallData);
                            scoreDataList.add(opponentBallData);
                            scoreBoardData.setScoreBoardData(scoreDataList);
                            return Response.ok(scoreBoardData).build();
                        }
                    }
                }
            }
           else{
                return Response.ok("updateAgain").build();
            }


        }
        return Response.ok("previous ball updated").build();


    }

    /**
     * @return A object {@link Response} with the information of result method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success.")
    })
    @PUT
    @Path("updateBall/{id}/{playerTokenId}")
    public Response updateBall(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId gameId ,
                               @ApiParam(value = "playerTokenId")  @PathParam("playerTokenId") @NotNull final String playerTokenId) {
        if(matchDataDAO.isTeamAvail(gameId)) {
            MatchData matchData = matchDataDAO.getOne(gameId);
            PlayersGameData playerGameData = playersGameDataDAO.getOne(playerTokenId, matchData.getId());

            if(playerGameData == null) {
                return Response.ok("match not found").build();
            }
            PlayerData opponentData = playersGameDataDAO.getOne(playerGameData.getOpponentId(), matchData.getId()).getPlayerData();

            String playerId = playerGameData.getPlayerId();
            String opponentId = playerGameData.getOpponentId();
            PlayerData playerData = playerGameData.getPlayerData();
            int playerOverSize = 0;
            int opponentOverSize = 0;

            if(playerData != null){
                playerOverSize = playerData.getOvers().size() - 1;
            }
            if(opponentData != null){
                opponentOverSize = opponentData.getOvers().size() - 1;
            }

            if(playerId.equals(playerTokenId)){
                if(playerData != null){
                    Over playerOver = playerGameData.getPlayerData().getOvers().get(playerOverSize);
                    Over opponentOver = opponentData.getOvers().get(opponentOverSize);
                    ObjectId bowlerId = new ObjectId(playerOver.getBowlerId());
                    if(playerGameData.getPlayerData().getOvers().size() >= 3 && playerOver.getBallsData().size() == 6) {
                        return Response.ok("gameOver").build();
                    }
//                    if(playerOver.getBallsData().size() >  opponentOver.getBallsData().size() ) {
//                        return Response.ok("updateScore").status(Response.Status.BAD_REQUEST).build();
//                    }

                    if(playerOver.getBallsData().size() <= 5){
                        if(playerOver.getBallsData().size() == 0){
                            BallData ballData = generateBallData(bowlerId);
                            Ball ball = generateBall(ballData);
                            playerOver.getBallsData().add(ballData);
                            playersGameDataDAO.updateplayersGameDatToDB(playerGameData.getId(), playerGameData.getPlayerId(), playerGameData.getGameId(), playerGameData);
                            return Response.ok(ball).build();
                        }else{
                            int previousBall = playerOver.getBallsData().size() - 1;
                            if(playerOver.getBallsData().get(previousBall).isScoreUpdated()){
                                BallData ballData = generateBallData(bowlerId);
                                Ball ball = generateBall(ballData);
                                playerOver.getBallsData().add(ballData);
                                playersGameDataDAO.updateplayersGameDatToDB(playerGameData.getId(), playerGameData.getPlayerId(), playerGameData.getGameId(), playerGameData);
                                return Response.ok(ball).build();
                            }else{
                                return Response.ok("score not updated for previous ball").status(Response.Status.BAD_REQUEST).build();
                            }
                        }
                    }else{
                        if(playerGameData.getPlayerData().getOvers().size() < 3){
                            return Response.ok("getBowler").build();
                        }else{
                            return Response.ok("gameOver").build();
                        }
                    }
                }
            }
        }

        return Response.ok("hi").build();
    }
//    /**
//     * Update the information of a {@link MatchData}.
//     * @return A object {@link Response} with the information of result  method.
//     */
//    @ApiResponses(value = {
//            @ApiResponse(code = 200, message = "Operation success.")
//    })
//    @PUT
//    @Path("updateBall/{id}/{playerTokenId}")
//    public Response updateBall(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId gameId ,
//                               @ApiParam(value = "playerTokenId")  @PathParam("playerTokenId") @NotNull final String playerTokenId) {
//        if(matchDataDAO.isTeamAvail(gameId)) {
//            MatchData matchData = matchDataDAO.getOne(gameId);
//            PlayersGameData playerGameData = playersGameDataDAO.getOne(playerTokenId, matchData.getId());
//            if(playerGameData == null) {
//                return Response.ok("match not found").build();
//            }
//            PlayerData opponentData = playersGameDataDAO.getOne(playerGameData.getOpponentId(), matchData.getId()).getPlayerData();
//
//            String playerId = playerGameData.getPlayerId();
//            String opponentId = playerGameData.getOpponentId();
//            PlayerData playerData = playerGameData.getPlayerData();
//            int playerOverSize = 0;
//            int opponentOverSize = 0;
//
//            if(playerData != null){
//                playerOverSize = playerData.getOvers().size() - 1;
//            }
//            if(opponentData != null){
//                opponentOverSize = opponentData.getOvers().size() - 1;
//            }
//
//            if(playerId.equals(playerTokenId)){
//                if(playerData != null){
//                    Over playerOver =  playerGameData.getPlayerData().getOvers().get(playerOverSize);
//                    Over opponentOver =  opponentData.getOvers().get(opponentOverSize);
//                    ObjectId bowlerId = new ObjectId(playerOver.getBowlerId());
//                    if(playerGameData.getPlayerData().getOvers().size() >= 3 && playerOver.getBallsData().size() == 6) {
//                        return Response.ok("gameOver").build();
//                    }
//                    if(playerOver.getBallsData().size() <= 5){
//                        if(playerOver.getBallsData().size() == 0){
//                            BallData ballData = generateBallData(bowlerId);
//                            Ball ball = generateBall(ballData);
//                            playerOver.getBallsData().add(ballData);
//                            playersGameDataDAO.updateplayersGameDatToDB(playerGameData.getId(), playerGameData.getPlayerId(), playerGameData.getGameId(), playerGameData);
//                            return Response.ok(ball).build();
//                        }else{
//                            int previousBall =  playerOver.getBallsData().size() - 1;
//                            int opponentBall = opponentOver.getBallsData().size() - 1;
//                            if(playerOver.getBallsData().get(previousBall).isScoreUpdated()){
//                                BallData ballData = generateBallData(bowlerId);
//                                Ball ball = generateBall(ballData);
//                                playerOver.getBallsData().add(ballData);
//                                playersGameDataDAO.updateplayersGameDatToDB(playerGameData.getId(), playerGameData.getPlayerId(), playerGameData.getGameId(), playerGameData);
//                                return Response.ok(ball).build();
//                            }else{
//                                return Response.ok("callBallAgain").build();
//                            }
//                        }
//                    }else{
//                        if(playerGameData.getPlayerData().getOvers().size() < 3){
//                            return Response.ok("getBowler").build();
//                        }else{
//                            return Response.ok("gameOver").build();
//                        }
//                    }
//                }
//            }
//        }
//
//        return Response.ok("hi").build();
//    }

    /**
     * Update the information of a {@link MatchData}.
     * @return A object {@link Response} with the information of result  method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success.")
    })
    @PUT
    @Path("updateBotBall/{id}/{playerTokenId}")
    public Response updateBotBall(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId gameId ,
                                  @ApiParam(value = "playerTokenId")  @PathParam("playerTokenId") @NotNull final String playerTokenId) {
        if(matchDataDAO.isTeamAvail(gameId)) {
            MatchData matchData = matchDataDAO.getOne(gameId);
            PlayersGameData playerGameData = playersGameDataDAO.getOne(matchData.getPlayerId(), matchData.getId());

            if(playerGameData == null) {
                return Response.ok("match not found").build();
            }
            PlayerData botData = playersGameDataDAO.getOne(playerGameData.getOpponentId(), matchData.getId()).getPlayerData();

            String playerId = playerGameData.getPlayerId();
            String opponentId = playerGameData.getOpponentId();
            PlayerData playerData = playerGameData.getPlayerData();
            int playerOverSize = 0;
            int opponentOverSize = 0;

            if(playerData != null){
                playerOverSize = playerData.getOvers().size() - 1;
            }
            if(botData != null){
                opponentOverSize = botData.getOvers().size() - 1;
            }

            if(playerId.equals(playerTokenId)){
                if(playerData != null){
                    Over playerOver =  playerGameData.getPlayerData().getOvers().get(playerOverSize);
                    Over opponentOver =  botData.getOvers().get(playerOverSize);
                    ObjectId bowlerId = new ObjectId(playerOver.getBowlerId());
                    if(playerGameData.getPlayerData().getOvers().size() >= 3 && playerOver.getBallsData().size() == 6) {
                        return Response.ok("gameOver").build();
                    }
                    if(playerOver.getBallsData().size() <= 5){
                        if(playerOver.getBallsData().size() == 0){

                            BallData ballData = botData.getOversBowled().get(playerOverSize).getBallsData().get(playerOver.getBallsData().size());
                            ballData.setScoreUpdated(false);
                            Ball ball = generateBall(ballData);
                            playerOver.getBallsData().add(ballData);
                            playersGameDataDAO.updateplayersGameDatToDB(playerGameData.getId(), playerGameData.getPlayerId(), playerGameData.getGameId(), playerGameData);
                            return Response.ok(ball).build();
                        }else{
                            int previousBall =  playerOver.getBallsData().size() - 1;
                            if(playerOver.getBallsData().get(previousBall).isScoreUpdated()){
                                BallData ballData = botData.getOversBowled().get(playerOverSize).getBallsData().get(playerOver.getBallsData().size()-1);
                                ballData.setScoreUpdated(false);
                                Ball ball = generateBall(ballData);
                                playerOver.getBallsData().add(ballData);
                                playersGameDataDAO.updateplayersGameDatToDB(playerGameData.getId(), playerGameData.getPlayerId(), playerGameData.getGameId(), playerGameData);
                                return Response.ok(ball).build();
                            }else{
                                return Response.ok("callBallAgain").build();
                            }
                        }
                    }else{
                        if(playerGameData.getPlayerData().getOvers().size() < 3){
                            return Response.ok("getBowler").build();
                        }else{
                            return Response.ok("gameOver").build();
                        }
                    }
                }
            }
        }

        return Response.ok("hi").build();
    }
    /**
     * update winner.
     * @return A object {@link Response} with the information of result  method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success.")
    })
    @PUT
    @Path("{matchId}/updateWinner")

    /**
     * Get cuurent ball count.
     * @return A object {@link Response} with the information of result  method.
     */
    public Response updateWinner(@ApiParam(value = "matchId") @PathParam("matchId") @NotNull final ObjectId gameId) {
        MatchData matchData = matchDataDAO.getOne(gameId);
        PlayersGameData playerGameData = playersGameDataDAO.getOne(matchData.getPlayerId(), matchData.getId());

        if(playerGameData == null) {
            return Response.ok("Player not found").build();
        }
        PlayersGameData opponentData = playersGameDataDAO.getOne(playerGameData.getOpponentId(), matchData.getId());
        int totalBallsPlayedByOpponent = 0;
        int totalBallsPlayedByPlayer = 0;
        for(Over data : playerGameData.getPlayerData().getOvers()) {
            totalBallsPlayedByOpponent = totalBallsPlayedByOpponent + data.getBallsData().size();
        }

        for(Over data : opponentData.getPlayerData().getOvers()) {
            totalBallsPlayedByPlayer = totalBallsPlayedByOpponent + data.getBallsData().size();
        }
//
//        if(totalBallsPlayedByPlayer > totalBallsPlayedByOpponent) {
//            matchData.setWon(playerGameData.getPlayerId());
//            matchData.setMatchStatus("ended");
//        } else if (totalBallsPlayedByOpponent > totalBallsPlayedByPlayer) {
//            matchData.setWon(opponentData.getPlayerId());
//            matchData.setMatchStatus("ended");
//        } else
        double playerScore = playerGameData.getPlayerData().getTotal() + playerGameData.getPlayerData().getTimingBonus();
        double opponentScore = opponentData.getPlayerData().getTotal() + playerGameData.getPlayerData().getTimingBonus();
        WinnerResponse winnerResponse = new WinnerResponse();
        if(totalBallsPlayedByOpponent == 18 && totalBallsPlayedByPlayer == 18){
            if(playerGameData.getPlayerData().getTotal() == opponentData.getPlayerData().getTotal()){
                playerScore = playerGameData.getPlayerData().getTotal() + 25 + playerGameData.getPlayerData().getTimingBonus();
                opponentScore = opponentData.getPlayerData().getTotal() + 25 + playerGameData.getPlayerData().getTimingBonus();
                winnerResponse.setWon("tied");
                matchData.setWon("tied");
                matchData.setMatchStatus("ended");
            }else if( playerGameData.getPlayerData().getTotal() > opponentData.getPlayerData().getTotal()) {
                matchData.setWon(playerGameData.getPlayerId());
                playerScore = 0;
                playerScore = playerGameData.getPlayerData().getTotal() + 50 + playerGameData.getPlayerData().getTimingBonus();
                winnerResponse.setWon(playerGameData.getPlayerId());
                matchData.setMatchStatus("ended");
            } else {
                opponentScore = 0;
                opponentScore = opponentData.getPlayerData().getTotal() + 50 + playerGameData.getPlayerData().getTimingBonus();
                matchData.setWon(opponentData.getPlayerId());
                winnerResponse.setWon(opponentData.getPlayerId());
                matchData.setMatchStatus("ended");
            }
            Player player = playerDAO.getOne(matchData.getPlayerId());
            player.setStatus("OFFLINE");
            Player opponentPlayer = playerDAO.getOne(matchData.getOpponentId());
            opponentPlayer.setStatus("OFFLINE");
            playerDAO.updateStatusDAO(matchData.getPlayerId(), player);
            playerDAO.updateStatusDAO(matchData.getOpponentId(), opponentPlayer);
            matchDataDAO.update(matchData.getId(), matchData);

            PlayerData playerData = playersGameDataDAO.getOneByPlayerId(matchData.getPlayerId()).getPlayerData();
            PlayerData opponentPlayerData = playersGameDataDAO.getOneByPlayerId(matchData.getOpponentId()).getPlayerData();
            int playerWinningBonus = matchData.getWon().equals(matchData.getPlayerId()) ? 50 : 0;
            int opponentWinningBonus = matchData.getWon().equals(matchData.getOpponentId()) ? 50 : 0;
            WinnerPlayerData winerPlayerData = new WinnerPlayerData(matchData.getPlayerId(), playerData.getTotal(),playerWinningBonus, playerData.getTimingBonus(), playerScore, playerData.getSixes(), playerData.getFours(), playerData.getOut());
            WinnerPlayerData winnerOpponentData = new WinnerPlayerData(matchData.getOpponentId(), opponentPlayerData.getTotal(),opponentWinningBonus,opponentPlayerData.getTimingBonus(),
                    opponentScore,opponentPlayerData.getSixes(), opponentPlayerData.getFours(), opponentPlayerData.getOut());
            ArrayList<WinnerPlayerData> winnerPlayerDataArrayList = new ArrayList<>();
            winnerPlayerDataArrayList.add(winerPlayerData);
            winnerPlayerDataArrayList.add(winnerOpponentData);
            winnerResponse.setPlayerData(winnerPlayerDataArrayList);
            return  Response.ok(winnerResponse).build();
        }
        return Response.ok("match yet to complete").status(Response.Status.BAD_REQUEST).build();
    }


    /**
     * update winner.
     * @return A object {@link Response} with the information of result  method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success.")
    })
    @PUT
    @Path("{matchId}/{campaignId}")

    /**
     * Get cuurent ball count.
     * @return A object {@link Response} with the information of result  method.
     */
    public Response updateWinnerWithCampaign(@ApiParam(value ="matchId")  @PathParam("matchId") @NotNull final ObjectId gameId,@ApiParam(value = "matchId") @NotNull final FKScoreBoard fkScoreBoard) {
        MatchData matchData = matchDataDAO.getOne(gameId);
        PlayersGameData playerGameData = playersGameDataDAO.getOne(matchData.getPlayerId(), matchData.getId());

        if(playerGameData == null) {
            return Response.ok("Player not found").build();
        }
        PlayersGameData opponentData = playersGameDataDAO.getOne(playerGameData.getOpponentId(), matchData.getId());
        int totalBallsPlayedByOpponent = 0;
        int totalBallsPlayedByPlayer = 0;
        for(Over data : playerGameData.getPlayerData().getOvers()) {
            totalBallsPlayedByOpponent = totalBallsPlayedByOpponent + data.getBallsData().size();
        }

        for(Over data : opponentData.getPlayerData().getOvers()) {
            totalBallsPlayedByPlayer = totalBallsPlayedByOpponent + data.getBallsData().size();
        }

        PlayersGameData fkScorePlayersData = playersGameDataDAO.getOne(fkScoreBoard.getTokenId(), matchData.getId());
        LOGGER.info("fkScorePlayersData : {}", fkScorePlayersData.getPlayerData().getFours());
        FKScoreAndType fkScore = new FKScoreAndType();
        fkScore.setScore(fkScorePlayersData.getPlayerData().getTotal());
        fkScore.setType("SCORE");
        FKScoreAndType fkSixes = new FKScoreAndType();
        fkSixes.setScore(fkScorePlayersData.getPlayerData().getSixes());
        fkSixes.setType("SIXES");
        FKScoreAndType fkBoundaries = new FKScoreAndType();
        fkBoundaries.setScore(fkScorePlayersData.getPlayerData().getFours());
        fkBoundaries.setType("BOUNDARIES");
        FKScoreAndType fkPerfectOvers = new FKScoreAndType();
        fkPerfectOvers.setScore(0);
        fkPerfectOvers.setType("PERFECT_OVERS");
        ArrayList<FKScoreAndType> fkScoreAndTypeArrayList = new ArrayList<>();
        fkScoreAndTypeArrayList.add(fkScore);
        fkScoreAndTypeArrayList.add(fkSixes);
        fkScoreAndTypeArrayList.add(fkBoundaries);
        fkScoreAndTypeArrayList.add(fkPerfectOvers);
        LOGGER.info("fk:{}",fkScoreAndTypeArrayList);
        fkScoreBoard.setScores(fkScoreAndTypeArrayList);
        OpponentPlayInfo opponentPlayInfo = new OpponentPlayInfo();
        if(fkScoreBoard.getTokenId().equals(matchData.getPlayerId())){
            opponentPlayInfo.setTokenId(matchData.getOpponentId());
            if(matchData.isBot()){
                opponentPlayInfo.setPlayWith("BOT");
            }else{
                opponentPlayInfo.setPlayWith("SINGLE_TEAM_PLAYER");
            }
            opponentPlayInfo.setTeam("MUMBAI");

        }else{
            opponentPlayInfo.setTokenId(matchData.getPlayerId());
            if(matchData.isBot()){
                opponentPlayInfo.setPlayWith("BOT");
            }else{
                opponentPlayInfo.setPlayWith("SINGLE_TEAM_PLAYER");
            }
            opponentPlayInfo.setTeam("MUMBAI");
        }
        fkScoreBoard.setPlayInfo(opponentPlayInfo);

        double playerScore = playerGameData.getPlayerData().getTotal() + playerGameData.getPlayerData().getTimingBonus();
        double opponentScore = opponentData.getPlayerData().getTotal() + playerGameData.getPlayerData().getTimingBonus();
        WinnerResponse winnerResponse = new WinnerResponse();
        if(playerGameData.getPlayerData().getTotal() == opponentData.getPlayerData().getTotal()){
            playerScore = playerGameData.getPlayerData().getTotal() + 25 + playerGameData.getPlayerData().getTimingBonus();
            opponentScore = opponentData.getPlayerData().getTotal() + 25 + playerGameData.getPlayerData().getTimingBonus();
            winnerResponse.setWon("tied");
            matchData.setWon("tied");
            matchData.setMatchStatus("ended");
        }else if( playerGameData.getPlayerData().getTotal() > opponentData.getPlayerData().getTotal()) {
            playerScore = 0;
            matchData.setWon(playerGameData.getPlayerId());
            fkScoreBoard.setStatus("COMPLETED");
            matchData.setMatchStatus("COMPLETED");
            playerScore = playerGameData.getPlayerData().getTotal() + 50 + playerGameData.getPlayerData().getTimingBonus();
            winnerResponse.setWon(playerGameData.getPlayerId());
        } else {
            opponentScore = 0;
            opponentScore = opponentData.getPlayerData().getTotal() + 50 + playerGameData.getPlayerData().getTimingBonus();
            winnerResponse.setWon(opponentData.getPlayerId());
            matchData.setWon(opponentData.getPlayerId());
            matchData.setMatchStatus("COMPLETED");
            fkScoreBoard.setStatus("COMPLETED");
        }
        Player player = playerDAO.getOne(matchData.getPlayerId());
        player.setStatus("OFFLINE");
        Player opponentPlayer = playerDAO.getOne(matchData.getOpponentId());
        opponentPlayer.setStatus("OFFLINE");
        playerDAO.updateStatusDAO(matchData.getPlayerId(), player);
        playerDAO.updateStatusDAO(matchData.getOpponentId(), opponentPlayer);
        matchDataDAO.update(matchData.getId(), matchData);

        PlayerData playerData = playerGameData.getPlayerData();
        PlayerData opponentPlayerData = opponentData.getPlayerData();


        WinnerPlayerData winerPlayerData = new WinnerPlayerData(matchData.getPlayerId(), playerData.getTotal(),50, playerData.getTimingBonus(), playerScore, playerData.getSixes(), playerData.getFours(), playerData.getOut());
        WinnerPlayerData winnerOpponentData = new WinnerPlayerData(matchData.getOpponentId(), opponentPlayerData.getTotal(),50,opponentPlayerData.getTimingBonus(), opponentScore,opponentPlayerData.getSixes(), opponentPlayerData.getFours(), opponentPlayerData.getOut());
        ArrayList<WinnerPlayerData> winnerPlayerDataArrayList = new ArrayList<>();
        winnerPlayerDataArrayList.add(winerPlayerData);
        winnerPlayerDataArrayList.add(winnerOpponentData);
        winnerResponse.setPlayerData(winnerPlayerDataArrayList);
//        return Response.ok(winnerResponse).build();
        try {
            Random random = new Random();
            URL url = new URL(configuration.getFlipkartUrl()+"core/v1/games/stageCompletion");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            ObjectMapper Obj = new ObjectMapper();
            Object payload = null;
            payload = Obj.writeValueAsString(fkScoreBoard);
            OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream());
            osw.write(String.format(payload.toString(), random.nextInt(30), random.nextInt(20)));
            osw.flush();
            osw.close();
            if(conn.getResponseCode() == 204 || conn.getResponseCode() == 200){
                matchDataDAO.update(matchData.getId(), matchData);
                LOGGER.info("winnerResponse : {}", winnerResponse);
                return Response.ok(winnerResponse).build();

            }else{
                return Response.ok("no match found").build();
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Response.ok("no match found").build();
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success.")
    })
    @GET
    @Path("{matchId}/currentBall/{playerId}")
    public Response getCurrentBallStatus(@ApiParam(value = "matchId") @PathParam("matchId") @NotNull final ObjectId gameId, @ApiParam(value = "playerId") @PathParam("playerId") @NotNull final String playerId ) {
        if (matchDataDAO.isTeamAvail(gameId)) {
            MatchData matchData = matchDataDAO.getOne(gameId);
            PlayersGameData playerGameData = playersGameDataDAO.getOne(playerId, matchData.getId());

            if(playerGameData == null) {
                return Response.ok("match not found").build();
            }
            PlayerData opponentData = playersGameDataDAO.getOne(playerGameData.getOpponentId(), matchData.getId()).getPlayerData();
            int playersCurrentOver = playerGameData.getPlayerData().getOvers().size()-1;
            int opponentsCurrentOver = opponentData.getOvers().size()-1;

            Map ballStatus = new HashMap();
            ballStatus.put(playerGameData.getPlayerId(), playerGameData.getPlayerData().getOvers().get(playersCurrentOver).getBallsData().size()-1);
            ballStatus.put(playerGameData.getOpponentId(), opponentData.getOvers().get(opponentsCurrentOver).getBallsData().size()-1);
            return  Response.ok(ballStatus).build();
        }
        return  Response.ok("Match not available").build();
    }
    public PlayerData updatePlayerData(MatchData matchData, ObjectId gameId, SelectBowler selectBowler){
        PlayerData playerData = new PlayerData();
        Over over =  updateOvers(selectBowler);
        ArrayList<Over> overs = new ArrayList<Over>();
        overs.add(over);
        playerData.setOvers(overs);
        return playerData;
    }

    public Over updateOvers(SelectBowler selectBowler){
        Over over = new Over();
        over.setBowlerId(selectBowler.getBowlerId());
        over.setBallsData(new ArrayList<>());
        return over;
    }

    public BallData generateBallData(ObjectId id){
        Bowler bowler = bowlerDAO.getOne(id);

        int speed = generateSpeed(bowler.getSpeed());
        String bowlerType = bowler.getType();
        int lineIndex = 0;
        float frame = 0;
        float perfectPoint = 0;
        float difficulty = 0;

        BallPath ballPath = new BallPath();
        BallData ballData = ballPath.calculateBallPath(speed, bowlerType);
        return ballData;
    }

    public int generateSpeed(Range range){
        Random rand = new Random();
        LOGGER.info("range :{}", range);
        int speed = rand.nextInt(range.getMax() + 1 - range.getMin()) + range.getMin();
        LOGGER.info("speed :{}", speed);
        return speed;
    }

    public Ball generateBall(BallData ballData){
        Ball ball = new Ball(ballData.getFrames(), ballData.getSpeed(), ballData.getLength(), ballData.getPerfectPoint(), ballData.getSteps(), ballData.getDifficultModSteps(),
                ballData.getDifficulty(),ballData.getFinalSpotIndex(), ballData.getLineIndex(), ballData.getPreVelocity(), ballData.getPreIncrement(), ballData.getPostVelocity(), ballData.getPostIncrement()
                , ballData.getDirection(), ballData.getExtreme(), ballData.getPerfectZone(), ballData.getBowlerType(), ballData.isWicketInPathBool(), ballData.isBatsmanInPathBool());
        return ball;
    }



    public static int randFloat(int min, int max) {
        Random rand = new Random();
        return (int) (rand.nextFloat() * (max - min) + min);
    }



    public class CheckScore{
        public int score;
        public double timeBonus;

        public int getScore() {
            return score;
        }

        public double getTimeBonus() {
            return timeBonus;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public void setTimeBonus(double timeBonus) {
            this.timeBonus = timeBonus;
        }
    }

    public CheckScore checkShot(int overSize,double timeTapped, String hitDirection, double perfectPoint, String pitchLine, double extreme, double perfectZone, double steps,double difficultyModSteps, boolean isWicketinPath ) {



        double difference = perfectPoint - timeTapped;
        double absoluteDifference = Math.abs(difference);
        boolean isHit = false;
        int score = 0;

        CheckScore checkScore = new CheckScore();

        if (hitDirection.equals(pitchLine)) {


            if (absoluteDifference <= perfectZone) {
                isHit = true;
                score = 6;
            } else if (absoluteDifference <= perfectZone+difficultyModSteps*1) {
                isHit = true;
                score = 4;
            } else if (absoluteDifference <= perfectZone+difficultyModSteps*2) {
                isHit = true;
                score = 3;
            } else if (absoluteDifference <= perfectZone+difficultyModSteps*4) {

                isHit = true;
                score = 2;
            } else if (absoluteDifference < perfectZone + difficultyModSteps*6) {
                isHit = true;
                score = 1;
            }

        } else {
            // wrong direction
            score = 0;
        }

        if (absoluteDifference >= extreme) {
            if(isWicketinPath && difference <0){
                score = -5;
                isHit = true;
            }else{
                isHit = true;
                score = 0;
            }
        }

        double time = 1;
        double timeBonus = 0;



        if (isHit) {
            if (difference > 0) {
                time = Math.min(1, difference / (steps*7));
            } else {
                time = Math.max(-1, difference / (steps*7));
            }
            timeBonus = 1 - Math.abs(time);
        }

        if(overSize == 2){
            checkScore.setScore(score * 2);
            checkScore.setTimeBonus(timeBonus);
        }else{
            checkScore.setScore(score);
            checkScore.setTimeBonus(timeBonus);
        }

        return checkScore;

    }



}