package com.game.resources;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.api.fkpojos.*;
import com.game.api.Team;
import com.game.CricketConfiguration;
import com.game.db.daos.TeamDAO;
import io.swagger.annotations.*;

import javax.ws.rs.core.Response;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.net.*;
import java.util.List;
import java.util.Random;

@Api(value = "scores",
        tags = {"scoreBoard"})
@Path("/scoreBoard")
@Produces(MediaType.APPLICATION_JSON)
public class ScoreBoardResource {

    private CricketConfiguration configuration;
    private TeamDAO teamDAO;
    public ScoreBoardResource(final CricketConfiguration configuration, final TeamDAO teamDAO){
        this.configuration = configuration;
        this.teamDAO = teamDAO;
    }

    /**
     * Get a {@link Response} by identifier.
     *
     * @param leaderboardRequest the identifier.
     * @return
     */
    Random random = new Random();

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "Players not found")
    })
    @POST
    @Path("/campaignLeaderboard")
    public Response getOneWithCampaign(@ApiParam(value = "LeaderboardRequest") @NotNull final LeaderboardRequest leaderboardRequest) {
        try {
            URL url = new URL(configuration.getFlipkartUrl()+"core/v1/campaignLeaderboard");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setHostnameVerifier(new HostnameVerifier() {
//                @Override
//                public boolean verify(String hostname, SSLSession sslSession) {
//                    return true;
//                }
//            });
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            ObjectMapper Obj = new ObjectMapper();
            Object payload = null;
            payload = Obj.writeValueAsString(leaderboardRequest);
            OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream());
            osw.write(String.format(payload.toString(), random.nextInt(30), random.nextInt(20)));
            osw.flush();
            osw.close();
            if(conn.getResponseCode() == 204 || conn.getResponseCode() == 200){
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                String output;
                while ((output = br.readLine()) != null) {
                    ObjectMapper objectMapper = new ObjectMapper();
                    objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                    CampaignLeaderboard campaignLeaderboard = objectMapper.readValue(output, CampaignLeaderboard.class);
                    return Response.ok(campaignLeaderboard).build();

                }
                conn.disconnect();
            }else{
               return Response.ok("campaign not yet completed").build();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Response.ok("issue with leaderboard").build();
    }

    /**
     * Get a {@link com.game.api.fkpojos.CampaignLeaderboard} by identifier.
     *
     * @param liveLeaderboardRequest the identifier.
     * @return
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "Players not found")
    })
    @POST
    @Path("/campaignLiveLeaderboard")
    public Response getLiveLeaderBoard(@ApiParam(value = "LeaderboardRequest") @NotNull final LiveLeaderboardRequest liveLeaderboardRequest) {
        try {
            URL url = new URL(configuration.getFlipkartUrl()+"core/v1/campaignLiveLeaderboard");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setHostnameVerifier(new HostnameVerifier() {
//                @Override
//                public boolean verify(String hostname, SSLSession sslSession) {
//                    return true;
//                }
//            });
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            ObjectMapper Obj = new ObjectMapper();
            Object payload = null;
            payload = Obj.writeValueAsString(liveLeaderboardRequest);
            OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream());
            osw.write(String.format(payload.toString(), random.nextInt(30), random.nextInt(20)));
            osw.flush();
            osw.close();
            if(conn.getResponseCode() == 204 || conn.getResponseCode() == 200){
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                String output;
                while ((output = br.readLine()) != null) {
                    ObjectMapper objectMapper = new ObjectMapper();
                    objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                    LiveLeaderboard liveLeaderboard = objectMapper.readValue(output, LiveLeaderboard.class);
                    return Response.ok(liveLeaderboard).build();
                }
                conn.disconnect();
            }else{
                return Response.ok("issue with leaderboard").build();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Response.ok("issue with leaderboard").build();
    }

    /**
     * Get all {@link com.game.api.Team} objects.
     *
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "Teams not found")
    })
    @GET
    @Path("/teamLeaderboard/{campaignId}")
    public Response getScoreOfTeams(@ApiParam(value = "campaignId") @PathParam("campaignId") @NotNull final String campaignId) {
        final List<Team> teamsFind = teamDAO.getScores(campaignId);
        if (teamsFind.isEmpty()) {
            return Response.accepted(new com.game.api.Response("Teams not found."))
                    .status(Response.Status.NOT_FOUND)
                    .build();
        }
        return Response.ok(teamsFind).build();
    }

}
