package com.game.resources;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.api.fkpojos.StageJoin;
import com.game.api.gameplaypojos.*;
import com.game.api.Player;
import com.game.CricketConfiguration;
import com.game.db.daos.MatchDataDAO;
import com.game.db.daos.PlayerDAO;
import com.game.db.daos.PlayersGameDataDAO;
import com.game.util.Status;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.*;
import java.net.*;
import java.util.*;

/**
 * This class has receive all REST request and process it.
 *
 * @author Hruday
 * @version 1.0.0
 * @since 1.0.0
 */
@Api(value = "teams",
         tags = {"teams"})
@Path("/matches")
@Produces(MediaType.APPLICATION_JSON)
public class MatchDataResource {

    /**
     * Logger class.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MatchDataResource.class);

    /**
     * DAO team.
     */
    private MatchDataDAO matchDataDAO;

    private PlayerDAO playerDAO;

    private  PlayersGameDataDAO playersGameDataDAO;

    private CricketConfiguration configuration;

    private String playerNotOnline = "Player is not online";

    Random random = new Random();

    ObjectMapper obj = new ObjectMapper();

    /**
     * Constructor.
     *
     * @param matchDataDAO the dao team.
     */
    public MatchDataResource(final MatchDataDAO matchDataDAO,
                             final PlayerDAO playerDAO, final PlayersGameDataDAO playersGameDataDAO,
                             final CricketConfiguration configuration
    ) {
        this.matchDataDAO = matchDataDAO;
        this.playerDAO = playerDAO;
        this.playersGameDataDAO = playersGameDataDAO;
        this.configuration = configuration;
    }

    /**
     * Get all {@link MatchData} objects.
     *
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "Matches not found")
    })
    @GET
    public Response all() {
        final List<MatchData> matchesFind = matchDataDAO.getAll();
        if (matchesFind.isEmpty()) {
            return Response.accepted(new com.game.api.Response("Matches not found."))
                    .status(Response.Status.NOT_FOUND)
                    .build();
        }
        return Response.ok(matchesFind).build();
    }

    /**
     * Get a {@link MatchData} by identifier.
     *
     * @param id the identifier.
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "Teams not found")
    })
    @GET
    @Path("/{id}")
    public Response getOne(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId id) {
        final MatchData matchDataCopy = matchDataDAO.getOne(id);
        if (matchDataCopy != null) {
            return Response.ok(matchDataCopy).build();
        }
        return Response.accepted(new com.game.api.Response("Match not found.")).build();
    }

    /**
     * Update the information of a {@link Player}.
     *
     * @param id    The identifier.
     * @param matchData the player information.
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success.")
    })
    @PUT
    @Path("/{id}")
    public Response update(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId id,
                           @ApiParam(value = "Player") @NotNull final MatchData matchData) {
        Object matchRetData = matchDataDAO.update(id, matchData);
        return Response.ok(matchRetData).build();
    }


    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "No Matches found")
    })
    @POST
    @Path("startBot/{matchId}")
    public Response startBotGame(@ApiParam(value = "StageJoin") @NotNull final StageJoin stageJoin, @ApiParam(value = "matchId") @PathParam(value = "matchId") @NotNull final ObjectId matchId) {
        final Player requestedPlayer = playerDAO.getOne(stageJoin.getTokenId());
        if(requestedPlayer == null) return Response.ok("Player not found").status(Response.Status.BAD_REQUEST).build();
        requestedPlayer.setStatus(Status.ONLINE.toString());
        final MatchData matchFind = matchDataDAO.getOne(matchId);
        if(matchFind == null) {
            return  Response.ok("match  not found").build();
        }
        List<PlayersGameData> opponentDataList = new ArrayList<>();
        Player player = null;
        List<Player> playersList = new ArrayList<>();
        List<Player> players = playerDAO.getAllWithSameEloRating(requestedPlayer.getEloRating(), requestedPlayer.getCampaignId());
        //Shuffle player to randomize th list
        Collections.shuffle(players);
        for(Player playerData : players) {
            if(playerData  != null){
                if((playerData.getEloRating() == requestedPlayer.getEloRating())
                        && !(playerData.getTokenId().equals(requestedPlayer.getTokenId()))
                        && (playerData.getCampaignId().equals(requestedPlayer.getCampaignId()))
                        &&  (!(playerData.getTeamId().equals(requestedPlayer.getTeamId())))) {
                    PlayersGameData opponentData = playersGameDataDAO.getOneByPlayerId((String) playerData.getTokenId());
                    int total = 0;
                    if(opponentData != null) {
                        if(opponentData.getPlayerData().getOvers().size() >= 3){
                            if(opponentData.getPlayerData().getOvers().get(2).getBallsData().size() == 6 && opponentData.getPlayerData().getOvers().get(2).getBallsData().get(5).isScoreUpdated()){
                                for (Over data : opponentData.getPlayerData().getOvers()) {
                                    total = total + data.getBallsData().size();
                                }
                                if (total == 18) {
                                    playersList.add(playerData);
                                    opponentDataList.add(opponentData);
                                }

                                if(opponentDataList.size() > 10) {
                                    break;
                                }
                            }
                        }

                    }
                }
            }
        }

        player = playersList.get(random.nextInt(playersList.size()));
        PlayersGameData opponentData = opponentDataList.get(random.nextInt(opponentDataList.size()));


        if(player !=null) {
            matchFind.setOpponentId(opponentData.getPlayerId());
            matchFind.setMatchStatus(Status.PLAYING.toString());
            matchFind.setBot(true);
            matchFind.setBotId(stageJoin.getTokenId());
            MatchData matchData = matchDataDAO.update(matchFind.getId(), matchFind);
            PlayersGameData getBotsOpponent = playersGameDataDAO.getOneByMatchId(opponentData.getGameId());
            PlayersGameData opponentDataCopy = new PlayersGameData();
            opponentDataCopy.setPlayerId(opponentData.getPlayerId());
            opponentDataCopy.setOpponentId(stageJoin.getTokenId());
            opponentDataCopy.setGameId(matchFind.getId());
            opponentDataCopy.setPlayerData(opponentData.getPlayerData());
            opponentData.getPlayerData().setOversBowled(opponentData.getPlayerData().getOvers());
            opponentDataCopy.getPlayerData().setOversBowled(getBotsOpponent.getPlayerData().getOvers());
            playersGameDataDAO.savePlayersData(opponentData.getPlayerId(), matchFind.getId(), opponentDataCopy);
            requestedPlayer.setStatus(Status.PLAYING.toString());
            playerDAO.updateStatusDAO((String) requestedPlayer.getTokenId(), requestedPlayer);
            return Response.ok(matchData).build();
        } else {
            return Response.ok(playerNotOnline).status(Response.Status.BAD_REQUEST).build();
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "No Matches found")
    })
    @POST
    @Path("start")
    public Response startGame(@ApiParam(value = "StageJoin") @NotNull final StageJoin stageJoin) throws IOException {
        final Player requestedPlayer = playerDAO.getOne(stageJoin.getTokenId());
        if(requestedPlayer == null) return Response.ok("Player not found").build();
        if(requestedPlayer.getStatus().equals(Status.OFFLINE.toString())){
            requestedPlayer.setStatus(Status.ONLINE.toString());
            final List<MatchData> matchesFind = matchDataDAO.getNewMatches();
            if(!requestedPlayer.getStatus().equals("PLAYING")){
                if(matchesFind.isEmpty()){
                    try {
                        HttpURLConnection conn = stageJoinConnectMecthod();
                        Object payload = null;
                        payload = obj.writeValueAsString(stageJoin);
                        OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream());
                        osw.write(String.format(payload.toString(), random.nextInt(30), random.nextInt(20)));
                        osw.flush();
                        osw.close();
                        if(conn.getResponseCode() == 204 || conn.getResponseCode() == 200){
                            requestedPlayer.setStatus(Status.PLAYING.toString());
                            playerDAO.updateStatusDAO((String) requestedPlayer.getTokenId(), requestedPlayer);
                            return Response.ok(matchDataDAO.startGame(stageJoin.getTokenId(), null, stageJoin.getCampaignId())).build();
                        }else{
                            BufferedReader br;
                            br = new BufferedReader(new InputStreamReader(
                                    (conn.getErrorStream())));
                            return Response.ok(br.readLine()).status(conn.getResponseCode()).build();
                        }
                    } catch (IOException e) {
                        LOGGER.info("Just a stack trace, nothing to worry about", e);
                    }
                }else{
                    for(MatchData match : matchesFind){
                        final Player matchCreator = playerDAO.getOne(match.getPlayerId());
                        if(matchCreator.getEloRating() == requestedPlayer.getEloRating() && !(matchCreator.getTokenId().equals(requestedPlayer.getTokenId()))
                                && matchCreator.getStatus().equals(Status.PLAYING.toString()) && matchCreator.getCampaignId().equals(requestedPlayer.getCampaignId()) &&  !(matchCreator.getTeamId().equals(requestedPlayer.getTeamId()))){
                            try {
                                HttpURLConnection conn = stageJoinConnectMecthod();
                                Object payload = null;
                                payload = obj.writeValueAsString(stageJoin);
                                OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream());
                                osw.write(String.format(payload.toString(), random.nextInt(30), random.nextInt(20)));
                                osw.flush();
                                osw.close();
                                if(conn.getResponseCode() == 204 || conn.getResponseCode() == 200){
                                        MatchData matchData =  matchDataDAO.findAndModify(match.getId(), stageJoin.getTokenId(), stageJoin.getCampaignId());
                                        requestedPlayer.setStatus(Status.PLAYING.toString());
                                        playerDAO.updateStatusDAO(stageJoin.getTokenId(), requestedPlayer);
                                        return Response.ok(matchData).build();
                                }else{
                                    BufferedReader br;
                                    br = new BufferedReader(new InputStreamReader(
                                            (conn.getErrorStream())));
                                    return Response.ok(br.readLine()).status(conn.getResponseCode()).build();
                                }
                            } catch (IOException e) {
                                LOGGER.info("Just a stack trace, nothing to worry about", e);
                            }

                        }
                    }
                }
            }
        }else{
            return Response.ok("previous match not ended").status(Response.Status.BAD_REQUEST).build();
        }
        LOGGER.info("out of bounds");
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).header(HttpHeaders.CONTENT_TYPE, "text/plain")
                .build();
    }

    public HttpURLConnection stageJoinConnectMecthod() throws IOException {
        URL url = new URL(configuration.getFlipkartUrl()+"core/v1/games/stageJoin");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "application/json");
        return conn;
    }
}