package com.game.resources;

import com.game.api.Bowler;
import com.game.api.Campaign;
import com.game.api.CampaignDetails;
import com.game.api.Team;
import com.game.db.daos.BowlerDAO;
import com.game.db.daos.CampaignDAO;
import com.game.db.daos.TeamDAO;
import com.game.util.CSVConverter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * This class has receive all REST request and process it.
 *
 * @author Hruday
 * @version 1.0.0
 * @since 1.0.0
 */
@Api(value = "campaign",
        description = "campaign REST API for handle player CRUD operations on campaign collection.",
        tags = {"campaign"})
@Path("/campaign")
@Produces(MediaType.APPLICATION_JSON)
public class CampaignResource {
    /**
     * DAO Bowler.
     */
    private BowlerDAO bowlerDAO;
    private TeamDAO teamDAO;
    private CampaignDAO campaignDAO;

    private static final Logger LOGGER = LoggerFactory.getLogger(TeamResource.class);

    /**
     * Constructor.
     *
     * @param bowlerDAO the dao bowler.
     */
    public CampaignResource(final BowlerDAO bowlerDAO, final TeamDAO teamDAO, final CampaignDAO campaignDAO) {
        this.bowlerDAO = bowlerDAO;
        this.teamDAO = teamDAO;
        this.campaignDAO = campaignDAO;
    }


    /**
     * Get all {@link com.game.api.Bowler} objects.
     *
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "Players not found")
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCampaign(@ApiParam(value = "campaign") @NotNull final Campaign campaign) {
        CampaignDetails details = new CampaignDetails();
        details.setCampaignName(campaign.getCampaignName());
        details.setTips(campaign.getTips());
        details.setStadium(campaign.getStadium());
        details.setLifeManagement(campaign.getLifeManagement());
        details.setAdvertisement(campaign.getAdvertisement());
        details.setCampaignActions(campaign.getCampaignActions());
        details.setCampaignId(campaign.getCampaignId());
        if(campaign.getTeamsList() != null){
            for(Team team : campaign.getTeamsList()) {
                if(team.getName() != null){
                    team.setCampaignId(campaign.getCampaignId());
                    Team team1 = teamDAO.save(team);
                    if(team.getBowlerList() != null){
                        for(Bowler bowler : team.getBowlerList()){
                            if(bowler.getName() != null){
                                bowler.setTeamId(team1.getId());
                                bowler.setCampaignId(campaign.getCampaignId());
                                bowlerDAO.save(bowler);
                            }else{
                                return Response.ok("bowler name cant be null").build();
                            }
                        }
                    }else{
                        return Response.ok("bowler's list cant be null").build();
                    }

                }else{
                    return Response.ok("team name cant be null").build();
                }
            }
        }else{
            return Response.ok("team's list cant be null").build();
        }

        CampaignDetails campDetails = campaignDAO.save(details);
        if(campDetails == null) {
            return  Response.ok("Campaign already exists").build();
        }

        return Response.ok("Details saved").build();
    }


    /**
     * Get all {@link com.game.api.Campaign} objects.
     *
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "Players not found")
    })
    @GET
    @Path("/tips/{campaignId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTips(@ApiParam(value = "campaignId") @PathParam("campaignId") @NotNull final String campaignId) {
        if(campaignId != null){
            return Response.ok(campaignDAO.getOneByCampaignName(campaignId).getTips()).status(Response.Status.ACCEPTED).build();
        }
        return Response.ok("campaign not found").status(Response.Status.BAD_REQUEST).build();
    }


    /**
     * Get all {@link com.game.api.Campaign} objects.
     *
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "Players not found")
    })
    @GET
    @Path("/{campaignId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCampaign(@ApiParam(value = "campaignId") @PathParam("campaignId") @NotNull final String campaignId) {
        if(campaignId != null){
            return Response.ok(campaignDAO.getOneByCampaignName(campaignId)).build();
        }
        return Response.ok("campaign not found").status(Response.Status.BAD_REQUEST).build();
    }
}
