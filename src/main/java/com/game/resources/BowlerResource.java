package com.game.resources;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.game.api.Bowler;
import com.game.db.daos.BowlerDAO;
import org.bson.types.ObjectId;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class has receive all REST request and process it.
 *
 * @author Hruday
 * @version 1.0.0
 * @since 1.0.0
 */
@Api(value = "bowlers",
        tags = {"bowlers"})
@Path("/bowlers")
@Produces(MediaType.APPLICATION_JSON)
public class BowlerResource {


    /**
     * DAO Bowler.
     */
    private BowlerDAO bowlerDAO;

    /**
     * Constructor.
     *
     * @param bowlerDAO the dao bowler.
     */
    public BowlerResource(final BowlerDAO bowlerDAO) {
        this.bowlerDAO = bowlerDAO;
    }

    /**
     * Get all {@link com.game.api.Bowler} objects.
     *
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "Players not found")
    })
    @GET
    @Path("/{campaignId}/{teamId}")
    public Response all(@ApiParam(value = "campaignId") @PathParam("campaignId") @NotNull final String campaignId,
                        @ApiParam(value = "teamId") @PathParam("teamId") @NotNull final ObjectId teamId) {
        final List<Bowler> bowlersFind = bowlerDAO.getAll(campaignId, teamId);
        if (bowlersFind.isEmpty()) {
            return Response.accepted(new com.game.api.Response("Bowlers not found."))
                    .status(Response.Status.NOT_FOUND)
                    .build();
        }
        return Response.ok(bowlersFind).build();
    }

    /**
     * Get a {@link Bowler} by identifier.
     *
     * @param id the identifier.
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "Bowlers not found")
    })
    @GET
    @Path("/{id}")
    public Response getOne(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId id) {
        final Bowler bowler = bowlerDAO.getOne(id);
        if (bowler != null) {
            return Response.ok(bowler).build();
        }
        return Response.accepted(new com.game.api.Response("Bowler not found.")).build();
    }

    /**
     * Persis a {@link Bowler} object in a collection.
     *
     * @param bowler The objecto to persist.
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 201, message = "Players created successfully")
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(@ApiParam(value = "Bowler") @NotNull final Bowler bowler) {
        if(bowler.getName() != null){
            Object bowlerData = bowlerDAO.save(bowler);
            return Response.ok(bowlerData).build();
        }else{
            return Response.ok("bowler name can't be null").status(Response.Status.BAD_REQUEST).build();
        }
    }

    /**
     * Update the information of a {@link Bowler}.
     *
     * @param uniqueId    The identifier.
     * @param bowler the player information.
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success.")
    })
    @PUT
    @Path("/{id}")
    public Response update(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId uniqueId,
                           @ApiParam(value = "Bowler") @NotNull final Bowler bowler) {
        Object bowlerData = bowlerDAO.update(uniqueId, bowler);
        return Response.ok(bowlerData).build();
    }

    /**
     * Delete a {@link Bowler} object.
     * @param id   the identifier.
     * @return  A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success.")
    })
    @DELETE
    @Path("/{id}")
    public Response delete(@ApiParam(value = "id") @PathParam("id") @NotNull final ObjectId id) {
        bowlerDAO.delete(id);
        return Response.ok().build();
    }





}