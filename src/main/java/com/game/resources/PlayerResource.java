package com.game.resources;
import java.io.*;
import java.net.*;
import java.util.List;
import java.util.Random;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.core.*;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.api.fkpojos.*;
import com.game.api.Player;
import com.game.CricketConfiguration;
import com.game.db.daos.PlayerDAO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.HttpURLConnection;

/**
 * This class has receive all REST request and process it.
 *
 * @author Hruday
 * @version 1.0.0
 * @since 1.0.0
 */
@Api(value = "players",
        tags = {"players"})
@Path("/players")
@Produces(MediaType.APPLICATION_JSON)
public class PlayerResource {

    /**
     * Logger class.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerResource.class);
    private CricketConfiguration configuration;

    /**
     * DAO player.
     */
    private PlayerDAO playerDAO;

    /**
     * Constructor.
     *
     * @param playerDAO the dao player.
     */
    public PlayerResource(final PlayerDAO playerDAO, final CricketConfiguration configuration) {
        this.playerDAO = playerDAO;
        this.configuration = configuration;
    }

    /**
     * Get all {@link com.game.api.Player} objects.
     *
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "Players not found")
    })
    @GET
    @Path("/scoreBoard")
    public Response all() {
        final List<Player> playersFind = playerDAO.getAll();
        if (playersFind.isEmpty()) {
            return Response.accepted(new com.game.api.Response("Players not found."))
                    .status(Response.Status.NOT_FOUND)
                    .build();
        }
        return Response.ok(playersFind).build();
    }

    /**
     * Get a {@link Player} by identifier.
     *
     * @param id the identifier.
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "Players not found")
    })
    @GET
    @Path("/{id}")
    public Response getOne(@ApiParam(value = "id") @PathParam("id") @NotNull final String id) {
        final Player player = playerDAO.getOne(id);
        if (player != null) {
            return Response.ok(player).build();
        }
        return Response.accepted(new com.game.api.Response("Player not found.")).build();
    }

    /**
     * Get a {@link Player} by identifier.
     *
     * @param id the identifier.
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "Players not found")
    })
    @GET
    @Path("/{id}/{campaignId}")
    public Response getOneWithCampaign(@ApiParam(value = "id") @PathParam("id") @NotNull final String id, @ApiParam(value = "campaignId") @PathParam("campaignId") @NotNull final String campaignId) {
        try {
            URL url = new URL(configuration.getFlipkartUrl()+"core/v1/games/userGameSummary?tokenId="+id+"&campaignId="+campaignId+"&include=userName,userId");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            LOGGER.info("inside ggt :{}", conn.getResponseCode());
            if (conn.getResponseCode() != 200) {
                BufferedReader br;
                br = new BufferedReader(new InputStreamReader(
                        (conn.getErrorStream())));
                return Response.ok(br.readLine()).status(conn.getResponseCode()).build();
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            while ((output = br.readLine()) != null) {
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                UserInstance userInstance = objectMapper.readValue(output, UserInstance.class);
                final Player player = playerDAO.getOne(id);
                if(userInstance.getUserName() != null && userInstance.getUserName() !="" && player != null){
                    player.setStageNumber(userInstance.getUserGameInstance().getStageInfo().getTotal());
                    player.setLives(userInstance.getLives().getAvailable());
                    return Response.ok(player).build();
                }else{
                    if(userInstance.getUserName() != null && userInstance.getUserName() != ""){
                        Player player1 = new Player();
                        player1.setTokenId(id);
                        player1.setPlayerName(userInstance.getUserName());
                        player1.setStageNumber(userInstance.getUserGameInstance().getStageInfo().getTotal());
                        player1.setLives(userInstance.getLives().getAvailable());
                        player1.setCampaignId(campaignId);
                        return Response.ok(playerDAO.save(player1)).build();
                    }else{
                        return Response.ok(new com.game.api.Response("register")).build();
                    }
                }
            }
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).header(HttpHeaders.CONTENT_TYPE, "text/plain")
                .build();
    }


    /**
     * Persis a {@link Player} object in a collection.
     *
     * @param player The objecto to persist.
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 201, message = "Player created successfully")
    })
    @POST
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(@ApiParam(value = "Player") @NotNull final Player player) {
        LOGGER.info("Persist a player in collection with the information: {}", player);
        if(player.getPlayerName().length() > 12){
            return Response.ok("name should be less than 12 characters").build();
        }else{
            if(playerDAO.getOne((String) player.getTokenId()) != null){
                return Response.accepted(new com.game.api.Response("Player is already registered."))
                        .status(Response.Status.BAD_REQUEST)
                        .build();
            }else{
                Object playerData = playerDAO.save(player);
                return Response.ok(playerData).build();
            }
        }

    }


    /**
     * Persis a {@link Player} object in a collection.
     *
     * @param player The objecto to persist.
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 201, message = "Player created successfully")
    })
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveToDB(@ApiParam(value = "Player") @NotNull final Player player) throws IOException {

        try {
            Random random = new Random();
            URL url = new URL(configuration.getFlipkartUrl()+"core/v1/profile/username");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("PUT");
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            UpdateName updateName = new UpdateName("cricket", (String) player.getTokenId(), player.getCampaignId(),player.getPlayerName(), 0);
            ObjectMapper Obj = new ObjectMapper();
            Object payload = null;
            payload = Obj.writeValueAsString(updateName);
            OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream());
            osw.write(String.format(payload.toString(), random.nextInt(30), random.nextInt(20)));
            osw.flush();
            osw.close();
            if(conn.getResponseCode() == 204 || conn.getResponseCode() == 200){
                if(playerDAO.getOne((String) player.getTokenId()) != null){
                    return Response.accepted(new com.game.api.Response("Player is already registered."))
                            .status(Response.Status.BAD_REQUEST)
                            .build();
                }else{
                    Object playerData = playerDAO.save(player);
                    return Response.ok(playerData).build();
                }
            } else{
                BufferedReader br;
                br = new BufferedReader(new InputStreamReader(
                        (conn.getErrorStream())));
                return Response.ok(br.readLine()).status(conn.getResponseCode()).build();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Response.ok("out of bounds").status(Response.Status.BAD_REQUEST).build();
    }


    /**
     * Update the information of a {@link Player}.
     *
     * @param uniqueId    The identifier.
     * @param player the player information.
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success.")
    })
    @PUT
    @Path("/{uniqueId}")
    public Response update(@ApiParam(value = "uniqueId") @PathParam("uniqueId") @NotNull final String uniqueId,
                           @ApiParam(value = "Player") @NotNull final Player player) {
        Player player1 = playerDAO.getOne(uniqueId);
        if(player1 != null){
            Object playerData = playerDAO.update(uniqueId, player);
            return Response.ok(playerData).build();
        }else{
            return Response.ok("player not found").status(Response.Status.BAD_REQUEST).build();
        }
    }
    /**
     * Update the information of a {@link Player}.
     *
     * @param uniqueId    The identifier.
     * @param player the player information.
     * @return A object {@link Response} with the information of result this method.
     */
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success.")
    })
    @PUT
    @Path("status/{uniqueId}")
    public Response updateStatus(@ApiParam(value = "uniqueId") @PathParam("uniqueId") @NotNull final String uniqueId,
                                 @ApiParam(value = "Player") @NotNull final Player player) {
        LOGGER.info("Update the information of a player : {} ", player);
        playerDAO.updateStatusDAO(uniqueId, player);
        return Response.ok("updated : true").build();
    }

}