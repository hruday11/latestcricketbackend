package com.game.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.game.api.gameplaypojos.Range;
import com.game.util.ObjectIdSerializer;
import org.bson.types.ObjectId;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class Bowler {

    @JsonSerialize(using = ObjectIdSerializer.class)
    private ObjectId id;

    @JsonSerialize(using = ObjectIdSerializer.class)
    private Object teamId;

    @NotNull
    private String name;

    @NotNull
    private String type;

    @NotNull
    private String description;

    @NotNull
    private Range speed;

    private String campaignId;

    private String image;

    private String character;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Range getSpeed() {
        return speed;
    }

    public void setSpeed(Range speed) {
        this.speed = speed;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Object getTeamId() {
        return teamId;
    }

    public void setTeamId(Object teamId) {
        this.teamId = teamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bowler bowler = (Bowler) o;
        return Objects.equals(id, bowler.id) &&
                Objects.equals(teamId, bowler.teamId) &&
                Objects.equals(name, bowler.name) &&
                Objects.equals(type, bowler.type) &&
                Objects.equals(description, bowler.description) &&
                Objects.equals(speed, bowler.speed) &&
                Objects.equals(campaignId, bowler.campaignId) &&
                Objects.equals(image, bowler.image) &&
                Objects.equals(character, bowler.character);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, teamId, name, type, description, speed, campaignId, image, character);
    }

    @Override
    public String toString() {
        return "Bowler{" +
                "id=" + id +
                ", teamId=" + teamId +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                ", speed=" + speed +
                ", campaignId=" + campaignId +
                ", image='" + image + '\'' +
                ", character='" + character + '\'' +
                '}';
    }
}
