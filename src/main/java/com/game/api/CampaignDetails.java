package com.game.api;

import com.game.api.campaignpojos.*;
import org.bson.types.ObjectId;

import java.util.List;

public class CampaignDetails {
    private String campaignName;
    private String campaignId;
    private ObjectId id;
    private Stadium stadium;
    private List<String> tips;
    private Advertisement advertisement;
    private LifeManagement lifeManagement;
    private CampaignActions campaignActions;

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public LifeManagement getLifeManagement() {
        return lifeManagement;
    }

    public CampaignActions getCampaignActions() {
        return campaignActions;
    }

    public Advertisement getAdvertisement() {
        return advertisement;
    }

    public Stadium getStadium() {
        return stadium;
    }

    public List<String> getTips() {
        return tips;
    }

    public void setTips(List<String> tips) {
        this.tips = tips;
    }

    public void setStadium(Stadium stadium) {
        this.stadium = stadium;
    }

    public void setLifeManagement(LifeManagement lifeManagement) {
        this.lifeManagement = lifeManagement;
    }

    public void setCampaignActions(CampaignActions campaignActions) {
        this.campaignActions = campaignActions;
    }

    public void setAdvertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
    }
}
