package com.game.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.game.api.gameplaypojos.Color;
import com.game.util.ObjectIdSerializer;
import org.bson.types.ObjectId;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

public class Team {

    @JsonSerialize(using = ObjectIdSerializer.class)
    private ObjectId id;

    @NotNull
    private String name;

    private int score;

    private int playerCount;

    @NotNull
    private Color jerseyColor;

    private String campaignId;

    private String teamLogo;

    private String sponsorLogo;

    private List<Bowler> bowlerList;


    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Bowler> getBowlerList() {
        return bowlerList;
    }

    public String getSponsorLogo() {
        return sponsorLogo;
    }

    public String getTeamLogo() {
        return teamLogo;
    }

    public void setBowlerList(List<Bowler> bowlerList) {
        this.bowlerList = bowlerList;
    }

    public void setSponsorLogo(String sponsorLogo) {
        this.sponsorLogo = sponsorLogo;
    }

    public void setTeamLogo(String teamLogo) {
        this.teamLogo = teamLogo;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }

    public Color getJerseyColor() {
        return jerseyColor;
    }

    public void setJerseyColor(Color jerseyColor) {
        this.jerseyColor = jerseyColor;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return score == team.score &&
                playerCount == team.playerCount &&
                Objects.equals(id, team.id) &&
                Objects.equals(name, team.name) &&
                Objects.equals(jerseyColor, team.jerseyColor) &&
                Objects.equals(campaignId, team.campaignId) &&
                Objects.equals(teamLogo, team.teamLogo) &&
                Objects.equals(sponsorLogo, team.sponsorLogo) &&
                Objects.equals(bowlerList, team.bowlerList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, score, playerCount, jerseyColor, campaignId, teamLogo, sponsorLogo, bowlerList);
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", score=" + score +
                ", playerCount=" + playerCount +
                ", jerseyColor=" + jerseyColor +
                ", campaignId=" + campaignId +
                ", teamLogo='" + teamLogo + '\'' +
                ", sponsorLogo='" + sponsorLogo + '\'' +
                ", bowlerList=" + bowlerList +
                '}';
    }
}
