package com.game.api.gameplaypojos;

import java.util.Objects;

public class ScoreData {
    private int runs;

    private String playerID;

    private int total;

    private boolean updated = false;

    public ScoreData(int runs, String playerID,int total, boolean updated){
        this.runs = runs;
        this.playerID =  playerID;
        this.updated = updated;
        this.total = total;
    }

    public int getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public String getPlayerID() {
        return playerID;
    }

    public void setPlayerID(String playerID) {
        this.playerID = playerID;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScoreData scoreData = (ScoreData) o;
        return Float.compare(scoreData.runs, runs) == 0 &&
                updated == scoreData.updated &&
                Objects.equals(playerID, scoreData.playerID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(runs, playerID, updated);
    }

    @Override
    public String toString() {
        return "ScoreData{" +
                "score=" + runs +
                ", playerID='" + playerID + '\'' +
                ", updated=" + updated +
                '}';
    }
}
