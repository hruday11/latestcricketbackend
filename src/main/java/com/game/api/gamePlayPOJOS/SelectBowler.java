package com.game.api.gameplaypojos;

import java.util.Objects;

public class SelectBowler{
    private String bowlerId;
    private String playerId;
    private String opponentId;

    public SelectBowler() {
    }

    public SelectBowler(String bowlerId, String playerId) {
        this.bowlerId = bowlerId;
        this.playerId = playerId;
    }

    public String getBowlerId() {
        return bowlerId;
    }

    public void setBowlerId(String bowlerId) {
        this.bowlerId = bowlerId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SelectBowler that = (SelectBowler) o;
        return Objects.equals(bowlerId, that.bowlerId) &&
                Objects.equals(playerId, that.playerId) &&
                Objects.equals(opponentId, that.opponentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bowlerId, playerId, opponentId);
    }

    @Override
    public String toString() {
        return "SelectBowler{" +
                "bowlerId='" + bowlerId + '\'' +
                ", playerId='" + playerId + '\'' +
                ", oponentId='" + opponentId + '\'' +
                '}';
    }

    public String getOpponentId() {
        return opponentId;
    }

    public void setOpponentId(String oponentId) {
        this.opponentId = oponentId;
    }
}