package com.game.api.gameplaypojos;

import org.bson.types.ObjectId;

import java.util.Objects;

public class UpdatePlayer {

    private String playerName;

    private ObjectId teamId;

    private boolean tutorialCompleted;

    public ObjectId getTeamId() {
        return teamId;
    }

    public boolean isTutorialCompleted() {
        return tutorialCompleted;
    }

    public void setTutorialCompleted(boolean tutorialCompleted) {
        this.tutorialCompleted = tutorialCompleted;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public void setTeamId(ObjectId teamId) {
        this.teamId = teamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdatePlayer that = (UpdatePlayer) o;
        return Objects.equals(playerName, that.playerName) &&
                Objects.equals(teamId, that.teamId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerName, teamId);
    }

    @Override
    public String toString() {
        return "UpdatePlayer{" +
                "playerName='" + playerName + '\'' +
                ", teamId=" + teamId +
                '}';
    }
}
