package com.game.api.gameplaypojos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.game.util.ObjectIdSerializer;
import org.bson.types.ObjectId;

import javax.validation.constraints.NotNull;

public class PlayersGameData {
    @JsonSerialize(using = ObjectIdSerializer.class)
    private ObjectId id;

    private ObjectId gameId;

    @NotNull
    private String playerId;

    @NotNull
    private String opponentId;

    private PlayerData playerData;

    private String won;

    private int currentBall;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getOpponentId() {
        return opponentId;
    }

    public void setOpponentId(String opponentId) {
        this.opponentId = opponentId;
    }

    public PlayerData getPlayerData() {
        return playerData;
    }

    public void setPlayerData(PlayerData player1Data) {
        this.playerData = player1Data;
    }

    public String getWon() {
        return won;
    }

    public void setWon(String won) {
        this.won = won;
    }

    public ObjectId getGameId() {
        return gameId;
    }

    public void setGameId(ObjectId gameId) {
        this.gameId = gameId;
    }

    public int getCurrentBall() {
        return currentBall;
    }

    public void setCurrentBall(int currentBall) {
        this.currentBall = currentBall;
    }
}
