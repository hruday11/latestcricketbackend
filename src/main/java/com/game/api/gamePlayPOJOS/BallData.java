package com.game.api.gameplaypojos;

import java.util.Objects;

public class BallData{
    private double frames;

    private int speed;

    private int length;

    private double perfectPoint;

    private double steps;

    private double difficultModSteps;

    private float difficulty;

    private double finalSpotIndex;

    private double lineIndex;

    private double preVelocity;

    private double preIncrement;

    private double postVelocity;

    private double postIncrement;

    private String direction;

    private double hitTime;

    private double bonusTime;

    private String hitDirection;

    private int score;

    private boolean scoreUpdated = false;

    private double extreme;

    private double perfectZone;

    private String bowlerType;

    private boolean wicketInPathBool;

    private  boolean batsmanInPathBool;

    public BallData(){

    }

    public  BallData(double frames, int speed, int length, double perfectPoint, double steps, double difficultModSteps, float difficulty, double finalSpotIndex, double lineIndex, double preVelocity,
                     double preIncrement, double postVelocity, double postIncrement, String direction, double extreme, double perfectZone, double hitTime, double bonusTime,
                     String hitDirection, int score, boolean scoreUpdated, String bowlerType, boolean wicketInPathBool, boolean batsmanInPathBool){
        this.finalSpotIndex = finalSpotIndex;
        this.frames = frames;
        this.speed = speed;
        this.length  = length;
        this.perfectPoint = perfectPoint;
        this.steps = steps;
        this.difficultModSteps = difficultModSteps;
        this.difficulty = difficulty;
        this.lineIndex = lineIndex;
        this.preIncrement = preIncrement;
        this.preVelocity = preVelocity;
        this.postVelocity = postVelocity;
        this.postIncrement = postIncrement;
        this.direction = direction;
        this.extreme = extreme;
        this.perfectZone = perfectZone;
        this.hitDirection = hitDirection;
        this.hitTime = hitTime;
        this.bonusTime = bonusTime;
        this.score = score;
        this.scoreUpdated = scoreUpdated;
        this.bowlerType = bowlerType;
        this.batsmanInPathBool = batsmanInPathBool;
        this.wicketInPathBool = wicketInPathBool;
    }


    public double getFrames() {
        return frames;
    }

    public double getSteps() {
        return steps;
    }

    public double getPostIncrement() {
        return postIncrement;
    }

    public double getPostVelocity() {
        return postVelocity;
    }

    public double getPreIncrement() {
        return preIncrement;
    }

    public double getPreVelocity() {
        return preVelocity;
    }

    public double getBonusTime() {
        return bonusTime;
    }

    public double getDifficultModSteps() {
        return difficultModSteps;
    }

    public float getDifficulty() {
        return difficulty;
    }

    public double getHitTime() {
        return hitTime;
    }

    public double getPerfectPoint() {
        return perfectPoint;
    }

    public double getFinalSpotIndex() {
        return finalSpotIndex;
    }

    public String getBowlerType() {
        return bowlerType;
    }

    public int getLength() {
        return length;
    }

    public double getLineIndex() {
        return lineIndex;
    }

    public int getScore() {
        return score;
    }

    public int getSpeed() {
        return speed;
    }

    public boolean isScoreUpdated() {
        return scoreUpdated;
    }

    public String getDirection() {
        return direction;
    }

    public double getExtreme() {
        return extreme;
    }

    public double getPerfectZone() {
        return perfectZone;
    }


    public boolean isBatsmanInPathBool() {
        return batsmanInPathBool;
    }

    public boolean isWicketInPathBool() {
        return wicketInPathBool;
    }

    public void setBatsmanInPathBool(boolean batsmanInPathBool) {
        this.batsmanInPathBool = batsmanInPathBool;
    }

    public void setWicketInPathBool(boolean wicketInPathBool) {
        this.wicketInPathBool = wicketInPathBool;
    }

    public void setScoreUpdated(boolean scoreUpdated) {
        this.scoreUpdated = scoreUpdated;
    }

    public String getHitDirection() {
        return hitDirection;
    }

    public void setFinalSpotIndex(double finalSpotIndex) {
        this.finalSpotIndex = finalSpotIndex;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setLineIndex(double lineIndex) {
        this.lineIndex = lineIndex;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setBonusTime(double bonusTime) {
        this.bonusTime = bonusTime;
    }

    public void setDifficultModSteps(double difficultModSteps) {
        this.difficultModSteps = difficultModSteps;
    }

    public void setDifficulty(float difficulty) {
        this.difficulty = difficulty;
    }

    public void setFrames(double frames) {
        this.frames = frames;
    }

    public void setHitDirection(String hitDirection) {
        this.hitDirection = hitDirection;
    }

    public void setHitTime(double hitTime) {
        this.hitTime = hitTime;
    }

    public void setBowlerType(String bowlerType) {
        this.bowlerType = bowlerType;
    }

    public void setExtreme(double extreme) {
        this.extreme = extreme;
    }

    public void setPerfectZone(double perfectZone) {
        this.perfectZone = perfectZone;
    }

    public void setPerfectPoint(double perfectPoint) {
        this.perfectPoint = perfectPoint;
    }


    public void setPostIncrement(double postIncrement) {
        this.postIncrement = postIncrement;
    }

    public void setPostVelocity(double postVelocity) {
        this.postVelocity = postVelocity;
    }


    public void setPreIncrement(double preIncrement) {
        this.preIncrement = preIncrement;
    }


    public void setPreVelocity(double preVelocity) {
        this.preVelocity = preVelocity;
    }


    public void setScore(int score) {
        this.score = score;
    }

    public void setSteps(double steps) {
        this.steps = steps;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BallData ballData = (BallData) o;
        return Double.compare(ballData.frames, frames) == 0 &&
                speed == ballData.speed &&
                length == ballData.length &&
                Double.compare(ballData.perfectPoint, perfectPoint) == 0 &&
                Double.compare(ballData.steps, steps) == 0 &&
                Double.compare(ballData.difficultModSteps, difficultModSteps) == 0 &&
                Double.compare(ballData.difficulty, difficulty) == 0 &&
                Double.compare(ballData.finalSpotIndex, finalSpotIndex) == 0 &&
                lineIndex == ballData.lineIndex &&
                Double.compare(ballData.preVelocity, preVelocity) == 0 &&
                Double.compare(ballData.preIncrement, preIncrement) == 0 &&
                Double.compare(ballData.postVelocity, postVelocity) == 0 &&
                Double.compare(ballData.postIncrement, postIncrement) == 0 &&
                Double.compare(ballData.hitTime, hitTime) == 0 &&
                Double.compare(ballData.bonusTime, bonusTime) == 0 &&
                score == ballData.score &&
                scoreUpdated == ballData.scoreUpdated &&
                Objects.equals(direction, ballData.direction) &&
                Objects.equals(hitDirection, ballData.hitDirection);
    }

    @Override
    public int hashCode() {
        return Objects.hash(frames, speed, length, perfectPoint, steps, difficultModSteps, difficulty, finalSpotIndex, lineIndex, preVelocity, preIncrement, postVelocity, postIncrement, direction, hitTime, bonusTime, hitDirection, score, scoreUpdated);
    }

    @Override
    public String toString() {
        return "BallData{" +
                "frames=" + frames +
                ", speed=" + speed +
                ", length=" + length +
                ", perfectPoint=" + perfectPoint +
                ", steps=" + steps +
                ", difficultModSteps=" + difficultModSteps +
                ", difficulty=" + difficulty +
                ", finalSpotIndex=" + finalSpotIndex +
                ", lineIndex=" + lineIndex +
                ", preVelocity=" + preVelocity +
                ", preIncrement=" + preIncrement +
                ", postVelocity=" + postVelocity +
                ", postIncrement=" + postIncrement +
                ", direction='" + direction + '\'' +
                ", hitTime=" + hitTime +
                ", bonusTime=" + bonusTime +
                ", hitDirection='" + hitDirection + '\'' +
                ", score=" + score +
                ", scoreUpdated=" + scoreUpdated +
                '}';
    }
}
