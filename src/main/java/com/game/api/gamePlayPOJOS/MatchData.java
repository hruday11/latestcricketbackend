package com.game.api.gameplaypojos;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.game.util.ObjectIdSerializer;
import org.bson.types.ObjectId;

public class MatchData {

    @JsonSerialize(using = ObjectIdSerializer.class)
    private ObjectId id;

    private String playerId;

    private String opponentId;

    private PlayerData playerData;

    private PlayerData opponentData;

    private String won;

    private String matchStatus = "New";

    private int count;

    private boolean isBot;

    private String botId;

    private String campaignId;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getOpponentId() {
        return opponentId;
    }

    public void setOpponentId(String opponentId) {
        this.opponentId = opponentId;
    }

    public PlayerData getPlayerData() {
        return playerData;
    }

    public void setPlayerData(PlayerData player1Data) {
        this.playerData = player1Data;
    }

    public PlayerData getOpponentData() {
        return opponentData;
    }

    public void setOpponentData(PlayerData opponentData) {
        this.opponentData = opponentData;
    }

    public String getWon() {
        return won;
    }

    public void setWon(String won) {
        this.won = won;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getMatchStatus() {
        return matchStatus;
    }

    public void setMatchStatus(String matchStatus) {
        this.matchStatus = matchStatus;
    }

    public boolean isBot() {
        return isBot;
    }

    public void setBot(boolean bot) {
        isBot = bot;
    }

    public String getBotId() {
        return botId;
    }

    public void setBotId(String botId) {
        this.botId = botId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }
}
