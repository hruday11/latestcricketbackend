package com.game.api.gameplaypojos.responsepojos;

import java.util.List;

public class SelectedBowlerData {

    private List<BowlerPlayerData> bowlerData;

    public void setBowlerData(List<BowlerPlayerData> bowlerData) {
        this.bowlerData = bowlerData;
    }

    public List<BowlerPlayerData> getBowlerData() {
        return bowlerData;
    }


}
