package com.game.api.gameplaypojos.responsepojos;

import com.game.api.gameplaypojos.ScoreData;

import java.util.List;

public class ScoreBoardData {

    private List<ScoreData> scoreBoardData;

    public List<ScoreData> getScoreBoardData() {
        return scoreBoardData;
    }

    public void setScoreBoardData(List<ScoreData> scoreBoardData) {
        this.scoreBoardData = scoreBoardData;
    }
}
