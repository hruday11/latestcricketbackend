package com.game.api.gameplaypojos.responsepojos;

import com.game.api.Bowler;

public class BowlerPlayerData{

    private String playerId;

    private Bowler bowler;

    public BowlerPlayerData(String playerId, Bowler bowler){
        this.playerId = playerId;
        this.bowler = bowler;
    }

    public void setBowler(Bowler bowler) {
        this.bowler = bowler;
    }

    public Bowler getBowler() {
        return bowler;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getPlayerId() {
        return playerId;
    }


}