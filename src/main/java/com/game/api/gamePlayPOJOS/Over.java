package com.game.api.gameplaypojos;

import java.util.*;

public class Over {

    public String bowlerId;

    private List<BallData> ballsData = new ArrayList<>();

    public String getBowlerId() {
        return bowlerId;
    }

    public void setBowlerId(String bowlerId) {
        this.bowlerId = bowlerId;
    }

    public List<BallData> getBallsData() {
        return ballsData;
    }

    public void setBallsData(List<BallData> ballsData) {
        this.ballsData = ballsData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Over over = (Over) o;
        return Objects.equals(bowlerId, over.bowlerId) &&
                Objects.equals(ballsData, over.ballsData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bowlerId, ballsData);
    }

    @Override
    public String toString() {
        return "Over{" +
                "bowlerId=" + bowlerId +
                ", ballsData=" + ballsData +
                '}';
    }

}
