package com.game.api.gameplaypojos;

import java.util.*;

public class PlayerData {

    private List<Over> overs;

    private List<Over> oversBowled;

    private int total;

    private double timingBonus;

    private int sixes;

    private int fours;

    private int out;


    public List<Over> getOvers() {
        return overs;
    }

    public List<Over> getOversBowled() {
        return oversBowled;
    }

    public void setOversBowled(List<Over> oversBowled) {
        this.oversBowled = oversBowled;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setOvers(List<Over> overs) {
        this.overs = overs;
    }

    public int getFours() {
        return fours;
    }

    public int getOut() {
        return out;
    }

    public int getSixes() {
        return sixes;
    }

    public double getTimingBonus() {
        return timingBonus;
    }

    public void setTimingBonus(double timingBonus) {
        this.timingBonus = timingBonus;
    }

    public void setFours(int fours) {
        this.fours = fours;
    }

    public void setOut(int out) {
        this.out = out;
    }

    public void setSixes(int sixes) {
        this.sixes = sixes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerData that = (PlayerData) o;
        return total == that.total &&
                timingBonus == that.timingBonus &&
                sixes == that.sixes &&
                fours == that.fours &&
                out == that.out &&
                Objects.equals(overs, that.overs) &&
                Objects.equals(oversBowled, that.oversBowled);
    }

    @Override
    public int hashCode() {
        return Objects.hash(overs, oversBowled, total, timingBonus, sixes, fours, out);
    }

    @Override
    public String toString() {
        return "PlayerData{" +
                "overs=" + overs +
                ", oversBowled=" + oversBowled +
                ", total=" + total +
                ", timingBonus=" + timingBonus +
                ", sixes=" + sixes +
                ", fours=" + fours +
                ", out=" + out +
                '}';
    }
}
