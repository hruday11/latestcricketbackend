package com.game.api.gameplaypojos;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class Range {

    private int max;

    @NotNull
    private int min;


    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Range range = (Range) o;
        return Float.compare(range.max, max) == 0 &&
                Float.compare(range.min, min) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(max, min);
    }

    @Override
    public String
    toString() {
        return "Range{" +
                "max=" + max +
                ", min=" + min +
                '}';
    }
}
