package com.game.api.gameplaypojos;

import java.util.List;

public class WinnerResponse {

    private List<WinnerPlayerData> playerData;

    private String won;

    public String getWon() {
        return won;
    }

    public List<WinnerPlayerData> getPlayerData() {
        return playerData;
    }

    public void setWon(String won) {
        this.won = won;
    }

    public void setPlayerData(List<WinnerPlayerData> playerData) {
        this.playerData = playerData;
    }
}
