package com.game.api.gameplaypojos;

import java.util.Objects;

public class WinnerPlayerData {

    private String playerId;

    private int runs;

    private int winningBonus;

    private double timingBonus;

    private double score;

    private int sixes;

    private int fours;

    private int wickets;

    public WinnerPlayerData(String playerId, int runs, int winningBonus, double timingBonus, double score, int sixes, int fours, int wickets){
        this.playerId = playerId;
        this.runs =  runs;
        this.winningBonus = winningBonus;
        this.timingBonus = timingBonus;
        this.score = score;
        this.sixes = sixes;
        this.fours = fours;
        this.wickets = wickets;
    }

    public double getTimingBonus() {
        return timingBonus;
    }

    public int getFours() {
        return fours;
    }

    public double getScore() {
        return score;
    }

    public int getRuns() {
        return runs;
    }

    public String getPlayerId() {
        return playerId;
    }

    public int getSixes() {
        return sixes;
    }

    public int getWickets() {
        return wickets;
    }

    public int getWinningBonus() {
        return winningBonus;
    }

    public void setTimingBonus(double timingBonus) {
        this.timingBonus = timingBonus;
    }

    public void setFours(int fours) {
        this.fours = fours;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public void setSixes(int sixes) {
        this.sixes = sixes;
    }

    public void setWickets(int wickets) {
        this.wickets = wickets;
    }

    public void setWinningBonus(int winningBonus) {
        this.winningBonus = winningBonus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WinnerPlayerData that = (WinnerPlayerData) o;
        return runs == that.runs &&
                winningBonus == that.winningBonus &&
                timingBonus == that.timingBonus &&
                score == that.score &&
                sixes == that.sixes &&
                fours == that.fours &&
                wickets == that.wickets &&
                Objects.equals(playerId, that.playerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerId, runs, winningBonus, timingBonus, score, sixes, fours, wickets);
    }

    @Override
    public String toString() {
        return "WinnerPlayerData{" +
                "playerId='" + playerId + '\'' +
                ", runs=" + runs +
                ", winningBonus=" + winningBonus +
                ", timingBonus=" + timingBonus +
                ", score=" + score +
                ", sixes=" + sixes +
                ", fours=" + fours +
                ", wickets=" + wickets +
                '}';
    }


}
