package com.game.api.gameplaypojos;

import java.util.Objects;

public class Score {

    private float time;

    private String hitDirection;

    private String playerId;

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public void setHitDirection(String hitDirection) {
        this.hitDirection = hitDirection;
    }

    public String getHitDirection() {
        return hitDirection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Score score = (Score) o;
        return Float.compare(score.time, time) == 0 &&
                Objects.equals(hitDirection, score.hitDirection) &&
                Objects.equals(playerId, score.playerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(time, hitDirection, playerId);
    }

    @Override
    public String toString() {
        return "Score{" +
                "time=" + time +
                ", hitDirection='" + hitDirection + '\'' +
                ", playerId='" + playerId + '\'' +
                '}';
    }
}
