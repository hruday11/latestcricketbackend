package com.game.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.game.api.campaignpojos.*;
import com.game.util.ObjectIdSerializer;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Objects;

public class  Campaign {

    @JsonSerialize(using = ObjectIdSerializer.class)
    private ObjectId id;

    private String campaignId;

    private String campaignName;

    private int lives;

    private  int noOfOvers;

    private int powerOver;

    private List<Team> teamsList;

    private List<CampaignOver> overs;

    private Stadium stadium;

    private List<String> tips;

    private LifeManagement lifeManagement;

    private CampaignActions campaignActions;

    private Advertisement advertisement;

    public ObjectId getId() {
        return id;
    }

    public int getNoOfOvers() {
        return noOfOvers;
    }

    public void setNoOfOvers(int noOfOvers) {
        this.noOfOvers = noOfOvers;
    }

    public List<Team> getTeamsList() {
        return teamsList;
    }

    public void setTeamsList(List<Team> teamsList) {
        this.teamsList = teamsList;
    }


    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public int getPowerOver() {
        return powerOver;
    }

    public void setPowerOver(int powerOver) {
        this.powerOver = powerOver;
    }

    public List<CampaignOver> getOvers() {
        return overs;
    }

    public List<String> getTips() {
        return tips;
    }

    public Stadium getStadium() {
        return stadium;
    }

    public Advertisement getAdvertisement() {
        return advertisement;
    }

    public CampaignActions getCampaignActions() {
        return campaignActions;
    }

    public LifeManagement getLifeManagement() {
        return lifeManagement;
    }

    public void setOvers(List<CampaignOver> overs) {
        this.overs = overs;
    }

    public void setAdvertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
    }

    public void setCampaignActions(CampaignActions campaignActions) {
        this.campaignActions = campaignActions;
    }

    public void setLifeManagement(LifeManagement lifeManagement) {
        this.lifeManagement = lifeManagement;
    }

    public void setStadium(Stadium stadium) {
        this.stadium = stadium;
    }

    public void setTips(List<String> tips) {
        this.tips = tips;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Campaign campaign = (Campaign) o;
        return lives == campaign.lives &&
                noOfOvers == campaign.noOfOvers &&
                powerOver == campaign.powerOver &&
                Objects.equals(id, campaign.id) &&
                Objects.equals(campaignId, campaign.campaignId) &&
                Objects.equals(campaignName, campaign.campaignName) &&
                Objects.equals(teamsList, campaign.teamsList)&&
                Objects.equals(overs, campaign.overs) &&
                Objects.equals(stadium, campaign.stadium) &&
                Objects.equals(tips, campaign.tips) &&
                Objects.equals(lifeManagement, campaign.lifeManagement) &&
                Objects.equals(campaignActions, campaign.campaignActions) &&
                Objects.equals(advertisement, campaign.advertisement);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, campaignId, campaignName, lives, noOfOvers, powerOver, teamsList, overs, stadium, tips, lifeManagement, campaignActions, advertisement);
    }

    @Override
    public String toString() {
        return "Campaign{" +
                "id=" + id +
                ", campaignId='" + campaignId + '\'' +
                ", campaignName='" + campaignName + '\'' +
                ", lives=" + lives +
                ", noOfOvers=" + noOfOvers +
                ", powerOver=" + powerOver +
                ", teamsList=" + teamsList +
                ", overs=" + overs +
                ", stadium=" + stadium +
                ", tips=" + tips +
                ", lifeManagement=" + lifeManagement +
                ", campaignActions=" + campaignActions +
                ", advertisement=" + advertisement +
                '}';
    }
}
