package com.game.api.fkpojos;

import java.util.Objects;

public class UserInfoWithReward {

    private String userName;

    private int score;

    private int rank;

    private String rewardId;

    public int getScore() {
        return score;
    }

    public String getUserName() {
        return userName;
    }

    public int getRank() {
        return rank;
    }

    public String getRewardId() {
        return rewardId;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInfoWithReward that = (UserInfoWithReward) o;
        return score == that.score &&
                rank == that.rank &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(rewardId, that.rewardId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, score, rank, rewardId);
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "userName='" + userName + '\'' +
                ", score=" + score +
                ", rank=" + rank +
                ", rewardId='" + rewardId + '\'' +
                '}';
    }
}
