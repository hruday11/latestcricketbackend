package com.game.api.fkpojos;

import java.util.Objects;

public class OpponentPlayInfo {

    private String playWith;

    private String tokenId;

    private String team;

    public String getTokenId() {
        return tokenId;
    }

    public String getTeam() {
        return team;
    }

    public String getPlayWith() {
        return playWith;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public void setPlayWith(String playWith) {
        this.playWith = playWith;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OpponentPlayInfo that = (OpponentPlayInfo) o;
        return Objects.equals(playWith, that.playWith) &&
                Objects.equals(tokenId, that.tokenId) &&
                Objects.equals(team, that.team);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playWith, tokenId, team);
    }

    @Override
    public String toString() {
        return "OpponentPlayInfo{" +
                "playWith='" + playWith + '\'' +
                ", tokenId='" + tokenId + '\'' +
                ", team='" + team + '\'' +
                '}';
    }
}
