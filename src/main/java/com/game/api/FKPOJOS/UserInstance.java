package com.game.api.fkpojos;

import java.util.Objects;

public class UserInstance {

    private String userId;

    private String userName;

    private Lives lives;

    private GetSummary userGameInstance;

    public String getUserId() {
        return userId;
    }

    public Lives getLives() {
        return lives;
    }
    public String getUserName() {
        return userName;
    }

    public void setLives(Lives lives) {
        this.lives = lives;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public GetSummary getUserGameInstance() {
        return userGameInstance;
    }

    public void setUserGameInstance(GetSummary userGameInstance) {
        this.userGameInstance = userGameInstance;
    }

    @Override
    public String toString() {
        return "UserInstance{" +
                "userGameInstance=" + userGameInstance +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInstance that = (UserInstance) o;
        return Objects.equals(userGameInstance, that.userGameInstance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userGameInstance);
    }
}
