package com.game.api.fkpojos;

import java.util.Objects;

public class DisplayInfo {

    private String title;

    private String description;

    private String imageUrl;

    private String landingUrl;

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getLandingUrl() {
        return landingUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setLandingUrl(String landingUrl) {
        this.landingUrl = landingUrl;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DisplayInfo that = (DisplayInfo) o;
        return Objects.equals(title, that.title) &&
                Objects.equals(description, that.description) &&
                Objects.equals(imageUrl, that.imageUrl) &&
                Objects.equals(landingUrl, that.landingUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, description, imageUrl, landingUrl);
    }

    @Override
    public String toString() {
        return "DisplayInfo{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", landingUrl='" + landingUrl + '\'' +
                '}';
    }
}
