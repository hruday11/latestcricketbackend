package com.game.api.fkpojos;

import java.util.Objects;

public class LiveLeaderboardRequest {

    private String campaignId;

    private int startFrom = 1;

    private int pageSize = 500;

    private String league;

    private String  leaderboardId;

    private String tokenId;

    public String getTokenId() {
        return tokenId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public String getLeague() {
        return league;
    }

    public String getLeaderboardId() {
        return leaderboardId;
    }

    public int getStartFrom() {
        return startFrom;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public void setStartFrom(int startFrom) {
        this.startFrom = startFrom;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public void setLeaderboardId(String leaderboardId) {
        this.leaderboardId = leaderboardId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LiveLeaderboardRequest that = (LiveLeaderboardRequest) o;
        return startFrom == that.startFrom &&
                pageSize == that.pageSize &&
                Objects.equals(campaignId, that.campaignId) &&
                Objects.equals(league, that.league) &&
                Objects.equals(leaderboardId, that.leaderboardId) &&
                Objects.equals(tokenId, that.tokenId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(campaignId, startFrom, pageSize, league, leaderboardId, tokenId);
    }

    @Override
    public String toString() {
        return "LiveLeaderboardRequest{" +
                "campaignId='" + campaignId + '\'' +
                ", startFrom=" + startFrom +
                ", pageSize=" + pageSize +
                ", league='" + league + '\'' +
                ", leaderboardId='" + leaderboardId + '\'' +
                ", tokenId='" + tokenId + '\'' +
                '}';
    }
}
