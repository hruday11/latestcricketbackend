package com.game.api.fkpojos;

import java.util.Objects;

public class FKScoreAndType {

    private String type;

    private int score;

    public String getType() {
        return type;
    }

    public int getScore() {
        return score;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FKScoreAndType that = (FKScoreAndType) o;
        return score == that.score &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, score);
    }

    @Override
    public String toString() {
        return "FKScoreAndType{" +
                "type='" + type + '\'' +
                ", score=" + score +
                '}';
    }
}
