package com.game.api.fkpojos;

import java.util.Objects;

public class StageInfo {
    private int total;

    private int latestIncomplete;

    private boolean isGameCompleted;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getLatestIncomplete() {
        return latestIncomplete;
    }

    public void setGameCompleted(boolean gameCompleted) {
        isGameCompleted = gameCompleted;
    }

    public boolean isGameCompleted() {
        return isGameCompleted;
    }

    public void setLatestIncomplete(int latestIncomplete) {
        this.latestIncomplete = latestIncomplete;
    }

    @Override
    public String toString() {
        return "StageInfo{" +
                "total=" + total +
                ", latestIncomplete=" + latestIncomplete +
                ", isGameCompleted=" + isGameCompleted +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StageInfo stageInfo = (StageInfo) o;
        return total == stageInfo.total &&
                latestIncomplete == stageInfo.latestIncomplete &&
                isGameCompleted == stageInfo.isGameCompleted;
    }

    @Override
    public int hashCode() {
        return Objects.hash(total, latestIncomplete, isGameCompleted);
    }
}
