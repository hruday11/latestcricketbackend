package com.game.api.fkpojos;


import java.util.*;

public class FKScoreBoard {

    private String tokenId;

    private String campaignId;

    private String team;

    private long requestTime;

    private String channel;

    private String ipAddress;

    private int stageNumber;

    private int retryNumber;

    private String status = "FAILED";

    private List<FKScoreAndType> scores;

    private OpponentPlayInfo playInfo;

    public String getTeam() {
        return team;
    }

    public String getTokenId() {
        return tokenId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getChannel() {
        return channel;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public int getStageNumber() {
        return stageNumber;
    }

    public int getRetryNumber() {
        return retryNumber;
    }

    public List<FKScoreAndType> getScores() {
        return scores;
    }

    public OpponentPlayInfo getPlayInfo() {
        return playInfo;
    }

    public String getStatus() {
        return status;
    }

    public void setStageNumber(int stageNumber) {
        this.stageNumber = stageNumber;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public void setRetryNumber(int retryNumber) {
        this.retryNumber = retryNumber;
    }

    public void setScores(List<FKScoreAndType> scores) {
        this.scores = scores;
    }

    public void setPlayInfo(OpponentPlayInfo playInfo) {
        this.playInfo = playInfo;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FKScoreBoard that = (FKScoreBoard) o;
        return requestTime == that.requestTime &&
                stageNumber == that.stageNumber &&
                retryNumber == that.retryNumber &&
                Objects.equals(tokenId, that.tokenId) &&
                Objects.equals(campaignId, that.campaignId) &&
                Objects.equals(team, that.team) &&
                Objects.equals(channel, that.channel) &&
                Objects.equals(ipAddress, that.ipAddress) &&
                Objects.equals(status, that.status) &&
                Objects.equals(scores, that.scores) &&
                Objects.equals(playInfo, that.playInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tokenId, campaignId, team, requestTime, channel, ipAddress, stageNumber, retryNumber, status, scores, playInfo);
    }

    @Override
    public String toString() {
        return "FKScoreBoard{" +
                "tokenId='" + tokenId + '\'' +
                ", campaignId='" + campaignId + '\'' +
                ", team='" + team + '\'' +
                ", requestTime=" + requestTime +
                ", channel='" + channel + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", stageNumber=" + stageNumber +
                ", retryNumber=" + retryNumber +
                ", status='" + status + '\'' +
                ", scores=" + scores +
                ", playInfo=" + playInfo +
                '}';
    }
}
