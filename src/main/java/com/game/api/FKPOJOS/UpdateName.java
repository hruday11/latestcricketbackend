package com.game.api.fkpojos;

import java.util.Objects;

public class UpdateName {

    private String gameName = "cricket";

    private String tokenId;

    private String campaignId;

    private String userName;

    private int retryNumber = 0;

    public UpdateName(String gameName, String tokenId, String campaignId, String userName, int retryNumber){
        this.gameName = gameName;
        this.tokenId = tokenId;
        this.campaignId = campaignId;
        this.userName = userName;
        this.retryNumber = retryNumber;
    }

    public String getUserName() {
        return userName;
    }

    public int getRetryNumber() {
        return retryNumber;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public String getGameName() {
        return gameName;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public void setRetryNumber(int retryNumber) {
        this.retryNumber = retryNumber;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    @Override
    public String toString() {
        return "UpdateName{" +
                "gameName='" + gameName + '\'' +
                ", tokenId='" + tokenId + '\'' +
                ", campaignId='" + campaignId + '\'' +
                ", userName='" + userName + '\'' +
                ", retryNumber=" + retryNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateName that = (UpdateName) o;
        return retryNumber == that.retryNumber &&
                Objects.equals(gameName, that.gameName) &&
                Objects.equals(tokenId, that.tokenId) &&
                Objects.equals(campaignId, that.campaignId) &&
                Objects.equals(userName, that.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameName, tokenId, campaignId, userName, retryNumber);
    }
}
