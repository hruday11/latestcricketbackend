package com.game.api.fkpojos;

import java.util.*;

public class CampaignLeaderboard {

    private boolean hasMoreData;

    private int nextAvailableFrom;

    private int count;

    private UserInfo userinfo;

    private List<UserInfoWithReward> leaderboard;

    private List<Reward> rewards;

    public int getCount() {
        return count;
    }

    public List<Reward> getRewards() {
        return rewards;
    }

    public List<UserInfoWithReward> getLeaderboard() {
        return leaderboard;
    }

    public int getNextAvailableFrom() {
        return nextAvailableFrom;
    }

    public UserInfo getUserinfo() {
        return userinfo;
    }

    public boolean isHasMoreData() {
        return hasMoreData;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setHasMoreData(boolean hasMoreData) {
        this.hasMoreData = hasMoreData;
    }

    public void setLeaderboard(List<UserInfoWithReward> leaderboard) {
        this.leaderboard = leaderboard;
    }

    public void setNextAvailableFrom(int nextAvailableFrom) {
        this.nextAvailableFrom = nextAvailableFrom;
    }

    public void setRewards(List<Reward> rewards) {
        this.rewards = rewards;
    }

    public void setUserinfo(UserInfo userinfo) {
        this.userinfo = userinfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CampaignLeaderboard that = (CampaignLeaderboard) o;
        return hasMoreData == that.hasMoreData &&
                nextAvailableFrom == that.nextAvailableFrom &&
                count == that.count &&
                Objects.equals(userinfo, that.userinfo) &&
                Objects.equals(leaderboard, that.leaderboard) &&
                Objects.equals(rewards, that.rewards);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hasMoreData, nextAvailableFrom, count, userinfo, leaderboard, rewards);
    }

    @Override
    public String toString() {
        return "CampaignLeaderboard{" +
                "hasMoreData=" + hasMoreData +
                ", nextAvailableFrom=" + nextAvailableFrom +
                ", count=" + count +
                ", userinfo=" + userinfo +
                ", leaderboard=" + leaderboard +
                ", rewards=" + rewards +
                '}';
    }
}