package com.game.api.fkpojos;

import java.util.Objects;

public class UserInfo {

    private String userName;

    private int score;

    private int rank;

    public int getScore() {
        return score;
    }

    public String getUserName() {
        return userName;
    }

    public int getRank() {
        return rank;
    }


    public void setScore(int score) {
        this.score = score;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInfo userInfo = (UserInfo) o;
        return score == userInfo.score &&
                rank == userInfo.rank &&
                Objects.equals(userName, userInfo.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, score, rank);
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "userName='" + userName + '\'' +
                ", score=" + score +
                ", rank='" + rank + '\'' +
                '}';
    }
}
