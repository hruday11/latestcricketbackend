package com.game.api.fkpojos;

import java.util.*;

public class LiveLeaderboard {

    private boolean hasMoreData;

    private int nextAvailableFrom;

    private int count;

    private UserInfo userInfo;

    private List<UserInfo> leaderboard;

    public int getNextAvailableFrom() {
        return nextAvailableFrom;
    }

    public List<UserInfo> getLeaderboard() {
        return leaderboard;
    }

    public int getCount() {
        return count;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public boolean isHasMoreData() {
        return hasMoreData;
    }

    public void setNextAvailableFrom(int nextAvailableFrom) {
        this.nextAvailableFrom = nextAvailableFrom;
    }

    public void setLeaderboard(List<UserInfo> leaderboard) {
        this.leaderboard = leaderboard;
    }

    public void setHasMoreData(boolean hasMoreData) {
        this.hasMoreData = hasMoreData;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LiveLeaderboard that = (LiveLeaderboard) o;
        return hasMoreData == that.hasMoreData &&
                nextAvailableFrom == that.nextAvailableFrom &&
                count == that.count &&
                Objects.equals(userInfo, that.userInfo) &&
                Objects.equals(leaderboard, that.leaderboard);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hasMoreData, nextAvailableFrom, count, userInfo, leaderboard);
    }

    @Override
    public String toString() {
        return "LiveLeaderboard{" +
                "hasMoreData=" + hasMoreData +
                ", nextAvailableFrom=" + nextAvailableFrom +
                ", count=" + count +
                ", userInfo=" + userInfo +
                ", leaderboard=" + leaderboard +
                '}';
    }
}
