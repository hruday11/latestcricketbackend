package com.game.api.fkpojos;

import java.util.Objects;

public class BlackListRequest {
    private String gameName;

    private String tokenId;

    private String campaignId;

    private FraudReason reason;

    private int retryNumber;

    public int getRetryNumber() {
        return retryNumber;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public String getTokenId() {
        return tokenId;
    }

    public String getGameName() {
        return gameName;
    }

    public FraudReason getReason() {
        return reason;
    }

    public void setRetryNumber(int retryNumber) {
        this.retryNumber = retryNumber;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public void setReason(FraudReason reason) {
        this.reason = reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlackListRequest that = (BlackListRequest) o;
        return retryNumber == that.retryNumber &&
                Objects.equals(gameName, that.gameName) &&
                Objects.equals(tokenId, that.tokenId) &&
                Objects.equals(campaignId, that.campaignId) &&
                Objects.equals(reason, that.reason);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameName, tokenId, campaignId, reason, retryNumber);
    }

    @Override
    public String toString() {
        return "BlackListRequest{" +
                "gameName='" + gameName + '\'' +
                ", tokenId='" + tokenId + '\'' +
                ", campaignId='" + campaignId + '\'' +
                ", reason=" + reason +
                ", retryNumber=" + retryNumber +
                '}';
    }
}
