package com.game.api.fkpojos;

public class StageWiseScore {
    private String type;
    private float score;
    private Object stageWiseScore;

    public float getScore() {
        return score;
    }

    public Object getStageWiseScore() {
        return stageWiseScore;
    }

    public String getType() {
        return type;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public void setStageWiseScore(Object stageWiseScore) {
        this.stageWiseScore = stageWiseScore;
    }

    public void setType(String type) {
        this.type = type;
    }
}
