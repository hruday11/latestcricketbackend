package com.game.api.fkpojos;

public class Lives {
    private int available;

    public void setAvailable(int available) {
        this.available = available;
    }

    public int getAvailable() {
        return available;
    }

}
