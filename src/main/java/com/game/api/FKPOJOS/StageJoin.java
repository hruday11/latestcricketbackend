package com.game.api.fkpojos;

import java.util.Objects;

public class StageJoin {
    private String tokenId;

    private String campaignId;

    private String team;

    private long requestTime;

    private String channel;

    private String ipAddress;

    private int stageNumber;

    private int retryNumber;

    public String getCampaignId() {
        return campaignId;
    }

    public String getTokenId() {
        return tokenId;
    }

    public int getRetryNumber() {
        return retryNumber;
    }

    public int getStageNumber() {
        return stageNumber;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public String getChannel() {
        return channel;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getTeam() {
        return team;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public void setRetryNumber(int retryNumber) {
        this.retryNumber = retryNumber;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    public void setStageNumber(int stageNumber) {
        this.stageNumber = stageNumber;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    @Override
    public String toString() {
        return "StageJoin{" +
                "tokenId='" + tokenId + '\'' +
                ", campaignId='" + campaignId + '\'' +
                ", team='" + team + '\'' +
                ", requestTime=" + requestTime +
                ", channel='" + channel + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", stageNumber=" + stageNumber +
                ", retryNumber=" + retryNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StageJoin stageJoin = (StageJoin) o;
        return requestTime == stageJoin.requestTime &&
                stageNumber == stageJoin.stageNumber &&
                retryNumber == stageJoin.retryNumber &&
                Objects.equals(tokenId, stageJoin.tokenId) &&
                Objects.equals(campaignId, stageJoin.campaignId) &&
                Objects.equals(team, stageJoin.team) &&
                Objects.equals(channel, stageJoin.channel) &&
                Objects.equals(ipAddress, stageJoin.ipAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tokenId, campaignId, team, requestTime, channel, ipAddress, stageNumber, retryNumber);
    }
}
