package com.game.api.fkpojos;

import java.util.Objects;

public class Reward {

    private String rewardId;

    private String rewardType;

    private int winnerCount;

    private DisplayInfo displayInfo;

    private boolean isPrimary;

    private int priority;

    private int actualPriceValue;

    private int discountedPriceValue;

    public String getRewardId() {
        return rewardId;
    }

    public DisplayInfo getDisplayInfo() {
        return displayInfo;
    }

    public int getActualPriceValue() {
        return actualPriceValue;
    }

    public int getDiscountedPriceValue() {
        return discountedPriceValue;
    }

    public int getPriority() {
        return priority;
    }

    public int getWinnerCount() {
        return winnerCount;
    }

    public String getRewardType() {
        return rewardType;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public void setActualPriceValue(int actualPriceValue) {
        this.actualPriceValue = actualPriceValue;
    }

    public void setDiscountedPriceValue(int discountedPriceValue) {
        this.discountedPriceValue = discountedPriceValue;
    }

    public void setDisplayInfo(DisplayInfo displayInfo) {
        this.displayInfo = displayInfo;
    }

    public void setPrimary(boolean primary) {
        isPrimary = primary;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public void setWinnerCount(int winnerCount) {
        this.winnerCount = winnerCount;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reward reward = (Reward) o;
        return winnerCount == reward.winnerCount &&
                isPrimary == reward.isPrimary &&
                priority == reward.priority &&
                actualPriceValue == reward.actualPriceValue &&
                discountedPriceValue == reward.discountedPriceValue &&
                Objects.equals(rewardId, reward.rewardId) &&
                Objects.equals(rewardType, reward.rewardType) &&
                Objects.equals(displayInfo, reward.displayInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rewardId, rewardType, winnerCount, displayInfo, isPrimary, priority, actualPriceValue, discountedPriceValue);
    }

    @Override
    public String toString() {
        return "Reward{" +
                "rewardId='" + rewardId + '\'' +
                ", rewardType='" + rewardType + '\'' +
                ", winnerCount=" + winnerCount +
                ", displayInfo=" + displayInfo +
                ", isPrimary=" + isPrimary +
                ", priority=" + priority +
                ", actualPriceValue=" + actualPriceValue +
                ", discountedPriceValue=" + discountedPriceValue +
                '}';
    }
}
