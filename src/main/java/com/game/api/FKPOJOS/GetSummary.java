package com.game.api.fkpojos;

import java.util.*;

public class GetSummary {

    private StageInfo stageInfo;

    private List<StageWiseScore> scores;

    public List<StageWiseScore> getScores() {
        return scores;
    }

    public StageInfo getStageInfo() {
        return stageInfo;
    }

    public void setScores(List<StageWiseScore> scores) {
        this.scores = scores;
    }

    public void setStageInfo(StageInfo stageInfo) {
        this.stageInfo = stageInfo;
    }

    @Override
    public String toString() {
        return "GetSummary{" +
                ", stageInfo=" + stageInfo +
                ", scores=" + scores +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetSummary that = (GetSummary) o;
        return Objects.equals(stageInfo, that.stageInfo) &&
                Objects.equals(scores, that.scores);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stageInfo, scores);
    }
}

