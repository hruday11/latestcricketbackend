package com.game.api.fkpojos;

import java.util.Objects;

public class LeaderboardRequest {

    private String campaignId;

    private int startFrom;

    private int pageSize;

    private String league;

    private String leaderboardId;

    private String tokenId;

    private String strategyId = "CP_LEAGUE_PLAYER_SCORE";

    public String getTokenId() {
        return tokenId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getStartFrom() {
        return startFrom;
    }

    public String getLeaderboardId() {
        return leaderboardId;
    }

    public String getLeague() {
        return league;
    }

    public String getStrategyId() {
        return strategyId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public void setLeaderboardId(String leaderboardId) {
        this.leaderboardId = leaderboardId;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setStartFrom(int startFrom) {
        this.startFrom = startFrom;
    }

    public void setStrategyId(String strategyId) {
        this.strategyId = strategyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeaderboardRequest that = (LeaderboardRequest) o;
        return startFrom == that.startFrom &&
                pageSize == that.pageSize &&
                Objects.equals(campaignId, that.campaignId) &&
                Objects.equals(league, that.league) &&
                Objects.equals(leaderboardId, that.leaderboardId) &&
                Objects.equals(tokenId, that.tokenId) &&
                Objects.equals(strategyId, that.strategyId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(campaignId, startFrom, pageSize, league, leaderboardId, tokenId, strategyId);
    }

    @Override
    public String toString() {
        return "LeaderboardRequest{" +
                "campaignId='" + campaignId + '\'' +
                ", startFrom=" + startFrom +
                ", pageSize=" + pageSize +
                ", league='" + league + '\'' +
                ", leaderboardId='" + leaderboardId + '\'' +
                ", tokenId='" + tokenId + '\'' +
                ", strategyId='" + strategyId + '\'' +
                '}';
    }
}
