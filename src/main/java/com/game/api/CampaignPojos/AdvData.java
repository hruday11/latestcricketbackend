package com.game.api.campaignpojos;

import java.util.Objects;

public class AdvData {

    private String videoLink;

    private int toBePlayedAfterOverNumber;

    private int duration;

    private String interstitialLink;

    private String maxClickableCTA;

    private String cta1LabelName;

    private String cta1LabelLink;

    private String cta1Color;

    private String cta2LabelName;

    private String cta2LabelLink;

    private String cta2Color;

    public int getDuration() {
        return duration;
    }

    public int getToBePlayedAfterOverNumber() {
        return toBePlayedAfterOverNumber;
    }

    public String getCta1Color() {
        return cta1Color;
    }

    public String getCta1LabelLink() {
        return cta1LabelLink;
    }

    public String getCta1LabelName() {
        return cta1LabelName;
    }

    public String getCta2Color() {
        return cta2Color;
    }

    public String getCta2LabelLink() {
        return cta2LabelLink;
    }

    public String getCta2LabelName() {
        return cta2LabelName;
    }

    public void setCta1Color(String cta1Color) {
        this.cta1Color = cta1Color;
    }

    public void setCta1LabelLink(String cta1LabelLink) {
        this.cta1LabelLink = cta1LabelLink;
    }

    public void setCta1LabelName(String cta1LabelName) {
        this.cta1LabelName = cta1LabelName;
    }

    public void setCta2Color(String cta2Color) {
        this.cta2Color = cta2Color;
    }

    public void setCta2LabelLink(String cta2LabelLink) {
        this.cta2LabelLink = cta2LabelLink;
    }

    public void setCta2LabelName(String cta2LabelName) {
        this.cta2LabelName = cta2LabelName;
    }

    public String getInterstitialLink() {
        return interstitialLink;
    }

    public String getMaxClickableCTA() {
        return maxClickableCTA;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setInterstitialLink(String interstitialLink) {
        this.interstitialLink = interstitialLink;
    }

    public void setMaxClickableCTA(String maxClickableCTA) {
        this.maxClickableCTA = maxClickableCTA;
    }

    public void setToBePlayedAfterOverNumber(int toBePlayedAfterOverNumber) {
        this.toBePlayedAfterOverNumber = toBePlayedAfterOverNumber;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdvData advData = (AdvData) o;
        return toBePlayedAfterOverNumber == advData.toBePlayedAfterOverNumber &&
                duration == advData.duration &&
                Objects.equals(videoLink, advData.videoLink) &&
                Objects.equals(interstitialLink, advData.interstitialLink) &&
                Objects.equals(maxClickableCTA, advData.maxClickableCTA) &&
                Objects.equals(cta1LabelName, advData.cta1LabelName) &&
                Objects.equals(cta1LabelLink, advData.cta1LabelLink) &&
                Objects.equals(cta1Color, advData.cta1Color) &&
                Objects.equals(cta2LabelName, advData.cta2LabelName) &&
                Objects.equals(cta2LabelLink, advData.cta2LabelLink) &&
                Objects.equals(cta2Color, advData.cta2Color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(videoLink, toBePlayedAfterOverNumber, duration, interstitialLink, maxClickableCTA, cta1LabelName, cta1LabelLink, cta1Color, cta2LabelName, cta2LabelLink, cta2Color);
    }

    @Override
    public String toString() {
        return "AdvData{" +
                "videoLink='" + videoLink + '\'' +
                ", toBePlayedAfterOverNumber=" + toBePlayedAfterOverNumber +
                ", duration=" + duration +
                ", interstitialLink='" + interstitialLink + '\'' +
                ", maxClickableCTA='" + maxClickableCTA + '\'' +
                ", cta1LabelName='" + cta1LabelName + '\'' +
                ", cta1LabelLink='" + cta1LabelLink + '\'' +
                ", cta1Color='" + cta1Color + '\'' +
                ", cta2LabelName='" + cta2LabelName + '\'' +
                ", cta2LabelLink='" + cta2LabelLink + '\'' +
                ", cta2Color='" + cta2Color + '\'' +
                '}';
    }
}
