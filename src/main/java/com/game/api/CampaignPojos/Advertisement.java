package com.game.api.campaignpojos;

import java.util.Objects;

public class Advertisement {


    private AdvData videoOnly;

    private AdvData interstitialOnly;

    private AdvData videoInterstitial;

    public AdvData getInterstitialOnly() {
        return interstitialOnly;
    }

    public AdvData getVideoInterstitial() {
        return videoInterstitial;
    }

    public AdvData getVideoOnly() {
        return videoOnly;
    }

    public void setInterstitialOnly(AdvData interstitialOnly) {
        this.interstitialOnly = interstitialOnly;
    }

    public void setVideoInterstitial(AdvData videoInterstitial) {
        this.videoInterstitial = videoInterstitial;
    }

    public void setVideoOnly(AdvData videoOnly) {
        this.videoOnly = videoOnly;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Advertisement that = (Advertisement) o;
        return Objects.equals(videoOnly, that.videoOnly) &&
                Objects.equals(interstitialOnly, that.interstitialOnly) &&
                Objects.equals(videoInterstitial, that.videoInterstitial);
    }

    @Override
    public int hashCode() {
        return Objects.hash(videoOnly, interstitialOnly, videoInterstitial);
    }

    @Override
    public String toString() {
        return "Advertisement{" +
                "videoOnly=" + videoOnly +
                ", interstitialOnly=" + interstitialOnly +
                ", videoInterstitial=" + videoInterstitial +
                '}';
    }
}
