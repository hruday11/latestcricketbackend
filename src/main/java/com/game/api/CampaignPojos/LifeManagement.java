package com.game.api.campaignpojos;

import java.util.Objects;

public class LifeManagement {

    private int freeChances;
    private int maxActionChances;
    private int gemsRequiredForChance;
    private int maxGemChances;

    public int getFreeChances() {
        return freeChances;
    }

    public int getGemsRequiredForChance() {
        return gemsRequiredForChance;
    }

    public int getMaxActionChances() {
        return maxActionChances;
    }

    public int getMaxGemChances() {
        return maxGemChances;
    }

    public void setGemsRequiredForChance(int gemsRequiredForChance) {
        this.gemsRequiredForChance = gemsRequiredForChance;
    }

    public void setFreeChances(int freeChances) {
        this.freeChances = freeChances;
    }

    public void setMaxActionChances(int maxActionChances) {
        this.maxActionChances = maxActionChances;
    }

    public void setMaxGemChances(int maxGemChances) {
        this.maxGemChances = maxGemChances;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LifeManagement that = (LifeManagement) o;
        return freeChances == that.freeChances &&
                maxActionChances == that.maxActionChances &&
                gemsRequiredForChance == that.gemsRequiredForChance &&
                maxGemChances == that.maxGemChances;
    }

    @Override
    public int hashCode() {
        return Objects.hash(freeChances, maxActionChances, gemsRequiredForChance, maxGemChances);
    }

    @Override
    public String toString() {
        return "LifeManagement{" +
                "freeChances=" + freeChances +
                ", maxActionChances=" + maxActionChances +
                ", gemsRequiredForChance=" + gemsRequiredForChance +
                ", maxGemChances=" + maxGemChances +
                '}';
    }
}
