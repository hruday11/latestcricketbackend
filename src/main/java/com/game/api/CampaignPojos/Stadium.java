package com.game.api.campaignpojos;

import java.util.List;
import java.util.Objects;

public class Stadium {

    private String name;

    private List<String> assets;

    public String getName() {
        return name;
    }

    public List<String> getAssets() {
        return assets;
    }

    public void setAssets(List<String> assets) {
        this.assets = assets;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stadium stadium = (Stadium) o;
        return Objects.equals(name, stadium.name) &&
                Objects.equals(assets, stadium.assets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, assets);
    }

    @Override
    public String toString() {
        return "Stadium{" +
                "name='" + name + '\'' +
                ", assets=" + assets +
                '}';
    }
}
