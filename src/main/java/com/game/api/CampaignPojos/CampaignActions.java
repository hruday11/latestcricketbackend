package com.game.api.campaignpojos;

import java.util.Objects;

public class CampaignActions {

    private String actionType;

    private String actionTitle;

    private String availableChannels;

    private String actionDisplayLogo;

    private String actionDescription;

    private String landingUrl;

    public String getLandingUrl() {
        return landingUrl;
    }

    public String getActionDescription() {
        return actionDescription;
    }

    public String getActionDisplayLogo() {
        return actionDisplayLogo;
    }

    public String getActionTitle() {
        return actionTitle;
    }

    public String getActionType() {
        return actionType;
    }

    public String getAvailableChannels() {
        return availableChannels;
    }

    public void setLandingUrl(String landingUrl) {
        this.landingUrl = landingUrl;
    }

    public void setActionDescription(String actionDescription) {
        this.actionDescription = actionDescription;
    }

    public void setActionDisplayLogo(String actionDisplayLogo) {
        this.actionDisplayLogo = actionDisplayLogo;
    }

    public void setActionTitle(String actionTitle) {
        this.actionTitle = actionTitle;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public void setAvailableChannels(String availableChannels) {
        this.availableChannels = availableChannels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CampaignActions that = (CampaignActions) o;
        return Objects.equals(actionType, that.actionType) &&
                Objects.equals(actionTitle, that.actionTitle) &&
                Objects.equals(availableChannels, that.availableChannels) &&
                Objects.equals(actionDisplayLogo, that.actionDisplayLogo) &&
                Objects.equals(actionDescription, that.actionDescription) &&
                Objects.equals(landingUrl, that.landingUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(actionType, actionTitle, availableChannels, actionDisplayLogo, actionDescription, landingUrl);
    }

    @Override
    public String toString() {
        return "CampaignActions{" +
                "actionType='" + actionType + '\'' +
                ", actionTitle='" + actionTitle + '\'' +
                ", availableChannels='" + availableChannels + '\'' +
                ", actionDisplayLogo='" + actionDisplayLogo + '\'' +
                ", actionDescription='" + actionDescription + '\'' +
                ", landingUrl='" + landingUrl + '\'' +
                '}';
    }
}
