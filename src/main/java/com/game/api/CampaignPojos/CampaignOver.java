package com.game.api.campaignpojos;

import com.game.api.gameplaypojos.Range;

import java.util.Objects;

public class CampaignOver {

    private Range ball1;

    private Range ball2;

    private Range ball3;

    private Range ball4;

    private Range ball5;

    private Range ball6;

    public Range getBall1() {
        return ball1;
    }

    public Range getBall2() {
        return ball2;
    }

    public Range getBall3() {
        return ball3;
    }

    public Range getBall4() {
        return ball4;
    }

    public Range getBall5() {
        return ball5;
    }

    public Range getBall6() {
        return ball6;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CampaignOver that = (CampaignOver) o;
        return Objects.equals(ball1, that.ball1) &&
                Objects.equals(ball2, that.ball2) &&
                Objects.equals(ball3, that.ball3) &&
                Objects.equals(ball4, that.ball4) &&
                Objects.equals(ball5, that.ball5) &&
                Objects.equals(ball6, that.ball6);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ball1, ball2, ball3, ball4, ball5, ball6);
    }

    @Override
    public String toString() {
        return "CampaignOver{" +
                "ball1=" + ball1 +
                ", ball2=" + ball2 +
                ", ball3=" + ball3 +
                ", ball4=" + ball4 +
                ", ball5=" + ball5 +
                ", ball6=" + ball6 +
                '}';
    }
}
