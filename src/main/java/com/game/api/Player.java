package com.game.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.game.util.ObjectIdSerializer;
import org.bson.types.ObjectId;
import java.util.List;
import java.util.Objects;

public class Player {

    @JsonSerialize(using = ObjectIdSerializer.class)
    private ObjectId id;

    private String playerName;

    @JsonSerialize(using = ObjectIdSerializer.class)
    private ObjectId teamId;


    private String campaignId;

    private int eloRating = 1200;

    private int score;

    private int highScore;

    private int lives;

    private String status = "OFFLINE";

    private List<Object> matchesPlayed;

    private String tokenId;

    private boolean tutorialCompleted = true;

    private int stageNumber = 1;


    public enum Status {
        ONLINE, OFFLINE, PLAYING
    }


    /**
     * Gets the id.
     *
     * @return the value id.
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id value.
     */
    public void setId(final ObjectId id) {
        this.id = id;
    }

    /**
     * Gets the playerName.
     *
     * @return the value playerName.
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * Sets the playerName.
     *
     * @param playerName value.
     */
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }



    public int getEloRating() {
        return eloRating;
    }

    public void setEloRating(int eloRating) {
        this.eloRating = eloRating;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int gethighScore() {
        return highScore;
    }

    public void sethighScore(int highScore) {
        this.highScore = highScore;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ObjectId getTeamId(){
        return teamId;
    }

    public void setTeamId(ObjectId teamId){
        this.teamId =  teamId;
    }

    public Object getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public List getMatchesPlayed(){
        return  matchesPlayed;
    }

    public void setMatchesPlayed(List<Object> matchesPlayed){
        this.matchesPlayed = matchesPlayed;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }


    public void setTutorialCompleted(boolean tutorialCompleted) {
        this.tutorialCompleted = tutorialCompleted;
    }

    public boolean isTutorialCompleted() {
        return tutorialCompleted;
    }

    public void setStageNumber(int stageNumber) {
        this.stageNumber = stageNumber;
    }

    public int getStageNumber() {
        return stageNumber;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return eloRating == player.eloRating &&
                score == player.score &&
                highScore == player.highScore &&
                lives == player.lives &&
                tutorialCompleted == player.tutorialCompleted &&
                stageNumber == player.stageNumber &&
                Objects.equals(id, player.id) &&
                Objects.equals(playerName, player.playerName) &&
                Objects.equals(teamId, player.teamId) &&
                Objects.equals(campaignId, player.campaignId) &&
                Objects.equals(status, player.status) &&
                Objects.equals(matchesPlayed, player.matchesPlayed) &&
                Objects.equals(tokenId, player.tokenId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, playerName, teamId, campaignId, eloRating, score, highScore, lives, status, matchesPlayed, tokenId, tutorialCompleted, stageNumber);
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", playerName='" + playerName + '\'' +
                ", teamId=" + teamId +
                ", campaignId='" + campaignId + '\'' +
                ", eloRating=" + eloRating +
                ", score=" + score +
                ", highScore=" + highScore +
                ", lives=" + lives +
                ", status='" + status + '\'' +
                ", matchesPlayed=" + matchesPlayed +
                ", tokenId='" + tokenId + '\'' +
                ", tutorialCompleted=" + tutorialCompleted +
                ", stageNumber=" + stageNumber +
                '}';
    }
}
