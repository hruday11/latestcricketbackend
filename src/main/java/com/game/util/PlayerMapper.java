package com.game.util;
import com.game.api.Player;
import org.bson.Document;

/**
 * Mapper class for Player objects.
 *
 * @author Hruday
 * @version 1.0.0
 * @since 1.0.0
 */
public class PlayerMapper {

    /**
     * Map objects {@link Document} to {@link com.game.api.Player}.
     *
     * @return A object {@link com.game.api.Player}.
     */
    public static Player map(final Document playerDocument) {
        final Player player = new Player();
        player.setId(playerDocument.getObjectId("_id"));
        player.setPlayerName(playerDocument.getString("playerName"));
        player.setEloRating(playerDocument.getInteger("eloRating"));
        player.sethighScore(playerDocument.getInteger("highScore"));
        player.setLives(playerDocument.getInteger("lives"));
        player.setStatus(playerDocument.getString("status"));
        player.setTokenId(playerDocument.getString("tokenId"));
        player.setTeamId(playerDocument.getObjectId("teamId"));
        player.setCampaignId(playerDocument.getString("campaignId"));
        player.setTutorialCompleted(playerDocument.getBoolean("tutorialCompleted"));
        player.setStageNumber(playerDocument.getInteger("stageNumber"));
        return player;
    }
}