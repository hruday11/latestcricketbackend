package com.game.util;

import com.game.api.gameplaypojos.PlayersGameData;
import com.game.util.Decoders.PlayerDecoder;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PlayersMatchDataMapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(MatchDataMapper.class);

    public static PlayersGameData map(final Document playerGameDataDocument) {

        final PlayersGameData playersGameData = new PlayersGameData();
        final PlayerDecoder playerDecoder = new PlayerDecoder();
        playersGameData.setId(playerGameDataDocument.getObjectId("_id"));
        playersGameData.setGameId(playerGameDataDocument.getObjectId("gameId"));
        playersGameData.setOpponentId(playerGameDataDocument.getString("opponentId"));
        playersGameData.setPlayerId(playerGameDataDocument.getString("playerId"));
        playersGameData.setPlayerData(playerDecoder.decode((Document) playerGameDataDocument.get("playerData")));
        playersGameData.setWon(playerGameDataDocument.getString("won"));
        playersGameData.setCurrentBall(playerGameDataDocument.getInteger("currentBall"));
        return playersGameData;
    }
}
