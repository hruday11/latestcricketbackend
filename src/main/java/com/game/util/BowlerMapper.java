package com.game.util;
import com.game.api.Bowler;
import com.game.util.Decoders.RangeDecoder;
import org.bson.Document;

/**
 * Mapper class for bowler objects.
 *
 * @author Hruday
 * @version 1.0.0
 * @since 1.0.0
 */
public class BowlerMapper {

    /**
     * Map objects {@link Document} to {@link com.game.api.Bowler}.
     *
     * @return A object {@link com.game.api.Bowler}.
     */

    public static Bowler map(final Document bowlerDocument) {
        final Bowler bowler = new Bowler();
        final RangeDecoder rangeDecoder = new RangeDecoder();
        bowler.setId(bowlerDocument.getObjectId("_id"));
        bowler.setName(bowlerDocument.getString("name"));
        bowler.setTeamId(bowlerDocument.getObjectId("teamId"));
        bowler.setType(bowlerDocument.getString("type"));
        bowler.setDescription(bowlerDocument.getString("description"));
        bowler.setSpeed(rangeDecoder.decode((Document) bowlerDocument.get("speed")));
        bowler.setCampaignId(bowlerDocument.getString("campaignId"));
        bowler.setImage(bowlerDocument.getString("image"));
        bowler.setCharacter(bowlerDocument.getString("character"));
        return bowler;
    }
}