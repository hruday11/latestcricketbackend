package com.game.util.Decoders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.api.gameplaypojos.PlayerData;
import org.atmosphere.config.managed.Decoder;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Encode a {@link PlayerData} into a String
 */
public class PlayerDecoder implements Decoder<Document, PlayerData> {

    private final ObjectMapper mapper = new ObjectMapper();
    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerDecoder.class);

    @Override
    public PlayerData decode(Document s) {
        if(s == null) return null;
        return mapper.convertValue(s, PlayerData.class);
    }
}
