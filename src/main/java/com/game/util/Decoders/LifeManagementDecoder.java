package com.game.util.Decoders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.api.campaignpojos.LifeManagement;
import org.atmosphere.config.managed.Decoder;
import org.bson.Document;

/**
 * Encode a {@link LifeManagement} into a String
 */
public class LifeManagementDecoder implements Decoder<Document, LifeManagement> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public LifeManagement decode(Document s) {
        if(s == null) return null;
        return mapper.convertValue(s, LifeManagement.class);
    }
}