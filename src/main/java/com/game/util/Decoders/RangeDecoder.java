package com.game.util.Decoders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.api.gameplaypojos.Range;
import org.atmosphere.config.managed.Decoder;
import org.bson.Document;

/**
 * Encode a {@link Range} into a String
 */
public class RangeDecoder implements Decoder<Document, Range> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Range decode(Document s) {
        if(s == null) return null;
        return mapper.convertValue(s, Range.class);
    }
}