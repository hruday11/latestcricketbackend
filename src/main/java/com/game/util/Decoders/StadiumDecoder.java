package com.game.util.Decoders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.api.campaignpojos.Stadium;
import org.atmosphere.config.managed.Decoder;
import org.bson.Document;

/**
 * Encode a {@link Stadium} into a String
 */
public class StadiumDecoder implements Decoder<Document, Stadium> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Stadium decode(Document s) {
        if(s == null) return null;
        return mapper.convertValue(s, Stadium.class);
    }
}