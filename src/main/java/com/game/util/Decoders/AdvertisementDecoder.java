package com.game.util.Decoders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.api.campaignpojos.Advertisement;
import org.atmosphere.config.managed.Decoder;
import org.bson.Document;

/**
 * Encode a {@link Advertisement} into a String
 */
public class AdvertisementDecoder implements Decoder<Document, Advertisement> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Advertisement decode(Document s) {
        if(s == null) return null;
        return mapper.convertValue(s, Advertisement.class);
    }
}