package com.game.util.Decoders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.api.campaignpojos.CampaignActions;
import org.atmosphere.config.managed.Decoder;
import org.bson.Document;

/**
 * Encode a {@link CampaignActions} into a String
 */
public class CampaignActionsDecoder implements Decoder<Document, CampaignActions> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public CampaignActions decode(Document s) {
        if(s == null) return null;
        return mapper.convertValue(s, CampaignActions.class);
    }
}