package com.game.util.Decoders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.api.gameplaypojos.Color;
import com.game.api.gameplaypojos.PlayerData;
import org.atmosphere.config.managed.Decoder;
import org.bson.Document;

/**
 * Encode a {@link PlayerData} into a String
 */
public class ColorDecoder implements Decoder<Document, Color> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Color decode(Document s) {
        if(s == null) return null;
        return mapper.convertValue(s, Color.class);
    }
}
