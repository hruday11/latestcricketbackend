package com.game.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Utility {
    public List ballFinalSpotRightCoord = new ArrayList();
    public List ballFinalSecondSpotRightCoord = new ArrayList();
    public List ballFinalSpotLeftCoord = new ArrayList();
    public List ballFinalSpotCenterCoord = new ArrayList();
    public List ballFinalSpotSlowCoord = new ArrayList();

    public static Map g_gameDisplay = new HashMap();
    static {
        g_gameDisplay.put("width", 720);
        g_gameDisplay.put("height", 1280);
    }


    public void initFinalBallSpot() {
        // fast & medium far right
        Map ballFinalSpotRightCoordObj1 = new HashMap();
        ballFinalSpotRightCoordObj1.put("x", Utility.g_gameDisplay.get("width"));
        ballFinalSpotRightCoordObj1.put("y", (long)Utility.g_gameDisplay.get("height") * 0.7);
        ballFinalSpotRightCoord.add(ballFinalSpotRightCoordObj1);

        Map ballFinalSpotRightCoordObj2 = new HashMap();
        ballFinalSpotRightCoordObj2.put("x", Utility.g_gameDisplay.get("width"));
        ballFinalSpotRightCoordObj2.put("y", (long)Utility.g_gameDisplay.get("height") * 0.5);
        ballFinalSpotRightCoord.add(ballFinalSpotRightCoordObj2);

        Map ballFinalSpotRightCoordObj3 = new HashMap();
        ballFinalSpotRightCoordObj3.put("x", Utility.g_gameDisplay.get("width"));
        ballFinalSpotRightCoordObj3.put("y", (long)Utility.g_gameDisplay.get("height") * 0.3);
        ballFinalSpotRightCoord.add(ballFinalSpotRightCoordObj3);

        // fast & medium center right
        Map ballFinalSpotRightCoordObj4 = new HashMap();
        ballFinalSpotRightCoordObj4.put("x", (long)Utility.g_gameDisplay.get("width")* 0.8);
        ballFinalSpotRightCoordObj4.put("y", (long)Utility.g_gameDisplay.get("height") * 0.65);
        ballFinalSecondSpotRightCoord.add(ballFinalSpotRightCoordObj4);

        Map ballFinalSpotRightCoordObj5 = new HashMap();
        ballFinalSpotRightCoordObj5.put("x", (long)Utility.g_gameDisplay.get("width")* 0.8);
        ballFinalSpotRightCoordObj5.put("y", (long)Utility.g_gameDisplay.get("height") * 0.5);
        ballFinalSecondSpotRightCoord.add(ballFinalSpotRightCoordObj5);

        Map ballFinalSpotRightCoordObj6= new HashMap();
        ballFinalSpotRightCoordObj6.put("x", (long)Utility.g_gameDisplay.get("width")* 0.8);
        ballFinalSpotRightCoordObj6.put("y", (long)Utility.g_gameDisplay.get("height") * 0.35);
        ballFinalSecondSpotRightCoord.add(ballFinalSpotRightCoordObj6);


        // fast & medium left
        Map ballFinalSpotRightCoordObj9= new HashMap();
        ballFinalSpotRightCoordObj9.put("x", 0);
        ballFinalSpotRightCoordObj9.put("y", (long)Utility.g_gameDisplay.get("height") * 0.6);
        ballFinalSpotLeftCoord.add(ballFinalSpotRightCoordObj9);

        Map ballFinalSpotRightCoordObj10= new HashMap();
        ballFinalSpotRightCoordObj10.put("x", 0);
        ballFinalSpotRightCoordObj10.put("y", (long)Utility.g_gameDisplay.get("height") * 0.5);
        ballFinalSpotLeftCoord.add(ballFinalSpotRightCoordObj10);

        Map ballFinalSpotRightCoordObj11= new HashMap();
        ballFinalSpotRightCoordObj11.put("x", 0);
        ballFinalSpotRightCoordObj11.put("y", (long)Utility.g_gameDisplay.get("height") * 0.4);
        ballFinalSpotLeftCoord.add(ballFinalSpotRightCoordObj11);

        // spin ball / Shadow final spot
        Map ballFinalSpotRightCoordObj12= new HashMap();
        ballFinalSpotRightCoordObj12.put("x", (long)Utility.g_gameDisplay.get("width")* 0.1);
        ballFinalSpotRightCoordObj12.put("y", 0);
        ballFinalSpotCenterCoord.add(ballFinalSpotRightCoordObj12);

        Map ballFinalSpotRightCoordObj13= new HashMap();
        ballFinalSpotRightCoordObj13.put("x", (long)Utility.g_gameDisplay.get("width")* 0.3);
        ballFinalSpotRightCoordObj13.put("y", 0);
        ballFinalSpotCenterCoord.add(ballFinalSpotRightCoordObj13);

        Map ballFinalSpotRightCoordObj14= new HashMap();
        ballFinalSpotRightCoordObj14.put("x", (long)Utility.g_gameDisplay.get("width")* 0.58);
        ballFinalSpotRightCoordObj14.put("y", 0);
        ballFinalSpotCenterCoord.add(ballFinalSpotRightCoordObj14);

        Map ballFinalSpotRightCoordObj15= new HashMap();
        ballFinalSpotRightCoordObj15.put("x", (long)Utility.g_gameDisplay.get("width")* 0.85);
        ballFinalSpotRightCoordObj15.put("y", 0);
        ballFinalSpotCenterCoord.add(ballFinalSpotRightCoordObj15);

        // slow ball lower Left

        Map ballFinalSpotRightCoordObj16= new HashMap();
        ballFinalSpotRightCoordObj16.put("x", 0);
        ballFinalSpotRightCoordObj16.put("y", (long)Utility.g_gameDisplay.get("height")*0.1);
        ballFinalSpotSlowCoord.add(ballFinalSpotRightCoordObj16);

        Map ballFinalSpotRightCoordObj17= new HashMap();
        ballFinalSpotRightCoordObj17.put("x", 0);
        ballFinalSpotRightCoordObj17.put("y", (long)Utility.g_gameDisplay.get("height")*0.3);
        ballFinalSpotSlowCoord.add(ballFinalSpotRightCoordObj17);

        Map ballFinalSpotRightCoordObj18= new HashMap();
        ballFinalSpotRightCoordObj18.put("x", (long)Utility.g_gameDisplay.get("width"));
        ballFinalSpotRightCoordObj18.put("y", (long)Utility.g_gameDisplay.get("height")*0.3);
        ballFinalSpotSlowCoord.add(ballFinalSpotRightCoordObj18);

        Map ballFinalSpotRightCoordObj19= new HashMap();
        ballFinalSpotRightCoordObj19.put("x", (long)Utility.g_gameDisplay.get("width"));
        ballFinalSpotRightCoordObj19.put("y", (long)Utility.g_gameDisplay.get("height")*0.1);
        ballFinalSpotSlowCoord.add(ballFinalSpotRightCoordObj19);

    }

    public List getPitchSpotCoord() {
        List pitchSpotCoord = new ArrayList();

        // 1st column - leftmost 3 pitch points (short, good, full)
        Coordinates c1 = new Coordinates(375, 588);
        Coordinates c2 = new Coordinates(358, 538);
        Coordinates c3 = new Coordinates(331, 467);
        List spot1 = new ArrayList();
        spot1.add(c1);
        spot1.add(c2);
        spot1.add(c3);
        pitchSpotCoord.add(spot1);

        // 2nd column - left middle
        Coordinates c4 = new Coordinates(398, 588);
        Coordinates c5 = new Coordinates(385, 537);
        Coordinates c6 = new Coordinates(369, 466);
        List spot2 = new ArrayList();
        spot1.add(c4);
        spot1.add(c5);
        spot1.add(c6);
        pitchSpotCoord.add(spot2);

        // 3rd column - right middle
        Coordinates c7 = new Coordinates(418, 587);
        Coordinates c8 = new Coordinates(414, 536);
        Coordinates c9 = new Coordinates(408, 465);
        List spot3 = new ArrayList();
        spot1.add(c7);
        spot1.add(c8);
        spot1.add(c9);
        pitchSpotCoord.add(spot3);

        // 4th column - right most
        Coordinates c10 = new Coordinates(439, 587);
        Coordinates c11 = new Coordinates(443, 536);
        Coordinates c12 = new Coordinates(448, 465);
        List spot4 = new ArrayList();
        spot1.add(c10);
        spot1.add(c11);
        spot1.add(c12);
        pitchSpotCoord.add(spot4);

        return  pitchSpotCoord;
    }

    public Map getBallLocationBasedOnBowlerList() {
        Map ballLocationBasedOnBowlerList = new HashMap();

        Map fastB = new HashMap();
        fastB.put("X", 459);
        fastB.put("y", 808);
        fastB.put("bowlerTime",1700);
        fastB.put("throwTime",1000);
        ballLocationBasedOnBowlerList.put("fast", fastB);

        Map mediumB = new HashMap();
        mediumB.put("X", 459);
        mediumB.put("y", 808);
        mediumB.put("bowlerTime",1825);
        mediumB.put("throwTime",1150);
        ballLocationBasedOnBowlerList.put("medium", mediumB);

        Map slowB = new HashMap();
        slowB.put("X", 449);
        slowB.put("y", 823);
        slowB.put("bowlerTime",1465);
        slowB.put("throwTime",1150);
        ballLocationBasedOnBowlerList.put("slow", slowB);

        Map spinB = new HashMap();
        spinB.put("X", 449);
        spinB.put("y", 808);
        spinB.put("bowlerTime",1150);
        spinB.put("throwTime",950);
        ballLocationBasedOnBowlerList.put("spin", spinB);

        return  ballLocationBasedOnBowlerList;
    }
}
