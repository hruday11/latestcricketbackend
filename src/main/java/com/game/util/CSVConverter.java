package com.game.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.*;
import java.util.*;

public class CSVConverter {
    List<List<String>> rows = new ArrayList<>();


    public List<Map<String, String>> ConvertCSV() throws IOException {

       try (InputStream in = new FileInputStream("sample.xls");) {

           CSV csv = new CSV(true, ',', in );
           List < String > fieldNames = null;
           if (csv.hasNext()) fieldNames = new ArrayList < > (csv.next());
           List < Map < String, String >> list = new ArrayList < > ();
           while (csv.hasNext()) {
               List < String > x = csv.next();
               Map< String, String > obj = new LinkedHashMap<>();
               for (int i = 0; i < fieldNames.size(); i++) {
                   obj.put(fieldNames.get(i), x.get(i));
               }
               list.add(obj);
           }
           ObjectMapper mapper = new ObjectMapper();
           mapper.enable(SerializationFeature.INDENT_OUTPUT);
//           mapper.writeValue(System.out, list);
           return list;
       } catch (Exception e) {
           e.printStackTrace();
       }
        return null;
    }

}
