package com.game.util;

import com.game.api.*;
import com.game.util.Decoders.ColorDecoder;
import org.bson.Document;

public class TeamMapper {
    public static Team map(final Document teamDocument) {
        final Team team = new Team();
        final ColorDecoder colorDecoder = new ColorDecoder();
        team.setId(teamDocument.getObjectId("_id"));
        team.setScore(teamDocument.getInteger("score"));
        team.setName(teamDocument.getString("name"));
        team.setJerseyColor(colorDecoder.decode((Document) teamDocument.get("jerseyColor")));
        team.setPlayerCount(teamDocument.getInteger("playerCount"));
        team.setCampaignId(teamDocument.getString("campaignId"));
        team.setTeamLogo(teamDocument.getString("teamLogo"));
        team.setSponsorLogo(teamDocument.getString("sponsorLogo"));

        return team;
    }

}
