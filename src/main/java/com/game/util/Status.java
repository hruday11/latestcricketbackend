package com.game.util;

public enum Status {
        ONLINE("ONLINE"),
        OFFLINE("OFFLINE"),
        PLAYING("PLAYING");

        private final String text;

        /**
         * @param text
         */
        Status(final String text) {
            this.text = text;
        }

        /* (non-Javadoc)
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return text;
        }
    }
