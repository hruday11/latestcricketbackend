package com.game.util;
import org.bson.Document;

/**
 * Mapper class for bowler objects.
 *
 * @author Hruday
 * @version 1.0.0
 * @since 1.0.0
 */
public class PlayerDataMapper {

    /**
     * Map objects {@link Document} to {@link com.game.api.Bowler}.
     *
     * @return A object {@link com.game.api.Bowler}.
     */
//    public static PlayerData map(final Document playerDocument) {
//        final PlayerData playerData = new PlayerData();
//        playerData.setOvers(playerData.getOvers("overs"));
//        return playerData;
//    }
}