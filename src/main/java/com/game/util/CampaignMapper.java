package com.game.util;

import com.game.api.CampaignDetails;
import com.game.util.Decoders.*;
import org.bson.Document;

import java.util.List;

public class CampaignMapper {
    public static CampaignDetails map(final Document campaignDocument) {
        final CampaignDetails campaign = new CampaignDetails();
        final AdvertisementDecoder advertisementDecoder = new AdvertisementDecoder();
        final CampaignActionsDecoder campaignActionsDecoder = new CampaignActionsDecoder();
        final StadiumDecoder stadiumDecoder = new StadiumDecoder();
        final LifeManagementDecoder lifeManagementDecoder = new LifeManagementDecoder();

        campaign.setCampaignName(campaignDocument.getString("campaignName"));
        campaign.setId(campaignDocument.getObjectId("_id"));
        campaign.setAdvertisement(advertisementDecoder.decode((Document) campaignDocument.get("advertisement")));
        campaign.setCampaignActions(campaignActionsDecoder.decode((Document)campaignDocument.get("campaignActions")));
        campaign.setLifeManagement(lifeManagementDecoder.decode((Document)campaignDocument.get("lifeManagement")));
        campaign.setStadium(stadiumDecoder.decode((Document)campaignDocument.get("stadium")));
        campaign.setTips((List<String>) campaignDocument.get("tips"));
        campaign.setCampaignId(campaignDocument.getString("campaignId"));
        return campaign;
    }
}
