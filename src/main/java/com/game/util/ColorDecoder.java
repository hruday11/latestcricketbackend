package com.game.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.game.api.gamePlayPOJOS.Color;
import com.game.api.gamePlayPOJOS.PlayerData;
import org.atmosphere.config.managed.Decoder;
import org.bson.Document;

/**
 * Encode a {@link PlayerData} into a String
 */
public class ColorDecoder implements Decoder<Document, Color> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Color decode(Document s) {
        if(s == null) return null;
        return mapper.convertValue(s, Color.class);
    }
}
