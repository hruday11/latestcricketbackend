package com.game.util;

import com.game.api.gameplaypojos.BallData;

import java.util.Random;

public class BallPath {

    public static String lineOfString(){
        String[] objects = { "left", "right"};
        int length = objects.length;
        for (int i = 0; i < length; i++)
        {
            int rand = (int) (Math.random() * length);
            return objects[rand];
        }
        return "right";
    }

    Random rand = new Random();
    private int length = rand.nextInt(3);;                                 // [0, 2] 0 - short, 1 - good, 2 - full
    private String pitchLine = lineOfString();                             // "left" or "right"
    private double lineIndex = 0;                              // [0, 3] 0 - leftmost, 3 - rightmost lane
    private double frames = 0;                                 // Used to determine the perfect point or timing functionalities
    private double perfectPoint = 0;
    private long difficulty = 1;
    private double steps;
    private double perfectZone;
    private String hitDirection;


    double preVelocity;
    double preIncrement;
    double postIncrement;
    double postVelocity;
    double finalSpotIndex;
    double extreme;
    double difficultModSteps;
    boolean wicketInPathBool = false;
    boolean batsmanInPathBool = false;


    public BallData calculateBallPath(int speed, String bowlerType) {
        if (speed < 60) {
            this.slowAndSpinBall(bowlerType, speed);
        } else {
            this.fastAndMediumBall(speed);
        }

        this.frames = this.frameCalculator(preVelocity, preIncrement);

        this.calculateHitSections(speed);
        isWicketOrBatsmanInPathOfTheBall(bowlerType);

        BallData ballData = new BallData(this.frames,
                speed,
                this.length,
                this.perfectPoint,
                this.steps,
                this.difficultModSteps,
                this.difficulty,
                this.finalSpotIndex,
                this.lineIndex,
                this.preVelocity,
                this.preIncrement,
                this.postVelocity,
                this.postIncrement,
                this.pitchLine,
                this.extreme,
                this.perfectZone,
                0.0,
                0.0,
                null,
                0,
                false,
                bowlerType,
                this.wicketInPathBool,
                this.batsmanInPathBool);

        return ballData;

    }

    public void slowAndSpinBall(String bowlerType, int speed) {


        String spinTypeString = "";

        if ( bowlerType == "spin" ) {
            long upperLimitInt = 3;
            long lowerLimitInt = 0;

            long rangeInt = 1;

            if (speed > 50) {
                rangeInt = 2;
            }

            // Line changers
            if (this.length == 0) {
                if (rangeInt == 1) {

                    if (this.pitchLine == "right") {
                        lowerLimitInt += 2;
                    }
                    upperLimitInt -= 1;

                }else{
                    if(this.pitchLine == "right"){
                        lowerLimitInt += 1;
                    }
                }

                this.lineIndex = lowerLimitInt + Math.floor(Math.random() * (upperLimitInt));
                if (this.pitchLine == "left") {
                    spinTypeString = "left";
                    if (this.lineIndex == 0) {
                        spinTypeString = "right";
                    } else if (this.lineIndex == 1) {
                        spinTypeString = Math.random() <= 0.5 ? "left" : "right";
                        rangeInt = 1;
                    }

                } else {
                    spinTypeString = "right";
                    if (this.lineIndex == 3) {
                        spinTypeString = "left";
                    } else if (this.lineIndex == 2) {
                        spinTypeString = Math.random() <= 0.5 ? "left" : "right";
                        rangeInt = 1;
                    }

                }

            } else {

                if (this.pitchLine == "left") {
                    this.lineIndex = Math.floor(Math.random() * 2);
                } else {
                    this.lineIndex = 2 + Math.floor(Math.random() * 2);
                }

                if (this.lineIndex == 0) {
                    spinTypeString = "right";
                } else if (this.lineIndex == 3) {
                    spinTypeString = "left";
                } else {
                    spinTypeString = Math.random() <= 0.5 ? "left" : "right";
                }
            }

            if (spinTypeString == "left") {
                finalSpotIndex = Math.max(0, this.lineIndex - rangeInt);
            } else {
                finalSpotIndex = Math.min(3, this.lineIndex + rangeInt);
            }

        } else {
            if (this.pitchLine == "left") {
                this.lineIndex = Math.floor(Math.random() * 2);
            } else {
                this.lineIndex = 2 + Math.floor(Math.random() * 2);
            }
            finalSpotIndex = this.lineIndex;
        }

        switch(this.length) {
            case 0:
                this.preVelocity = 0.00175 + (speed * 0.000175) - (this.length * 0.00175);
                this.preIncrement = 1.0125 + (speed * 0.000325);
                this.postVelocity = this.preVelocity * 0.7;
                this.postIncrement = this.preIncrement;
                break;

            case 1:
                this.preVelocity = 0.0020 + (speed * 0.00020) - (this.length * 0.0020);
                this.preIncrement = 1.0125 + (speed * 0.000325);
                this.postVelocity = this.preVelocity * 1.10;
                this.postIncrement = this.preIncrement + (speed * 0.00010);
                break;

            case 2:
                this.preVelocity = 0.00225 + (speed * 0.000225) - (this.length * 0.00225);
                this.preIncrement = 1.0125 + (speed * 0.000325);
                this.postVelocity = this.preVelocity * 1.50;
                this.postIncrement = this.preIncrement + (speed * 0.00015);
                break;
        }
    }

    public void fastAndMediumBall(int speed) {
        // medium and fast balls

        if (this.pitchLine == "left") {
            this.lineIndex = Math.floor(Math.random() * 2);
        } else {
            this.lineIndex = 2 + Math.floor(Math.random() * 2);
        }
        this.finalSpotIndex = this.lineIndex;
        // slow and spin balls
        // note increment always has to be > 1;
        switch(this.length) {
            case 0:
                this.preVelocity = 0.00175 + ((speed) * 0.000175) - (this.length * 0.00175);
                this.preIncrement = 1.015 + (speed * 0.000375);
                this.postVelocity = this.preVelocity;
                this.postIncrement = this.preIncrement;
                break;
            case 1:
                this.preVelocity = 0.0020 + ((speed) * 0.00020) - (this.length * 0.0020);
                this.preIncrement = 1.015 + (speed * 0.00035);
                this.postVelocity = this.preVelocity * 1.10;
                this.postIncrement = this.preIncrement + (speed * 0.00010);
                break;
            case 2:
                this.preVelocity = 0.00225 + ((speed) * 0.000225) - (this.length * 0.00225);
                this.preIncrement = 1.015 + (speed * 0.000325);
                this.postVelocity = this.preVelocity * 1.50;
                this.postIncrement = this.preIncrement + (speed * 0.00015);
                break;
        }
    }

    public double frameCalculator(double velocityDouble, double prePitchIncrementDouble) {
        // The equation                 ln(1-((1-r)/velocity*increment)/ln(increment) is used to find the number of frames required for the ball to reach the pitch point.
        // The fps of the engine is taken as 60 for reference. In case of lower or higher fps, the ball moves relative to the dt.

        double numeratorDouble = Math.log(1-((1-prePitchIncrementDouble)/(velocityDouble*prePitchIncrementDouble)));
        double denominatorDouble = Math.log(prePitchIncrementDouble);
        double frames = Math.floor(numeratorDouble/denominatorDouble);

        return frames;
    }

    public void calculateHitSections(int speed) {
        // formulas once finalized need to be refactored.
        if (speed >= 60 ) {
            // fast and medium balls
            this.perfectPoint = this.frames + (80 - speed)*0.003*this.frames + (1.5-this.length)*this.frames*0.075;
            this.steps = Math.max(80/speed, 1);
        } else {
            // slow and spin balls
            this.perfectPoint = this.frames + (50 - speed)*0.003*this.frames + (1.5-this.length)*this.frames*0.125;
            this.steps = Math.max(50/speed, 1.25); // 1.25 constant
        }

        // difficulty is WIP, keep it to 1 for now.
        this.perfectZone = this.steps * this.difficulty;
        this.extreme = this.steps * 7;

        // since the extreme is 7 multiplied by the steps value. We get the difficulty modified steps value using:
        // Explained in detail in the doc:
        this.difficultModSteps = ((7 - this.difficulty)/6) * this.steps;

    }



    public void isWicketOrBatsmanInPathOfTheBall(String bowlerType) {


        if ( bowlerType == "spin" ) {
            // Try and catch statements are important since we havn't added all types of ball combinations possible.
            // ps: In order to avoid undefined error.
            try {
                String ballImpacts = this.spinBallImpactList(this.length, this.pitchLine, this.lineIndex, this.finalSpotIndex);
                if ( ballImpacts == "wicket" ) {
                    wicketInPathBool = true;

                } else if ( ballImpacts == "batsman" ) {
                    batsmanInPathBool = true;
                }
            } catch(Error error) {
                return;
            }
        } else if ( bowlerType == "slow" ) {
            // Try and catch statements are important since we havn't added all types of ball combinations possible.
            // ps: In order to avoid undefined error.
            try {
                String ballImpacts = this.slowBallImpactList(this.length, this.pitchLine, this.lineIndex, this.finalSpotIndex);
                if ( ballImpacts == "wicket" ) {
                    wicketInPathBool = true;
                } else if ( ballImpacts == "batsman" ) {
                    batsmanInPathBool = true;
                }
            } catch(Error error) {
                return;
            }
        } else {
            // fast and medium balls
            if (lineIndex == 0) {
                // if ball is pitching on any of the leftmost pitches. It will impact batsman
                batsmanInPathBool = true;
            } else if (lineIndex == 1 && length == 2) {
                // if length is 2(full) and line is left middle. It will impact wicket
                wicketInPathBool = true;
            }
        }
        return;
    }

    public String spinBallImpactList(int length, String pitchLine, double lineIndex, double finalSpotIndex) {
        if(length == 0 && pitchLine == "left" && lineIndex == 0 && finalSpotIndex == 1) {
            return  "wicket";
        } else if(length == 0 && pitchLine == "left" && lineIndex == 1 && finalSpotIndex == 0) {
            return  "batsman";
        } else if(length == 0 && pitchLine == "left" && lineIndex == 2 && finalSpotIndex == 0) {
            return  "wicket";
        } else if(length == 0 && pitchLine == "left" && lineIndex == 2 && finalSpotIndex == 1) {
            return  "wicket";
        } else if(length == 1 && pitchLine == "left" && lineIndex == 0 && finalSpotIndex == 1) {
            return  "wicket";
        } else if(length == 1 && pitchLine == "left" && lineIndex == 0 && finalSpotIndex == 2) {
            return  "wicket";
        } else if(length == 1 && pitchLine == "left" && lineIndex == 1 && finalSpotIndex == 0) {
            return  "batsman";
        } else if(length == 1 && pitchLine == "right" && lineIndex == 2 && finalSpotIndex == 1) {
            return  "wicket";
        } else if(length == 1 && pitchLine == "right" && lineIndex == 2 && finalSpotIndex == 0) {
            return  "wicket";
        } else if(length == 2 && pitchLine == "left" && lineIndex == 0 && finalSpotIndex == 1) {
            return  "wicket";
        } else if(length == 2 && pitchLine == "left" && lineIndex == 0 && finalSpotIndex == 2) {
            return  "batsman";
        } else if(length == 2 && pitchLine == "left" && lineIndex == 1 && finalSpotIndex == 0) {
            return  "batsman";
        } else if(length == 2 && pitchLine == "left" && lineIndex == 1 && finalSpotIndex == 2) {
            return  "wicket";
        } else if(length == 2 && pitchLine == "right" && lineIndex == 2 && finalSpotIndex == 0) {
            return  "wicket";
        }

        return  "";
    }

    public  String slowBallImpactList(int length, String pitchLine, double lineIndex, double finalSpotIndex) {
        if(length == 0 && pitchLine == "left" && lineIndex == 0 && finalSpotIndex == 0) {
            return  "wicket";
        } else if(length == 1 && pitchLine == "left" && lineIndex == 0 && finalSpotIndex == 0) {
            return  "batsman";
        } else if(length == 1 && pitchLine == "left" && lineIndex == 1 && finalSpotIndex == 1) {
            return  "wicket";
        } else if(length == 2 && pitchLine == "left" && lineIndex == 0 && finalSpotIndex == 0) {
            return  "batsman";
        } else if(length == 2 && pitchLine == "left" && lineIndex == 1 && finalSpotIndex == 1) {
            return  "wicket";
        }

        return  "";
    }

    public void checkShot(double timeTappedFloat, String hitDirectionString) {

        double differenceFloat = this.perfectPoint - timeTappedFloat;
        double absoluteDifferenceFloat = Math.abs(differenceFloat);
        boolean isHitBool = false;
        int scoreInt = 0;

        if (hitDirection == this.pitchLine) {

            if (absoluteDifferenceFloat >= this.extreme) {
                isHitBool = true;
                scoreInt = 0;
                return;
            }

            if (absoluteDifferenceFloat <= this.perfectZone) {
                // perfect shot - 6
                scoreInt = 6;
                // this.ballShot = this.hitSpot[this.length][this.pitchLine][scoreInt];
                // (this.pitchLine == "left") ? this._batsman.setAnimation(0, '3 Six', false) : this._batsman.setAnimation(0, '1 Six', false)
                isHitBool = true;
                // alert(" SIXER ");
            } else if (absoluteDifferenceFloat <= this.perfectZone+this.steps*1) {
                // semi perfect shot - 4
                // (this.pitchLine == "left") ? this._batsman.setAnimation(0, '3 Six', false) : this._batsman.setAnimation(0, '1 Six', false)
                scoreInt = 4;
                // this.ballShot = this.hitSpot[this.length][this.pitchLine][scoreInt];
                isHitBool = true;
                // alert(" FOUR ");
            } else if (absoluteDifferenceFloat <= this.perfectZone+this.steps*2) {
                // moderate shot - 3
                // (this.pitchLine == "left") ? this._batsman.setAnimation(0, '3 Six', false) : this._batsman.setAnimation(0, '1 Six', false)
                scoreInt = 3;
                // this.ballShot = this.hitSpot[this.length][this.pitchLine][scoreInt];
                isHitBool = true;
                // alert(" Three ");
            } else if (absoluteDifferenceFloat <= this.perfectZone+this.steps*4) {
                // before the perfect point
                // (this.pitchLine == "left") ? this._batsman.setAnimation(0, '3 Six', false) : this._batsman.setAnimation(0, '1 Six', false)
                scoreInt = 2;
                // this.ballShot = this.hitSpot[this.length][this.pitchLine][scoreInt];
                isHitBool = true;
                // alert(" two ");
            } else if (absoluteDifferenceFloat < this.perfectZone + this.steps*6) {
                // before the perfect point
                // (this.pitchLine == "left") ? this._batsman.setAnimation(0, '3 Six', false) : this._batsman.setAnimation(0, '1 Six', false)
                scoreInt = 1;
                // this.ballShot = this.hitSpot[this.length][this.pitchLine][scoreInt];
                isHitBool = true;
                // alert(" 1 ");
            }

        } else {
            // wrong direction
            scoreInt = 0;
        }
    }

}