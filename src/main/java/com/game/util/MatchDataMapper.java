package com.game.util;

import com.game.api.gameplaypojos.MatchData;
import com.game.util.Decoders.PlayerDecoder;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MatchDataMapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(MatchDataMapper.class);

    public static MatchData map(final Document matchDataDocument) {

            final MatchData matchDataCopy = new MatchData();
            final PlayerDecoder playerDecoder = new PlayerDecoder();
            matchDataCopy.setId(matchDataDocument.getObjectId("_id"));
            matchDataCopy.setPlayerId(matchDataDocument.getString("playerId"));
            matchDataCopy.setOpponentId(matchDataDocument.getString("opponentId"));
            matchDataCopy.setPlayerData(playerDecoder.decode((Document) matchDataDocument.get("playerData")));
            matchDataCopy.setOpponentData(playerDecoder.decode((Document) matchDataDocument.get("opponentData")));
            matchDataCopy.setWon(matchDataDocument.getString("won"));
            matchDataCopy.setCount(matchDataDocument.getInteger("count"));
            matchDataCopy.setMatchStatus(matchDataDocument.getString("matchStatus"));
            matchDataCopy.setBot(matchDataDocument.getBoolean("isBot"));
            matchDataCopy.setBotId(matchDataDocument.getString("botId"));
            matchDataCopy.setCampaignId(matchDataDocument.getString("campaignId"));
            return matchDataCopy;
        }

}
