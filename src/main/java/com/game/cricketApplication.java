package com.game;

import com.game.db.MongoDBFactoryConnection;
import com.game.db.MongoDBManaged;
import com.game.db.daos.*;
import com.game.health.CricketHealthCheck;
import com.game.health.HealthCheckResource;
import com.game.resources.*;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import javax.servlet.*;
import java.util.EnumSet;


public class CricketApplication extends Application<CricketConfiguration> {

    public static void main(final String[] args) throws Exception {
        new CricketApplication().run(args);
    }

    @Override
    public String getName() {
        return "CricketGame";
    }

    @Override
    public void initialize(final Bootstrap<CricketConfiguration> bootstrap) {
        //initialize this configuration
    }


    @Override
    public void run(final CricketConfiguration configuration,
                    final Environment environment) {
        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        final MongoDBFactoryConnection mongoDBManagerConn = new MongoDBFactoryConnection(configuration.getMongoDBConnection());

        final MongoDBManaged mongoDBManaged = new MongoDBManaged(mongoDBManagerConn.getClient());
        final TeamDAO teamDao = new TeamDAO(mongoDBManagerConn.getClient()
                .getDatabase(configuration.getMongoDBConnection().getDatabase())
                .getCollection("teams"));
        final PlayerDAO playerDAO = new PlayerDAO(mongoDBManagerConn.getClient()
                .getDatabase(configuration.getMongoDBConnection().getDatabase())
                .getCollection("players"), teamDao);
        final BowlerDAO bowlerDao = new BowlerDAO(mongoDBManagerConn.getClient()
                .getDatabase(configuration.getMongoDBConnection().getDatabase())
                .getCollection("bowlers"));
        final MatchDataDAO matchDataDAO = new MatchDataDAO(mongoDBManagerConn.getClient()
                .getDatabase(configuration.getMongoDBConnection().getDatabase())
                .getCollection("matches"), playerDAO);
        final PlayersGameDataDAO playersGameDataDAO = new PlayersGameDataDAO(mongoDBManagerConn.getClient()
                .getDatabase(configuration.getMongoDBConnection().getDatabase())
                .getCollection("playersGameData"), matchDataDAO);
        final CampaignDAO campaignDAO = new CampaignDAO(mongoDBManagerConn.getClient()
                .getDatabase(configuration.getMongoDBConnection().getDatabase())
                .getCollection("campaigns"));
        environment.lifecycle().manage(mongoDBManaged);
        environment.jersey().register(new PlayerResource(playerDAO, configuration));
        environment.jersey().register(new BowlerResource(bowlerDao));
        environment.jersey().register(new TeamResource(teamDao));
        environment.jersey().register(new StartGameResource(matchDataDAO, playerDAO, bowlerDao, playersGameDataDAO, configuration, teamDao));
        environment.jersey().register(new MatchDataResource(matchDataDAO, playerDAO, playersGameDataDAO, configuration));
        environment.jersey().register(new ScoreBoardResource(configuration, teamDao));
        environment.jersey().register(new CampaignResource(bowlerDao, teamDao, campaignDAO));
        environment.healthChecks().register("CricketGameHealthCheck",
                new CricketHealthCheck(mongoDBManagerConn.getClient()));
        environment.jersey().register(new HealthCheckResource());


    }


}
