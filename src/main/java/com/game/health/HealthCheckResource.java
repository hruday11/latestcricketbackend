package com.game.health;

import com.game.db.MongoDBFactoryConnection;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.swing.text.DefaultStyledDocument.ElementSpec.ContentType;

@Path("/")
@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML, MediaType.TEXT_PLAIN})
@Api(value = "HealthCheckAndOor", description = "HealthCheck and OOR")
public class HealthCheckResource {
    private static boolean rotation = true;
    private String secretKey = "3pgames2020xyz";

    public static final Logger LOGGER = LoggerFactory.getLogger(HealthCheckResource.class);


    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "url not found")
    })
    @GET
    @Path("/{status: status|elb-healthcheck}")
    public Response status() {
        if (rotation) {
            return Response.status(Response.Status.OK).entity("ok").header(HttpHeaders.CONTENT_TYPE,"text/plain" )
                    .build();
        }
        return Response.status(Response.Status.NOT_FOUND).header(HttpHeaders.CONTENT_TYPE, "text/plain")
                .build();
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "url not found")
    })
    @POST
    @Path("/oor")
    public Response oor(@QueryParam("secret") @NotNull @NotEmpty String secret) {
        if (secret.equals(secretKey)) {
            rotation = false;
            return Response.status(Response.Status.OK).entity("oor").header(HttpHeaders.CONTENT_TYPE,"text/plain" )
                    .build();
        } else {
            LOGGER.error("Unauthorised request to access this resource");
            return Response.status(Response.Status.NOT_FOUND).header(HttpHeaders.CONTENT_TYPE, "text/plain")
                    .build();
        }

    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation success."),
            @ApiResponse(code = 404, message = "url not found")
    })
    @POST
    @Path("/bir")
    public Response bir(@QueryParam("secret") @NotNull @NotEmpty String secret) {
        if (secret.equals(secretKey)) {
            rotation = true;
            return Response.status(Response.Status.OK).entity("bir").header(HttpHeaders.CONTENT_TYPE,"text/plain" )
                    .build();
        } else {
            LOGGER.error("Unauthorised request to access this resource");
            return Response.status(Response.Status.NOT_FOUND).header(HttpHeaders.CONTENT_TYPE, "text/plain")
                    .build();
        }

    }


}
